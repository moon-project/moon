/**
 * @file crt1.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <stdlib.h>
#include <unistd.h>

void _start() {
    char sp[0];
    asm volatile("movq %%rsp, %0\n\t" : "=X"(sp));
    int* argc = (int*)((char*)sp + 8 + 16);
    const char** argv = (const char**)(((char*)argc) + 8);
    environ = (char**)(((char*)argv + 8) + 8 * *argc);
    exit(main(*argc, argv, environ));
}
