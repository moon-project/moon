#! /usr/bin/env bash

make all

if [ $? -ne 0 ]; then
  exit 1
fi

qcmd=(qemu-system-x86_64                                                   \
    -no-reboot                                                             \
    -display curses                                                        \
    -m 128M                                                           \
    -drive id=boot,format=raw,file=$USER.img,if=none                       \
    -drive id=data,format=raw,file=$USER-data.img,if=none                  \
    -device ahci,id=ahci                                                   \
    -device ide-drive,drive=boot,bus=ahci.0                                \
    -device ide-drive,drive=data,bus=ahci.1)

if [ -z "$1" ]; then
  qcmd+=(-s -S)
fi

bochscmd=(bochs -q)

if [[ "$1" == "box" ]]; then
  "${bochscmd[@]}"
else
  "${qcmd[@]}"
fi
