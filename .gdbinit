set history filename .gdb_history
set history save
set print symbol-filename on
set print array on
set print pretty on
set detach-on-fork on
set follow-fork-mode parent
target remote localhost:1234
break start
continue
