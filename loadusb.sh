#! /usr/bin/env bash

rm -rf bin/*
rm -rf libc/*
make clean all
sudo dd if=$USER.img of=/dev/sdb bs=16k && sync
git checkout -- bin
git checkout -- libc
