/**
 * @file assert.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __ASSERT_H
#define __ASSERT_H

#include <stdio.h>
#include <sys/debug.h>

#define assert(condition)                                                                                              \
    do {                                                                                                               \
        if (!(condition)) {                                                                                            \
            puts(CLR_RED "ASSERTION FAILED: " TOSTRING(condition) " is not true in " AT CLR_NRM "\n");                 \
            abort();                                                                                                   \
        }                                                                                                              \
    } while (0)

void abort();

#endif /* __ASSERT_H */
