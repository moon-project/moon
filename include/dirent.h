/**
 * @file dirent.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __DIRENT_H
#define __DIRENT_H

#include <sys/defs.h>

#define NAME_MAX 255

/**
 * @brief      Opens directory
 *
 * @param      dir   The dir
 * @param[in]  name  The name
 *
 * @return     { description_of_the_return_value }
 */
int opendir_takes_ptr(DIR* dir, const char* name);
dirent_t* typedreaddir(DIR* dirp);

DIR* opendir(const char* name);
struct dirent* readdir(DIR* dirp);

int getdents(DIR* dirp, void*, size_t count);
int closedir(DIR* dirp);

#endif
