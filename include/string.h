/**
 * @file string.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __STRING_H
#define __STRING_H

#include <sys/defs.h>

void* memset(void* space, int value, size_t bytes);
bool memcmp(const void*, const void*, size_t);
void* memcpy(void*, const void*, size_t);
void* strcpy(char*, const char*);
bool strcmp(const char*, const char*);
bool strncmp(const char* a, const char* b, int n);
char* strcat(char*, const char*);
int index(const char* s, int c);
const char* strchr(const char* s, int c);
size_t strlen(const char*);
char* strsep(char**, const char*);
char* strdup(char*);
void reverse(char* str); // Reverse in place

// Translations
void numtostr(uint64_t num, uint32_t base, bool sign);
int dbltostr(char* dest, double num);

#endif /*_STRING_H */
