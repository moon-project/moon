/**
 * @file keyboard.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __KEYBOARD_H
#define __KEYBOARD_H

#include <sys/defs.h>

#define GET_SCNCD 0xF0
#define ACK_SCNCD 0xFA
#define RSN_SCNCD 0xFE
#define ENA_SCNCD 0xF4
#define PS2DATA 0x60
#define PS2READ 0x64
#define PS2WRIT PS2READ

extern bool shift;
extern bool ctrl;
extern char scancodes_make[];
extern char scancodes_break[];

#define YIELD_SC_TO_ASCII_MAKE(CODE, ASCII) [CODE] ASCII,
#define YIELD_SC_TO_ASCII_BREAK(CODE, ASCII) [CODE & 0x80] ASCII,

#define FOREACH_SCANCODE(GENERATOR_MACRO)                                                                              \
    GENERATOR_MACRO(0x02, '1')                                                                                         \
    GENERATOR_MACRO(0x03, '2')                                                                                         \
    GENERATOR_MACRO(0x04, '3')                                                                                         \
    GENERATOR_MACRO(0x05, '4')                                                                                         \
    GENERATOR_MACRO(0x06, '5')                                                                                         \
    GENERATOR_MACRO(0x07, '6')                                                                                         \
    GENERATOR_MACRO(0x08, '7')                                                                                         \
    GENERATOR_MACRO(0x09, '8')                                                                                         \
    GENERATOR_MACRO(0x0A, '9')                                                                                         \
    GENERATOR_MACRO(0x0B, '0')                                                                                         \
    GENERATOR_MACRO(0x0C, '-')                                                                                         \
    GENERATOR_MACRO(0x0D, '=')                                                                                         \
    GENERATOR_MACRO(0x0E, '\b')                                                                                        \
    GENERATOR_MACRO(0x0F, '\t')                                                                                        \
    GENERATOR_MACRO(0x10, 'q')                                                                                         \
    GENERATOR_MACRO(0x11, 'w')                                                                                         \
    GENERATOR_MACRO(0x12, 'e')                                                                                         \
    GENERATOR_MACRO(0x13, 'r')                                                                                         \
    GENERATOR_MACRO(0x14, 't')                                                                                         \
    GENERATOR_MACRO(0x15, 'y')                                                                                         \
    GENERATOR_MACRO(0x16, 'u')                                                                                         \
    GENERATOR_MACRO(0x17, 'i')                                                                                         \
    GENERATOR_MACRO(0x18, 'o')                                                                                         \
    GENERATOR_MACRO(0x19, 'p')                                                                                         \
    GENERATOR_MACRO(0x1a, '[')                                                                                         \
    GENERATOR_MACRO(0x1b, ']')                                                                                         \
    GENERATOR_MACRO(0x1c, '\n')                                                                                        \
    /* GENERATOR_MACRO(0x1d, '\xFF')  Left Ctrl */                                                                     \
    GENERATOR_MACRO(0x1e, 'a')                                                                                         \
    GENERATOR_MACRO(0x1f, 's')                                                                                         \
    GENERATOR_MACRO(0x20, 'd')                                                                                         \
    GENERATOR_MACRO(0x21, 'f')                                                                                         \
    GENERATOR_MACRO(0x22, 'g')                                                                                         \
    GENERATOR_MACRO(0x23, 'h')                                                                                         \
    GENERATOR_MACRO(0x24, 'j')                                                                                         \
    GENERATOR_MACRO(0x25, 'k')                                                                                         \
    GENERATOR_MACRO(0x26, 'l')                                                                                         \
    GENERATOR_MACRO(0x27, ';')                                                                                         \
    GENERATOR_MACRO(0x28, '\'')                                                                                        \
    GENERATOR_MACRO(0x29, '`')                                                                                         \
    /* GENERATOR_MACRO(0x2a, '\xFF')  Left Shift */                                                                    \
    GENERATOR_MACRO(0x2b, '\\')                                                                                        \
    GENERATOR_MACRO(0x2c, 'z')                                                                                         \
    GENERATOR_MACRO(0x2d, 'x')                                                                                         \
    GENERATOR_MACRO(0x2e, 'c')                                                                                         \
    GENERATOR_MACRO(0x2f, 'v')                                                                                         \
    GENERATOR_MACRO(0x30, 'b')                                                                                         \
    GENERATOR_MACRO(0x31, 'n')                                                                                         \
    GENERATOR_MACRO(0x32, 'm')                                                                                         \
    GENERATOR_MACRO(0x33, ',')                                                                                         \
    GENERATOR_MACRO(0x34, '.')                                                                                         \
    GENERATOR_MACRO(0x35, '/')                                                                                         \
    GENERATOR_MACRO(0x39, ' ')

typedef enum {
    LSHIFT_MK = 0x2A,
    LSHIFT_BK = 0xAA,
    LCTRL_MK = 0x1D,
    LCTRL_BK = 0x9D,
    ESC_MK = 0x01,
    ESC_BK = 0x81,
    F7_MK = 0x41,
    F7_BK = 0xC1,
    BKSPC_MK = 0x0E,
    BKSPC_BK = 0x8E,
} modkeys;

uint8_t read_scan_code();
char convert_scancode(uint8_t sc);

#endif /* __KEYBOARD_H */
