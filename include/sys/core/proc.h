/**
 * @file proc.h
 * @author Neal Beeken, Mrunal Patel
 * @date 2017-11-20
 * @brief These are the process related functions and structures
 */

#ifndef __PROC_H
#define __PROC_H

#include <sys/core/elf64.h>
#include <sys/core/vfs.h>
#include <sys/defs.h>
/**
 * @brief Task State
 *
 * The current state of the task representing what to do with it next.
 */
typedef enum {
    RUNNABLE, /** Task can be run or is running */
    WAITING, /** Task is waiting on a specific result (e.g, disk) */
    SLEEPING, /** Task is sleeping can be woken up by signal */
    STOPPED, /** Task is intentionally stopped by signal */
    ZOMBIE, /** Task is complete but has not been waited on (potential memory leak since kernel can't drop resources) */
    EXITED /** Task has marked itself as exited */
} task_state_t;

typedef enum { TEXT_SECTION, DATA_SECTION, RODATA_SECTION, BSS_SECTION, HEAP_SECTION, STACK_SECTION } vma_sections_t;

/**
 * @defgroup VM_PERMS Virtual Memory Permissions Group
 * @{
 */
#define ENTRY (void*)0x4000f0
#define STACK_INIT (void*)0x7ffffffff000
#define STACK_INIT_PAGE (void*)0x7fffffffe000
#define HEAP_INIT (void*)0x600000
/** This region is readable by the task */
#define VM_READ 0b0001
/** This region is writable by the task */
#define VM_WRITE 0b0010
/** This region is executable by the task */
#define VM_EXEC 0b0100
/** This region is supposed to grow downward on pagefault */
#define VM_GROWS_DOWN 0b1000

/** Takes a vma pointer and checks to make sure vma is readable */
#define IS_READABLE(vma) (((vma)->permissions) & (VM_READ))
/** Takes a vma pointer and checks to make sure vma is writable */
#define IS_WRITABLE(vma) (((vma)->permissions) & (VM_WRITE))
/** Takes a vma pointer and checks to make sure vma is executable */
#define IS_EXECABLE(vma) (((vma)->permissions) & (VM_EXEC))
/** Takes a vma pointer and checks to see if vma should be grown downward */
#define IS_GROWSDWN(vma) (((vma)->permissions) & (VM_GROWS_DOWN))

/** Takes a vma pointer and sets it to readable */
#define SET_READABLE(vma) (((vma)->permissions) |= (VM_READ))
/** Takes a vma pointer and sets it to writable */
#define SET_WRITABLE(vma) (((vma)->permissions) |= (VM_WRITE))
/** Takes a vma pointer and sets it to executable */
#define SET_EXECABLE(vma) (((vma)->permissions) |= (VM_EXEC))
/** Takes a vma pointer and sets it to vma to be grown downward */
#define SET_GROWSDWN(vma) (((vma)->permissions) |= (VM_GROWS_DOWN))

#define PUSH_CALLEE "push %%rbx \n\t push %%r12 \n\t push %%r13 \n\t push %%r14 \n\t push %%r15 \n\t push %%rbp \n\t"
#define POP_CALLEE  "pop  %%rbp \n\t pop  %%r15 \n\t pop  %%r14 \n\t pop  %%r13 \n\t pop  %%r12 \n\t pop  %%rbx \n\t"

// Will this be current or next?
#define RESTORE_CONTEXT                                                                                                \
    "movq %P[taskrax](%[current]), %%rax\n\t \
                         movq %P[taskrcx](%[current]), %%rcx\n\t \
                         movq %P[taskrdx](%[current]), %%rdx\n\t \
                         movq %P[taskrbx](%[current]), %%rbx\n\t \
                         movq %P[taskrbp](%[current]), %%rbp\n\t \
                         movq %P[taskrsi](%[current]), %%rsi\n\t \
                         movq %P[taskrdi](%[current]), %%rdi\n\t"

/** @} */

/**
 * @brief Virtual Memory Area
 *
 * Responsibilities are the permissions of its VM Area and references
 */
typedef struct vma {
    struct mm* vm_mm; /** Pointer to parent struct */
    void* start; /** Pointer to start of region */
    void* end; /** Pointer to start of region */
    struct vma* next; /** Pointer to next region */
    uint64_t refcount;
    uint8_t permissions;
    vma_sections_t section;
    // file name or file struct
    // char* filename;
} vma_t;

typedef struct mm {
    vma_t* mapping; /** The list of VMAs */
    uintptr_t code_start, code_end; /** The .text section start and end */
    uintptr_t data_start, data_end; /** The .data section start and end */
    uintptr_t brk_start, brk_end; /** The .heap section start and end */
    /** The stack section start (we don't need end b/c we should just auto grow if there's an access beyond it) */
    uintptr_t stack_start, stack_end, stack_mark;
} mm_t;

/**
 * @brief Task struct representing an executable in some state
 *
 * Tasks are the core of the operating system's job at running various jobs.
 * Responsibilities include: managing memory, values needed for context switching,
 * relation and connection to TTY, open file table, pid and state, scheduler list
 */
typedef struct task {
    void* rsp; /** The stack pointer */
    void* cr3;
    void* phys_sp;
    void* tss_rsp;
    task_state_t state; /** The state of the task */
    void* function;
    mm_t* mm; /** The representation of the task's virtual memory space */
    char* name; /** The task name (typically filename) */
    pid_t pid; /** Task ID */
    pid_t ppid; /** The Parent Task ID (0 if kernel was parent) */
    bool foreground; /** true if the process is foreground for its tty */
    uint64_t exit_code;
    FileTable filetable;

    struct task* next; /** next reference needed by scheduler */
    struct task* prev; /** prev reference needed by scheduler */
} task_t;

/**
 * @brief The Head of the task list
 * This is the reference to the head of a
 * circular list of tasks managed by the kernel
 */
extern task_t* tasklisthead;
/**
 * @brief This is the current process being run by the kernel
 * This cursor moves around the circular list in a fashion defined by the scheduler
 */
extern task_t* tasklistcursor;

/**
 * @brief This the highest UNUSED pid.
 * This is a process id that increases by one with every new process
 * TODO: Currently there is no way to reuse process id's
 */
extern pid_t highestpid;

extern void forkret();

task_t* gettask(pid_t pid);

/**
 * @brief Idle task
 *
 * This task does nothing but yield, it is the first item in the process list
 * and it is never removed. Its what the OS does in its down time.
 */
void idle();
void idle1();

/**
 * @brief Context switcher
 * TODO Not Implemented
 * @param to The task to switch to
 * @param from The task which is being switch from
 * @param last TODO
 */
void ctxswitch(task_t* to, task_t* from, task_t** last);

/**
 * @brief Basic Context switcher
 * Saves the general purpose regs on the stack
 * as well as stack pointer and instruction pointer
 *
 * @param to The task to switch to
 * @param from The task which is being switch from
 */
void basic_ctxswitch(task_t* to, task_t* from);

/**
 * @brief Gets the current instruction
 * @return void* the current instruction at the point of calling this function
 */
extern void* get_rip();

/**
 * @brief Switches to a new task
 *
 * This is called either willingly or via timer interrupt
 */
void schedule();
void insert_task(task_t* task);
void remove_task(task_t* task);
task_t* getcurrenttask();
/**
 * @brief Create a new kernel thread.
 *
 * @param name A Name for the thread
 * @param function An address (as value) to the function to be called
 * @return task_t* allocated pointer to the task that can be run
 */
task_t* create_task(void* function);
task_t* copyproc(task_t* parent);
vma_t* load_elf_section(task_t* newtask, void* filebase, Elf64_Shdr* header, bool rw, vma_sections_t section);
/**
 * @brief Gets an mm_t's vma by index
 *
 * @param index The index to iterate too
 * @param head The start of the vma list
 * @param allocate_for_it Specify this if your building the vma and you want to allocate a new one at this index
 * @return vma_t* pointer to the vma that is index depth into the list
 */
vma_t* get_vma_by_index(int index, vma_t* head, bool allocate_for_it);
void insert_vma(task_t* proc, vma_t* vma);
void remove_vma(task_t* proc, vma_t* vma);
void free_vma(vma_t* vma, void* cr3);
/** Starting point for all threads, this will set state to EXITED when
 * thread function returns.
 */
void task_wrapper();
void goto_userspace();

void first_task(task_t* current, task_t* next);
void listprocs();

task_t* make_bin_task(char* filename);

#endif /* __ PROC_H */
