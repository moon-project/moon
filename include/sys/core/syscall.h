/**
 * @file syscall.h
 * @author Neal Beeken, Mrunal Patel
 * @brief Syscall table and entry point
 */

#ifndef __KSYSCALL_H
#define __KSYSCALL_H

#include <sys/core/irq.h>

typedef int (*syscall_f)(interrupt_stack_t*);

syscall_f getsyscall(uint64_t syscall_num);

#endif
