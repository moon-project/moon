/**
 * @file irq.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __IRQ_H
#define __IRQ_H

#include <sys/defs.h>

typedef struct interrupt_stack {
    uint64_t rdi;
    uint64_t rsi;
    uint64_t rbp;
    uint64_t rsp;
    uint64_t rbx;
    uint64_t rdx;
    uint64_t rcx;
    uint64_t rax;
} interrupt_stack_t;

void pic_init();
void pic_ignore_irq(uint8_t irq_line, bool set);
void pic_intr_ack(uint8_t irq_line);
void pit_init();
void idt_install();
void idt_load();

void isr_timer();
void isr_keyboard();

typedef void (*interrupt_handler_fun)(void);

extern interrupt_handler_fun int_routines[256];

#define GENERATE_ENUM(VAL, NUM, FUN) VAL = NUM,
#define GENERATE_STRING(VAL, NUM, FUN) #VAL,
#define GENERATE_STRING_AT_POS(VAL, NUM, FUN) [NUM] #VAL,
#define GENERATE_EXP_FUNC_AT_POS(VAL, NUM, FUN) [NUM] FUN,

/* clang-format off */

#define FOREACH_EXP(GENERATOR_MACRO)                                                                                   \
    GENERATOR_MACRO(DIVISION_BY_ZERO,           0, NULL)                                                               \
    GENERATOR_MACRO(DEBUG,                      1, NULL)                                                               \
    GENERATOR_MACRO(NON_MASKABLE_INT,           2, NULL)                                                               \
    GENERATOR_MACRO(BREAKPOINT,                 3, NULL)                                                               \
    GENERATOR_MACRO(INTO_DETECTED_OVERFLOW,     4, NULL)                                                               \
    GENERATOR_MACRO(OUT_OF_BOUNDS,              5, NULL)                                                               \
    GENERATOR_MACRO(INVALID_OPCODE,             6, NULL)                                                               \
    GENERATOR_MACRO(NO_COPROCESSOR,             7, NULL)                                                               \
    GENERATOR_MACRO(DOUBLE_FAULT,               8, NULL)                                                               \
    GENERATOR_MACRO(COPROCESSOR_SEG_OVERRUN,    9, NULL)                                                               \
    GENERATOR_MACRO(BAD_TSS,                   10, NULL)                                                               \
    GENERATOR_MACRO(SEGMENT_NOT_PRESENT,       11, NULL)                                                               \
    GENERATOR_MACRO(STACK_FAULT,               12, NULL)                                                               \
    GENERATOR_MACRO(GENERAL_PROTECTION_FAULT,  13, NULL)                                                               \
    GENERATOR_MACRO(PAGE_FAULT,                14, NULL)                                                               \
    GENERATOR_MACRO(UNKNOWN_INT,               15, NULL)                                                               \
    GENERATOR_MACRO(COPROCESSOR_FAULT,         16, NULL)                                                               \
    GENERATOR_MACRO(ALIGNMENT_CHK,             17, NULL)                                                               \
    GENERATOR_MACRO(MACHINE_CHECK,             18, NULL)

// enum exp_enum { FOREACH_EXP(GENERATE_ENUM) };
// const char* exp_strs[] = { FOREACH_EXP(GENERATE_STRING_AT_POS) };
// interrupt_handler_fun exp_funs[] = { FOREACH_EXP(GENERATE_EXP_FUNC_AT_POS) };

#define FOREACH_IRQ(GENERATOR_MACRO)                                                                                   \
    /* M PIC */                                                                                                        \
    GENERATOR_MACRO(SYSTEM_TIMER,         32, NULL)                                                                    \
    GENERATOR_MACRO(KEYBOARD_CONTROLLER,  33, NULL)                                                                    \
    GENERATOR_MACRO(COM2,                 35, NULL)                                                                    \
    GENERATOR_MACRO(COM1,                 36, NULL)                                                                    \
    GENERATOR_MACRO(LINE_PRINT_TERMINAL2, 37, NULL)                                                                    \
    GENERATOR_MACRO(FLOPPY,               38, NULL)                                                                    \
    GENERATOR_MACRO(LINE_PRINT_TERMINAL1, 39, NULL)                                                                    \
    /* S PIC */                                                                                                        \
    GENERATOR_MACRO(RTC_TIMER,            40, NULL)                                                                    \
    GENERATOR_MACRO(MOUSE_CONTROLLER,     44, NULL)                                                                    \
    GENERATOR_MACRO(MATH_COPROCESSOR,     45, NULL)                                                                    \
    GENERATOR_MACRO(ATA_CH1,              46, NULL)                                                                    \
    GENERATOR_MACRO(ATA_CH2,              47, NULL)
/* clang-format on */

#endif /* __IRQ_H */
