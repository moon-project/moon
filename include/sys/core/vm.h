/**
 * @file vm.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __VM_H
#define __VM_H

#include <sys/defs.h>

#define PHYS_OFFSET (0xFFFFFFUL << (40))
#define PA2VA(phys_addr) (((void*)phys_addr) + one2one_offset)
#define VA2PA(virt_addr) (((void*)virt_addr) - one2one_offset)

#define PGSZ (4 * ONE_KB)
#define PNULL 8
#define TBLENTRIES 512

// To be a physical null address we check if its divisible by PGSZ
#define IS_PNULL(physaddr) ((((uintptr_t)physaddr) % PGSZ) != 0)

typedef struct __attribute__((__packed__)) {
    bool alloc : 1;
    uintptr_t base;
    uint64_t next;
    uint64_t prev;
} physblk_t;

extern physblk_t* phys_blocks;
extern uint64_t physblk_count;
extern size_t one2one_offset;
extern void* heapbase;
;
extern void* heapbrk;
extern void* kpml4;

typedef struct __attribute__((__packed__)) {
    bool present : 1;
    bool rw : 1;
    bool user : 1;
    bool pwt : 1;
    bool pcd : 1;
    bool accessed : 1;
    bool dirty : 1;
    bool pat : 1;
    bool global : 1;
    bool _ : 3;
    uintptr_t base : 40;
    uint16_t __ : 11;
} va_entry_t;

#define VA_USER 0b100
#define VA_RW 0b010

#define GETBASE(base) (((uint64_t)base) << 12)
#define SETBASE(base) (((uint64_t)base) >> 12)
#define GETENTRY(offset) (*(va_entry_t*)(uint64_t)(offset))

uint64_t add_blocks(uint64_t start, uint64_t length, uint64_t* physfree);
void init_allocator(uintptr_t physbase, uintptr_t physfree);
uintptr_t phys_alloc(size_t numpgs);
void phys_free(uintptr_t base);
void init_vm(uintptr_t physbase, void* virtbase);
void* create_page_table();

uintptr_t map_va(void* addr, va_entry_t (*pml4)[TBLENTRIES], uint8_t flags);
uintptr_t va_is_mapped(void* addr, va_entry_t (*pml4_table)[TBLENTRIES]);

uintptr_t map_va2pa(void* addr, va_entry_t (*pml4)[TBLENTRIES], uintptr_t physpage);

uintptr_t unmap_va(void* addr, va_entry_t (*pml4)[TBLENTRIES]);
int va_user(void* addr, va_entry_t (*pml4)[TBLENTRIES], bool set);
int va_rw(void* addr, va_entry_t (*pml4)[TBLENTRIES], bool set);
void* valloc();
void vfree(void* ptr);

void debug_walk(void* addr, va_entry_t (*pml4)[TBLENTRIES]);

void* get_kern_heapbrk();
void* get_kern_heapbase();

void page_fault_handler(void* fault_addr, int info);

#endif /* __VM_H */
