/**
 * @file kutils.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __KUTILS_H
#define __KUTILS_H

#include <sys/core/screen.h>
#include <sys/defs.h>

#define ONE_TB 0x10000000000
#define ONE_GB 0x40000000
#define ONE_MB 0x100000
#define ONE_KB 0x400

#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define IS_DIVISIBLE_BY(denom, numer) ((uint64_t)(((uint64_t)numer) % ((uint64_t)denom) == 0))
#define FLOOR(to, val) ((uint64_t)(((uint64_t)val) - (((uint64_t)val) % (to))))
#define CEIL(to, val)                                                                                                  \
    ((uint64_t)(IS_DIVISIBLE_BY(to, ((uint64_t)val)) && ((uint64_t)val) != 0) ? ((uint64_t)val)                        \
                                                                              : FLOOR(to, ((uint64_t)val) + (to)))
// Casts to void* to ensure no pointer arithmetic magic
#define BYBYTE(ptr, expr) ((void*)((uintptr_t)ptr)expr)
// returns ptr to the end of a string
#define endofstr(string) ((char*)(string) + k_strlen((string)) - 1)
#define emptystr(string) ((*string) == '\0')

#define NL "\n"

#define KNRM "\e[s0f7b0]"
#define KBLU "\e[s1f1b0]"
#define KGRN "\e[s1f2b0]"
#define KCYN "\e[s1f3b0]"
#define KRED "\e[s1f4b0]"
#define KMAG "\e[s1f5b0]"
#define KYEL "\e[s1f6b0]"
#define KWHT "\e[s1f7b0]"
#define KBWN "\e[s0f6b0]"

#define _PRINT(level, fmt, ...)                                                                                        \
    do {                                                                                                               \
        kprintf(level "%s:%d: " KNRM fmt NL, __extension__ __FUNCTION__, __LINE__, ##__VA_ARGS__);                     \
    } while (0)

#define kdebug(S, ...) _PRINT(KMAG "", S, ##__VA_ARGS__)
#define kinfo(S, ...) _PRINT(KBLU "", S, ##__VA_ARGS__)
#define kwarn(S, ...) _PRINT(KYEL "", S, ##__VA_ARGS__)
// Success
#define ksuccess(S, ...) _PRINT(KGRN "", S, ##__VA_ARGS__)
// Failure
#define kerror(S, ...) _PRINT(KRED "", S, ##__VA_ARGS__)

#define panicif(condition)                                                                                             \
    do {                                                                                                               \
        if ((condition)) {                                                                                             \
            panics(KRED "PANICKED! because: " TOSTRING(condition) " is not true " AT KNRM "\n");                       \
        }                                                                                                              \
    } while (0)

void k_bzero(void* s, size_t n);
void k_memset(void* s, uint8_t value, size_t n);
void k_memcpy(void* dst, void* src, size_t n);
bool k_memcmp(void* dst, void* src, size_t n);

size_t k_strlen(const char* str);
void k_numtostr(uint64_t num, uint32_t base, bool sign);
bool k_strtonum(char* str, uint64_t* num, int base);
void k_strcpy(char* dest, char* src);
bool k_strcmp(char* dest, char* src);
char* k_strsep(char** stringp, const char* delim);
size_t k_strccount(char* str, char c);
char* k_strstr(char* searchspace, char* key);
void k_strcat(char* buffer, char* str);

void kprintf(const char* fmt, ...);
void ktitle(char* title);

char k_toupper(char c);
char k_tolower(char c);
bool k_ispunct(char c);
bool k_isdigit(char c);
bool k_isalpha(char c);

void* kalloc(size_t size);
void kfree(void* ptr);
void* krealloc(void* ptr, size_t newsize);

char* k_basename(char* fullpath);

int64_t k_pow(int64_t base, int64_t exponent);

#endif
