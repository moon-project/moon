/**
 * @file pcie.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __PCIE_H
#define __PCIE_H

#include <sys/defs.h>

#define FOUR_BYTE_ALIGN(val) ((val)&0xFC)
#define CONFIG_ADDRESS 0xCF8
/* This is where to put the data you want sent to the device */
#define CONFIG_DATA 0xCFC

typedef struct __attribute__((packed)) {
    uint32_t vendor_id : 16;
    uint32_t device_id : 16;
    uint32_t command : 16;
    uint32_t status : 16;
    uint32_t rev_id : 8;
    uint32_t prog_if : 8;
    uint32_t subclass : 8;
    uint32_t class_code : 8;
    uint32_t cacheline_sz : 8;
    uint32_t latency_timer : 8;
    uint32_t header_type : 8;
    uint32_t bist : 8;
} pcie_header_t;

typedef struct __attribute__((packed)) {
    pcie_header_t pcie_header;
    uint32_t bar[6];
    uint32_t cardbus_cis_ptr;
    uint32_t subsystem_vendor_id : 16;
    uint32_t subsystem_id : 16;
    uint32_t exp_rom_base_addr;
    uint32_t capability_ptr : 8;
    uint32_t _reserved : 24;
    uint32_t __reserved;
    uint32_t int_line : 8;
    uint32_t int_pin : 8;
    uint32_t min_grant : 8;
    uint32_t max_latency : 8;
} pcie_header0_t;

uint32_t pcie_readconfig(uint8_t, uint8_t, uint8_t, uint8_t);
void pcie_writeconfig(uint8_t, uint8_t, uint8_t, uint8_t, uint32_t);
void pcie_scanbus();

#endif /* __PCIE_H */

/*
|          |31 ------------------------------------------- 0|
| register | bits 31-24 | bits 23-16 | bits 15-8 | bits 7-0 |
|    00    |        Device ID        |       Vendor ID      |
|    04    |         Status          |        Command       |
|    08    | Class code |  Subclass  |  Prog IF  |  Rev ID  |
|    0C    |    BIST    |   Header   |  Latency  | Cache Sz |
|    10    |               Base address #0 (BAR0)           |
|    14    |               Base address #1 (BAR1)           |
|    18    |               Base address #2 (BAR2)           |
|    1C    |               Base address #3 (BAR3)           |
|    20    |               Base address #4 (BAR4)           |
|    24    |               Base address #5 (BAR5)           |
|    28    |                 Cardbus CIS Pointer            |
|    2C    |        Subsystem ID     |  Subsystem Vendor ID |
|    30    |           Expansion ROM base address           |
|    34    |               Reserved              | Cap Ptr  |
|    38    |                      Reserved                  |
|    3C    |   Max lat  | Min Grant  |  Int PIN  | Int Line |
*/
