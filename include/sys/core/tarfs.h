/**
 * @file tarfs.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __TARFS_H
#define __TARFS_H

#include <sys/core/vfs.h>
#include <sys/defs.h>

extern char _binary_tarfs_start;
extern char _binary_tarfs_end;

#define FBLKSZ 512

#define TMAGIC "ustar" /* ustar and a null */
#define TMAGICLEN 5 /* length of ustar string (its null terminated) */
#define TVERSION "00" /* 00 and no null */
#define TVERSIONLEN 2 /* length of version string (its not null terminated) */

enum filetype {
    /** regular file */
    REGTYPE = '0',
    /** old? regular file */
    AREGTYPE = '\0',
    /** link */
    LNKTYPE = '1',
    /** symbolic link */
    SYMTYPE = '2',
    /** character special */
    CHRTYPE = '3',
    /** block special */
    BLKTYPE = '4',
    /** directory */
    DIRTYPE = '5',
    /** FIFO */
    FIFOTYPE = '6',
    /** reserved */
    CONTTYPE = '7'
};

typedef struct posix_header_ustar {
    char name[100];
    char mode[8];
    char uid[8];
    char gid[8];
    char size[12];
    char mtime[12];
    char checksum[8];
    char typeflag[1];
    char linkname[100];
    char magic[6];
    char version[2]; // should be 0x3030
    char uname[32];
    char gname[32];
    char devmajor[8];
    char devminor[8];
    char prefix[155];
    char pad[12];
} tar_header_t;

typedef struct {
    char fullpath[101];
    uint8_t mode;
    size_t size;

    uint32_t modified_time;

    enum filetype type;

    char linkto[100];

    uint32_t uid;
    uint32_t gid;
    char uname[32];
    char gname[32];

    void* data;
} tarfile_t;

extern superblock_t* tar_filesystem;

void init_tarfs();

void tarfile_to_vfs_inode(tarfile_t* file, vfs_inode_t* inode);
bool tarheader_to_tarfile(tar_header_t* tarheader, tarfile_t* file);
bool tar_integrity_check(tar_header_t* tarheader);
vfs_interface_t tarfs_interface();

extern void* tarfs_start;
extern void* tarfs_end;

#endif
