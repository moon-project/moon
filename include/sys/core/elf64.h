/**
 * @file elf64.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef _ELF64_H
#define _ELF64_H

#include <sys/defs.h>

#define EI_NIDENT 16

typedef uint64_t Elf64_Addr;
typedef uint16_t Elf64_Half;
typedef uint64_t Elf64_Lword;
typedef uint64_t Elf64_Off;
typedef uint32_t Elf64_Sword;
typedef uint64_t Elf64_Sxword;
typedef uint32_t Elf64_Word;
typedef uint64_t Elf64_Xword;

enum {
    PT_NULL = 0x00000000,
    PT_LOAD = 0x00000001,
    PT_DYNAMIC = 0x00000002,
    PT_INTERP = 0x00000003,
    PT_NOTE = 0x00000004,
    PT_SHLIB = 0x00000005,
    PT_PHDR = 0x00000006,
    PT_LOOS = 0x60000000,
    PT_HIOS = 0x6FFFFFFF,
    PT_LOPROC = 0x70000000,
    PT_HIPROC = 0x7FFFFFFF
} seg_type;

enum {
    SHT_NULL = 0x0,
    SHT_PROGBITS = 0x1,
    SHT_SYMTAB = 0x2,
    SHT_STRTAB = 0x3,
    SHT_RELA = 0x4,
    SHT_HASH = 0x5,
    SHT_DYNAMIC = 0x6,
    SHT_NOTE = 0x7,
    SHT_NOBITS = 0x8,
    SHT_SHLIB = 0xA,
    SHT_DYNSYM = 0xB,
    SHT_INIT_ARRAY = 0xE,
    SHT_FINI_ARRAY = 0xF,
    SHT_PREINIT_ARRAY = 0x10,
    SHT_GROUP = 0x11,
    SHT_SYMTAB_SHNDX = 0x12,
    SHT_NUM = 0x13,
    SHT_LOOS = 0x60000000
} section_type;

enum {

    SHF_WRITE = 0x1,
    SHF_ALLOC = 0x2,
    SHF_EXECINSTR = 0x4,
    SHF_MERGE = 0x10,
    SHF_STRINGS = 0x20,

    SHF_INFO_LINK = 0x40,
    SHF_LINK_ORDER = 0x80,
    SHF_OS_NONCONFORMING = 0x100,
    SHF_GROUP = 0x200,
    SHF_TLS = 0x400,
    SHF_MASKOS = 0x0ff00000,
    SHF_MASKPROC = 0xf0000000,
    SHF_ORDERED = 0x4000000,
    SHF_EXCLUDE = 0x8000000
} section_permissions;

/** The ELF header defines whether to use 64-bit addresses.
 * The header contains three fields that are affected by this setting
 * and offset other fields that follow them. The ELF header is 64 bytes long. */
typedef struct {
    /** 0x7F followed by ELF(45 4c 46) in ASCII;
     * these four bytes constitute the magic number. */
    unsigned char e_ident[EI_NIDENT];

    /** 1, 2, 3, 4 specify whether the object is
     * relocatable, executable, shared, or core, respectively. */
    Elf64_Half e_type;

    /** Specifies target instruction set architecture. 0x3E for x86_64 */
    Elf64_Half e_machine;

    /** Set to 1 for the original version of ELF. */
    Elf64_Word e_version;

    /** This is the memory address of the entry point
     * from where the process starts executing.
     * This field is either 32 or 64 bits long depending
     * on the format defined earlier. */
    Elf64_Addr e_entry;

    /** Points to the start of the program header table.
     * It usually follows the file header immediately,
     * making the offset 0x34 or 0x40 for 32- and
     * 64-bit ELF executables, respectively. */
    Elf64_Off e_phoff;

    /** Points to the start of the section header table. */
    Elf64_Off e_shoff;

    Elf64_Word e_flags;

    /** Contains the size of this header, normally 64 Bytes
     * for 64-bit and 52 Bytes for 32-bit format. */
    Elf64_Half e_ehsize;

    /** Contains the size of a program header table entry. */
    Elf64_Half e_phentsize;

    /** Contains the number of entries in the program header table. */
    Elf64_Half e_phnum;

    /** Contains the size of a section header table entry. */
    Elf64_Half e_shentsize;

    /** Contains the number of entries in the section header table. */
    Elf64_Half e_shnum;

    /** Contains index of the section header
     * table entry that contains the section names. */
    Elf64_Half e_shstrndx;
} Elf64_Ehdr;

/** The program header table tells the system how
 *  to create a process image. It is found at file offset e_phoff,
 *  and consists of e_phnum entries, each with size e_phentsize.
 */
typedef struct {
    /** Identifies the type of the segment. */
    Elf64_Word p_type;
    Elf64_Word p_flags;
    Elf64_Off p_offset;
    Elf64_Addr p_vaddr;
    Elf64_Addr p_paddr;
    Elf64_Xword p_filesz;
    Elf64_Xword p_memsz;
    Elf64_Xword p_align;
} Elf64_Phdr;

typedef struct {
    Elf64_Word sh_name;
    Elf64_Word sh_type;
    Elf64_Word sh_flags;
    Elf64_Addr sh_addr;
    Elf64_Off sh_offset;
    Elf64_Xword sh_size;
    Elf64_Word sh_link;
    Elf64_Word sh_info;
    Elf64_Xword sh_addralign;
    Elf64_Xword sh_entsize;
} Elf64_Shdr;

typedef struct {

    Elf64_Ehdr* elf_hdr;

    uint16_t prog_hdrs_ct;
    Elf64_Phdr* prog_hdrs;

    uint16_t section_table_ct;
    Elf64_Shdr* section_table;

    void* shstrtab_base;

    Elf64_Shdr* text_hdr;
    void* text;
    Elf64_Shdr* rodata_hdr;
    void* rodata;
    Elf64_Shdr* data_hdr;
    void* data;
    Elf64_Shdr* bss_hdr;
    void* bss;
} elf_t;

void readelf(void* data, elf_t* elf);

#endif
