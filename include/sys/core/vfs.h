/**
 * @file vfs.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __VFS_H
#define __VFS_H

#include <sys/defs.h>

#define FS_BLOCK_SZ 4096

enum fs { STDBUFFFS, TARFS, LUNAFS };

typedef struct vfs_interface {
    int (*open_f)(char*, int, mode_t);
    int (*close_f)(int);
    int (*access_f)(const char* filename, int how);

    int (*create_f)(int, mode_t);
    int (*unlink_f)(char*);

    int (*read_f)(int, void*, size_t);
    int (*write_f)(int, void*, size_t);

    int (*lseek_f)(int, off64_t, int);

    int (*dup2_f)(int, int);

    int (*mkdir_f)(char*, mode_t);
    int (*rmdir_f)(char* filename);
    int (*rename_f)(const char* oldname, const char* newname);

    int (*opendir_f)(char*);
    dirent_t* (*getdents_f)(int, dirent_t*, size_t);
    int (*closedir_f)(DIR*);

} vfs_interface_t;

typedef char datablock[FS_BLOCK_SZ];
typedef datablock* (*single_indirect)[12];
typedef single_indirect* (*double_indirect)[12];

typedef struct vfs_inode {
    uint8_t mode;
    struct sb* superblock;
    size_t size;
    /* https://goo.gl/phyKux */
    uint16_t filemode;
    size_t links;
    bool is_dir;
    char name[100];
    union {
        // If its a file then its 12 pointers to 512 sectors of data
        datablock* data_ptrs[20];
        // If its a directory then its 12 pointers to files/dirs inside of it
        struct vfs_inode* dirents[20];
    } datafields;
#define data_ptrs datafields.data_ptrs
#define dirents datafields.dirents

    void** moredata; // TODO: Working progress
} vfs_inode_t;

/**
 * @brief FS Metadata
 *
 * This contains all meta data pertaining to a specific File System
 * necessary for the VFS to interface with it.
 */
typedef struct sb {
    /** Identifier for device */
    uint8_t device;
    /** Inode pointer */
    vfs_inode_t* root;
    /** Block size data is stored in */
    size_t size;
    /** functional interface with FS */
    vfs_interface_t interface;
    /** FS type */
    enum fs type;
    /** FS specific data  */
    void* fs_meta;
} superblock_t;

typedef struct {
    vfs_inode_t* inodes[30];
    size_t inode_count;
    size_t maxusedfd;
    size_t firstfreefd;
} FileTable;

int get_free_entry(vfs_inode_t* node);
void insert_inode(superblock_t* sbp, char* fullpath, vfs_inode_t* inode);
vfs_inode_t* find_inode(superblock_t* sbp, char* fullpath);

#endif /* __VFS_H */
