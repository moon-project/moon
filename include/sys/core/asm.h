/**
 * @file asm.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __ASM_H
#define __ASM_H

#include <sys/defs.h>

static inline void enable_interrupts() {
    asm volatile("sti");
}

static inline void disable_interrupts() {
    asm volatile("cli");
}

static inline void wport8(uint16_t port, uint8_t value) {
    asm volatile("outb %0,%1" : : "a"(value), "Nd"(port));
}

static inline void wport16(uint16_t port, uint16_t value) {
    asm volatile("outw %0,%1" : : "a"(value), "Nd"(port));
}

static inline void wport32(uint16_t port, uint32_t value) {
    asm volatile("outl %0,%1" : : "a"(value), "Nd"(port));
}

static inline uint8_t rport8(uint16_t port) {
    uint8_t value;
    asm volatile("inb %1, %0" : "=a"(value) : "Nd"(port));
    return value;
}

static inline uint16_t rport16(uint16_t port) {
    uint16_t value;
    asm volatile("inw %1, %0" : "=a"(value) : "Nd"(port));
    return value;
}

static inline uint32_t rport32(uint16_t port) {
    uint32_t value;
    asm volatile("inl %1, %0" : "=a"(value) : "Nd"(port));
    return value;
}

#define intr_test(x) asm volatile("int $" #x)

static inline void interrupt(uint8_t int_num) {
    asm volatile("int %0" : : "X"(int_num));
}

static inline void setcr3(uint64_t ptr) {
    asm volatile("movq %0, %%cr3\n\t" : : "r"(ptr) :);
}

static inline uint64_t getcr3() {
    uint64_t ptr;
    asm volatile("movq %%cr3, %0\n\t" : "=X"(ptr)::);
    return ptr;
}

static inline uint64_t getcr2() {
    uint64_t ptr;
    asm volatile("movq %%cr2, %0\n\t" : "=X"(ptr)::);
    return ptr;
}
#endif /* __ASM_H */
