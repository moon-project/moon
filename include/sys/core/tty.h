/**
 * @file tty.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __TTY_H
#define __TTY_H

#include <sys/core/screen.h>
#include <sys/core/vfs.h>

typedef struct {
    char* buffer;
    size_t tail;
    size_t head;
    void (*flush)();
} stdbuff_t;

extern superblock_t* stdbuff_sb;
extern vfs_inode_t* stdin_inode;
extern vfs_inode_t* stdout_inode;
extern vfs_inode_t* stderr_inode;

extern stdbuff_t stdin;
extern stdbuff_t stdout;
extern stdbuff_t stderr;

extern pid_t fg_pid;

void input(char c);
void output(char c, bool is_error);

void init_tty();

#endif /* __TTY_H */
