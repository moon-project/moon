/**
 * @file init.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __INIT_H
#define __INIT_H

#include "../defs.h"

typedef struct {
    uint8_t* initial_stack;
    uint32_t* loader_stack;
    void* physbase;
    void* physfree;
    uint32_t* modulep;
    void* virtbase;
} init_args_t;

typedef void (*init_f)(const init_args_t const*);

void init_io(const init_args_t const*);
void init_cpu(const init_args_t const*);
void init_dev(const init_args_t const*);
void init_ram(const init_args_t const*);
void init_fs(const init_args_t const*);
void init_proc(const init_args_t const*);

#define GENERATE_FUNCS(FUN_P) FUN_P,
#define GENERATE_STRS(FUN_P) #FUN_P,
#define GENERATE_COUNT(_) +1

/* Order matters!! */
#define FOREACH_INIT(GENERATOR_MACRO)                                                                                  \
    GENERATOR_MACRO(init_cpu) /* Interrupts, Timer, IDT */                                                             \
    GENERATOR_MACRO(init_ram) /* Virtual Memory */                                                                     \
    GENERATOR_MACRO(init_io) /* VGA screen, keyboard, terminal */                                                      \
    /* GENERATOR_MACRO(init_dev) PCIe Devices */                                                                       \
    GENERATOR_MACRO(init_fs) /* File system */                                                                         \
    GENERATOR_MACRO(init_proc) /* Processes */

#endif /* __INIT_H */
