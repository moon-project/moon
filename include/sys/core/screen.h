/**
 * @file screen.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __SCREEN_H
#define __SCREEN_H

#include <sys/core/vm.h>
#include <sys/debug.h>
#include <sys/defs.h>

/* http://wiki.osdev.org/Text_UI#Colours */

#define BOLDIFY 0x8

#define VGA_KBLACK 0x0
#define VGA_KBLUE 0x1
#define VGA_KGREEN 0x2
#define VGA_KCYAN 0x3
#define VGA_KRED 0x4
#define VGA_KMAGENTA 0x5
#define VGA_KBROWN 0x6
#define VGA_KGRAY 0x7

#define VGA_KDARKGRAY (VGA_KBLACK + BOLDIFY)
#define VGA_KBRIGHTBLUE (VGA_KBLUE + BOLDIFY)
#define VGA_KBRIGHTGREEN (VGA_KGREEN + BOLDIFY)
#define VGA_KBRIGHTCYAN (VGA_KCYAN + BOLDIFY)
#define VGA_KBRIGHTRED (VGA_KRED + BOLDIFY)
#define VGA_KBRIGHTMAGENTA (VGA_KMAGENTA + BOLDIFY)
#define VGA_KYELLOW (VGA_KBROWN + BOLDIFY)
#define VGA_KWHITE (VGA_KGRAY + BOLDIFY)

#define LUNABYTSEQLEN 8
#define OPENBRACK_OFF 1
#define CLOSBRACK_OFF 8

#define TYPE_OFF 2
#define S_OFF 3
#define FG_OFF 5
#define BG_OFF 7
#define BD_OFF 3

#define XTENS_OFF 3
#define XONES_OFF 4
#define YTENS_OFF 6
#define YONES_OFF 7

#define SCREEN_COL 80
#define SCREEN_ROW 24
//#define SCREEN_BASE ((void*)0xB8000)
#define SCREEN_BASE ((void*)0xFFFFFF00000B8000UL)

#define USER_TERM 0
#define KERN_TERM 1
#define TERMS_COUNT 2

typedef struct __attribute__((__packed__)) {
    uint8_t fg : 4;
    uint8_t bg : 4;
} color_t;

typedef struct __attribute__((__packed__)) {
    unsigned char glyph;
    color_t color;
} lunabytes_t;

typedef struct {
    size_t col;
    size_t row;
} cursor_t;

typedef struct {
    char* name;
    cursor_t cursor;
    color_t color;
    lunabytes_t screen[SCREEN_ROW][SCREEN_COL];
} terminal_t;

extern terminal_t* current_terminal;
extern lunabytes_t (*real_screen)[SCREEN_ROW][SCREEN_COL];

#define COLOR(fg_clr, bg_clr) ((color_t){ .fg = (fg_clr), .bg = (bg_clr) })
#define CURSOR(row_p, col_p) ((cursor_t){ .row = (row_p), .col = (col_p) })
#define CHAR2ANSI(glyph_p, clr_p) ((lunabytes_t){ .glyph = (glyph_p), .color = (clr_p) })

#define WONB COLOR(VGA_KGRAY, VGA_KBLACK)

void put_at_on(lunabytes_t bytes, cursor_t* location, terminal_t* terminal);
void put_on(lunabytes_t bytes, terminal_t* terminal);
void putc(char glyph);
void puts(char* glyph_str);

void esc(char* string, terminal_t* terminal);
void scroll(terminal_t* terminal);
void update_cursor(terminal_t* terminal, cursor_t* location, char c);
void display(terminal_t* terminal);
void backspace();

void update_clock();
void print_clock();
void panic_notification();

void moon_os();

void __attribute__((noreturn)) panic();
void __attribute__((noreturn)) panics(char* msg);

void echokey(char c);

#endif /* _SCREEN_H */
