/**
 * @file fcntl.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __FCNTL_H
#define __FCNTL_H

#define O_RDONLY 00
#define O_WRONLY 01
#define O_RDWR 02

#define S_IRWXU 0000700 /* RWX mask for owner */
#define S_IRUSR 0000400 /* R for owner */
#define S_IWUSR 0000200 /* W for owner */
#define S_IXUSR 0000100 /* X for owner */

#define O_CLOEXEC 02000000

#endif /* __FCNTL_H */
