/**
 * @file debug.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __DEBUG_H
#define __DEBUG_H

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define AT __FILE__ " at line " TOSTRING(__LINE__)

#define CLR_NRM "\e[s0f7b0]"
#define CLR_BLU "\e[s1f1b0]"
#define CLR_GRN "\e[s1f2b0]"
#define CLR_CYN "\e[s1f3b0]"
#define CLR_RED "\e[s1f4b0]"
#define CLR_MAG "\e[s1f5b0]"
#define CLR_YEL "\e[s1f6b0]"
#define CLR_WHT "\e[s1f7b0]"
#define CLR_BWN "\e[s0f6b0]"

#endif