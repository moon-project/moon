/**
 * @file defs.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __DEFS_H
#define __DEFS_H

/* clang-format off */
/* operators */
#define and &&
#define and_eq &=
#define bitand &
#define bitor |
#define compl ~
#define not !
#define not_eq !=
#define or ||
#define or_eq |=
#define xor ^
#define xor_eq ^=
/* clang-format on */

/* Null */
#define NULL ((void*)0)

#define NAME_MAX 255

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

/* offsetof */
#define offsetof(TYPE, MEMBER) ((size_t) & ((TYPE*)0)->MEMBER)

/* Boolean */
typedef enum { false = 0, true = 1 } bool;

/* Ints */
typedef unsigned long uint64_t;
typedef long int64_t;
typedef unsigned int uint32_t;
typedef int int32_t;
typedef unsigned short uint16_t;
typedef short int16_t;
typedef unsigned char uint8_t;
typedef char int8_t;

typedef int64_t intptr_t;
typedef uint64_t uintptr_t;

/* Sizeof */
typedef uint64_t size_t;
typedef int64_t ssize_t;

/* Offset */
typedef uint64_t off_t;

/* Time */
typedef struct {
    uint8_t sec;
    uint8_t min;
    uint8_t hrs;
} clock_t;

/* Process */
typedef uint32_t pid_t;

/* File system */
typedef uint16_t mode_t;
typedef uint64_t ino64_t;
typedef uint64_t off64_t;
typedef struct file {
    int fd;
} FILE;

typedef enum { DT_UNKNOWN, DT_REG, DT_DIR, DT_FIFO, DT_LNK } dentry_type;

typedef struct dirent {
    long d_ino;
    off_t d_off;
    unsigned short d_reclen;
    dentry_type d_type;
    char d_name[NAME_MAX + 1];
} dirent_t;

typedef struct DIR {
    int fd;
} DIR;

/* Signals */
typedef void (*sighandler_t)(int);
typedef uint32_t sigset_t;

#endif
