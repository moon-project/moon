/**
 * @file syscall.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */
#ifndef __SYSCALL_H
#define __SYSCALL_H

#include "defs.h"

/* clang-format off */
#define SYS_READ            0
#define SYS_WRITE           1
#define SYS_OPEN            2
#define SYS_CLOSE           3
#define SYS_STAT            4
#define SYS_FSTAT           5
#define SYS_BRK             12
#define SYS_RTSIGACTION     13
#define SYS_SIGPROGMASK     14
#define SYS_SIGRETURN       13
#define SYS_IOCTL           16
#define SYS_ACCESS          21
#define SYS_YIELD           24
#define SYS_DUP2            33
#define SYS_SLEEP           35
#define SYS_GETPID          39
#define SYS_FORK            57
#define SYS_EXEC            59
#define SYS_EXIT            60
#define SYS_WAIT            61
#define SYS_KILL            62
#define SYS_GETDENTS        78
#define SYS_GETCWD          79
#define SYS_CHDIR           80
#define SYS_RENAME          82
#define SYS_MKDIR           83
#define SYS_RMDIR           84
#define SYS_CREATE          85
#define SYS_UNLINK          87
#define SYS_GETRUSAGE       98
#define SYS_GETPPID         110
#define SYS_REBOOT          169
#define SYS_PIPE2           293

/* Processes */

void     sys_exit(int status);
pid_t    sys_getpid(void);
pid_t    sys_getppid(void);
void*    sys_brk(void* addr);
int      sys_pipe(int fds[2]);
int      sys_pipe2(int fds[2], int flags);
int      sys_execve(char* program_path, char* const argv[], char* const envp[]);
uint32_t sys_sleep(uint32_t seconds);
pid_t    sys_fork();
int      sys_wait(pid_t pid, int* status, int options);
int      sys_dup2(int oldfd, int newfd);
int      sys_chdir(const char* path);
size_t   sys_getcwd(char* buf, size_t size);
int      sys_yield();

/* Signal */

int sys_kill(pid_t pid, int sig);
int sys_sigprocmask(int how, const sigset_t* set, sigset_t* oldset);

/* File System */

int sys_access(const char* pathname, int mode);
int sys_open(const char* filename, int flags, int mode);
int sys_close(int fd);
int sys_ioctl(int fd);
int sys_unlink(const char* pathname);
int sys_getdents(DIR* dirp, void* entries, size_t size);

/* I/O */

ssize_t sys_read(int fd, void* buf, size_t count);
ssize_t sys_write(int fd, const void* buf, size_t count);

/* clang-format on */

#endif /* __SYSCALL_H */
