/**
 * @file unistd.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __UNISTD_H
#define __UNISTD_H

#include <sys/defs.h>

#define F_OK 0

extern char** environ;

int open_with_mode(const char* pathname, int flags, int mode);
int open(const char* pathname, int flags);
int close(int fd);
ssize_t read(int fd, void* buf, size_t count);
ssize_t write(int fd, const void* buf, size_t count);
int unlink(const char* pathname);

int access(const char* pathname, int mode);

int chdir(const char* path);
char* getcwd(char* buf, size_t size);

pid_t fork();
int execvpe(const char* file, char* const argv[], char* const envp[]);
int exec(const char* file);
pid_t wait(int* status);
int waitpid_with_options(int pid, int* status, int options);
int waitpid(int pid, int* status);
int yield();

uint32_t sleep(uint32_t seconds);

pid_t getpid(void);
pid_t getppid(void);

void* sbrk(intptr_t increment);
void* brk(void* addr);

// OPTIONAL: implement for ``on-disk r/w file system (+10 pts)''
off_t lseek(int fd, off_t offset, int whence);
int mkdir(const char* pathname, mode_t mode);

// OPTIONAL: implement for ``signals and pipes (+10 pts)''
int pipe(int pipefd[2]);
int pipe2(int fds[2], int flags);
int dup2(int oldfd, int newfd);

#endif
