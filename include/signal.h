/**
 * @file signal.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __SIGNAL_H
#define __SIGNAL_H

#include <sys/defs.h>
#include <sys/signum.h>

int kill(pid_t pid, int sig);

// OPTIONAL: implement for ``signals and pipes (+10 pts)''
sighandler_t signal(int signum, sighandler_t handler);

int sigemptyset(sigset_t* set);
int sigfillset(sigset_t* set);
int sigaddset(sigset_t* set, int signum);
int sigdelset(sigset_t* set, int signum);
int sigismember(const sigset_t* set, int signum);
int sigprocmask(int how, const sigset_t* set, sigset_t* oldset);

#endif
