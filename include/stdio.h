/**
 * @file stdio.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __STDIO_H
#define __STDIO_H

#include <sys/defs.h>
#include <sys/stdarg.h>

#define STDIN_FILENO 0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2
#define EOF -1

extern FILE* stdin;
extern FILE* stdout;
extern FILE* stderr;

int putchar(int c);
int puts(const char* s);
void printf(const char* fmt, ...);

char* fgets(char* s, int size, int stream);
char getchar();

char* readline(const char* prompt);

#endif
