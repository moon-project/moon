/**
 * @file stdlib.h
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#ifndef __STDLIB_H
#define __STDLIB_H

#include <sys/defs.h>

int main(int argc, char const* argv[], char* envp[]);

void exit(int status);

char* getenv(const char* name);
int setenv(const char* name, char* value, int overwrite);

void* malloc(size_t size);
void* calloc(size_t members, size_t size);
void free(void* ptr);

#endif
