#! /usr/bin/env python3
import sys
from time import sleep
from itertools import cycle

new = """
                                   _..._
                                 .:::::::.
                                :::::::::::
                                :::::::::::
                            moon`:::::::::'
                            os    `':::''  """

wax_cr = """
                                   _..._
                                 .::::. `.
                                :::::::.  :
                                ::::::::  :
                            moon`::::::' .'
                            os    `'::'-'  """

first_qt = """
                                   _..._
                                 .::::  `.
                                ::::::    :
                                ::::::    :
                            moon`:::::   .'
                            os    `'::.-'  """

wax_gb = """
                                   _..._
                                 .::'   `.
                                :::       :
                                :::       :
                            moon`::.     .'
                            os    `':..-'  """

full = """
                                   _..._
                                 .'     `.
                                :         :
                                :         :
                            moon`.       .'
                            os    `-...-'  """

wan_gb = """
                                   _..._
                                 .'   `::.
                                :       :::
                                :       :::
                            moon`.     .::'
                            os    `-..:''  """

last_qt = """
                                   _..._
                                 .'  ::::.
                                :    ::::::
                                :    ::::::
                            moon`.   :::::'
                            os    `-.::''  """

wan_cr = """
                                   _..._
                                 .' .::::.
                                :  ::::::::
                                :  ::::::::
                            moon`. '::::::'
                            os    `-.::''  """

try:
    phases = [new, wax_cr, first_qt, wax_gb, full, wan_gb, last_qt, wan_cr]
    print('')
    sys.stdout.write('\u001b[2J')
    for p in cycle(phases):
        sys.stdout.write('\u001b[2;0H')
        sleep(0.01)
        sys.stdout.write(p)
        sleep(0.14)
        sys.stdout.write('\u001b[2;0H')
        sleep(0.01)
except KeyboardInterrupt:
    print('')
    print('')
    sys.exit(0)
