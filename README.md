# Moon

## Build
The provided `Makefile`:
1. builds a kernel
2. copies it into `rootfs/boot/kernel/kernel`
3. creates an ISO CD image with the `rootfs/` contents

## Run
To boot the system in QEMU, run:
```
qemu-system-x86_64                                                         \
    -no-reboot                                                             \
    -display curses                                                        \
    -m 128M                                                                \
    -drive id=boot,format=raw,file=$USER.img,if=none                       \
    -drive id=data,format=raw,file=$USER-data.img,if=none                  \
    -device ahci,id=ahci                                                   \
    -device ide-drive,drive=boot,bus=ahci.0                                \
    -device ide-drive,drive=data,bus=ahci.1                                \
    -s -S
```
Explanation of parameters:
* `-curses` use a text console (omit this to use default SDL/VNC console)
* `-cdrom x.iso` connect a CD-ROM and insert CD with x.iso disk
* `-hda x.img` connect a hard drive with x.img disk
* `-gdb tcp::9999` listen for "remote" debugging connections on port NNNN -S wait for GDB to connect at startup
* `-no-reboot` prevent reboot when OS crashes

> :nerd: When using the -curses mode, switch to the `qemu>` console with ESC-2.

## Debug
To connect a remote debugger to the running qemu VM, from a different window: `gdb ./kernel`
At the `(gdb)` prompt, connect with: `target remote localhost:9999`

## Notes:
- `strsep` is only available when _BSD_SOURCE is defined. So makfile now has gnu11 standard.
