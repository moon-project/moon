/**
 * @file io.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/syscall.h>
#include <unistd.h>

static FILE _stdin = { .fd = STDIN_FILENO };
static FILE _stdout = { .fd = STDOUT_FILENO };
static FILE _stderr = { .fd = STDERR_FILENO };

FILE* stdin = &_stdin;
FILE* stdout = &_stdout;
FILE* stderr = &_stderr;

ssize_t read(int fd, void* buf, size_t count) {
    return sys_read(fd, buf, count);
}

ssize_t write(int fd, const void* buf, size_t count) {
    return sys_write(fd, buf, count);
}

int putchar(int c) {
    int wrote = write(STDOUT_FILENO, &c, 1);
    if (wrote < 0) {
        return -1;
    }
    return c;
}

char getchar() {
    char c;
    int readed = read(STDIN_FILENO, &c, 1);
    if (readed < 0) {
        return -1;
    }
    return c;
}

int puts(const char* s) {
    write(STDOUT_FILENO, s, strlen(s));
    // for (; *s; ++s)
    //     if (putchar(*s) != *s)
    //         return EOF;
    // // (putchar('\n') == '\n') ? 0 : EOF
    return 0;
}

char* gets(char* s) {
    return NULL; // DEPRECATED DON'T USE
}

char* fgets(char* s, int size, int stream) {
    int bytes = read(stream, s, size);
    if (bytes < 0) {
        return NULL;
    }
    return s;
}

static char* readlinebuffer = NULL;
char* readline(const char* prompt) {
    puts(prompt);
    if (readlinebuffer == NULL) {
        readlinebuffer = calloc(1, 1024);
    } else {
        memset(readlinebuffer, 0, 1024);
    }
    return fgets(readlinebuffer, 1023, STDIN_FILENO);
}

void printf(const char* fmt, ...) {
    va_list args;
    va_start(args, fmt);
    int num_specifiers = 0;
    int len = strlen(fmt);
    char buf[2];
    char* string;
    void* ptr;

    for (int i = 0; i < len; ++i) {
        if (fmt[i] == '\e') {
            // esc((char*)(fmt + i), current_terminal);
            i += 8;
            continue;
        }
        if (fmt[i] == '%') {
            ++i; // Move i fwd to the specifier
            // Handle fmt
            switch (fmt[i]) {
            case 'c': { // %c - character
                buf[0] = va_arg(args, int);
                buf[1] = '\0';
                puts(buf);
                break;
            }
            case 'b': {
                string = va_arg(args, bool) ? "true" : "false";
                puts(string);
                break;
            }
            case 's': { // %s - string
                string = va_arg(args, char*);
                puts(string);
                break;
            }
            case 'd': { // %d - pass in to knumstr up to 64-bit int
                numtostr(va_arg(args, int32_t), 10, true);
                break;
            }
            case 'x': { // %x - pass in to knumstr up to 64-bit int in hex
                numtostr(va_arg(args, uint64_t), 16, false);
                break;
            }
            case 'o': { // %x - pass in to knumstr up to 64-bit int in hex
                numtostr(va_arg(args, uint64_t), 8, false);
                break;
            }
            case 'p': { // %p - 64-bit pointer proceeded by 0x or (nil) if 0x0
                ptr = va_arg(args, void*);
                if (ptr == 0) {
                    puts("(nil)");
                } else {
                    putchar('0');
                    putchar('x');
                    numtostr((uintptr_t)ptr, 16, false);
                }
                break;
            }
            default: { // Unsupported modifier
                putchar(fmt[i]);
                break;
            }
            }
            ++num_specifiers;
        } else {
            putchar(fmt[i]);
        }
    }
    va_end(args);
    return;
}
