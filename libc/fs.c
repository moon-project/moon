/**
 * @file fs.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <dirent.h>
#include <sys/syscall.h>

int open_with_mode(const char* filename, int flags, int mode) {
    return sys_open(filename, flags, mode);
}

int open(const char* filename, int flags) {
    return sys_open(filename, flags, 0);
}

int close(int fd) {
    return sys_close(fd);
}

int access(const char* pathname, int mode) {
    return sys_access(pathname, mode);
}

int opendir_takes_ptr(DIR* dir, const char* name) {
    dir->fd = open(name, 0);
    if (dir->fd == -1) {
        return -1;
    } else {
        return 0;
    }
}

static DIR directory;
DIR* opendir(const char* name) {
    directory.fd = open(name, 0);
    if (directory.fd == -1) {
        return NULL;
    } else {
        return &directory;
    }
}

struct dirent* readdir(DIR* dirp) {
    return NULL; // TODO
}

int getdents(DIR* dirp, void* entries, size_t size) {
    return sys_getdents(dirp, entries, size);
}

int closedir(DIR* dirp) {
    return close(dirp->fd);
}

int unlink(const char* pathname) {
    return sys_unlink(pathname);
}
