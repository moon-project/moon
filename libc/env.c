/**
 * @file env.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

char** environ;

char* getenv(const char* name) {
    if (name == NULL)
        return NULL;
    char** ptr;
    char* value;
    for (ptr = environ; *ptr; ptr++) {
        int index_of_eq = index(*ptr, '=');
        if (strncmp(name, *ptr, index_of_eq)) {
            value = (*ptr) + index_of_eq + 1;
            return value;
        }
    }
    return NULL;
}

int setenv(const char* name, char* value, int overwrite) {
    if (name == NULL || value == NULL)
        return -1;
    char* pos;
    if ((pos = getenv(name)) != NULL) {
        if (overwrite != 0) {
            strcpy(pos, value);
        }
        return 0;
    }
    // Not found so time to insert
    char** end;
    for (end = environ; *end; end++)
        ; // Get end of environ
    size_t space_needed = strlen(name) + strlen(value) + 2;
    end[0] = calloc(space_needed, sizeof(char));
    strcpy(*end, name);
    strcat(*end, "=");
    strcat(*end, value);
    *(end + 1) = 0; // null terminate env
    return 0;
}
