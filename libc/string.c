/**
 * @file string.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

void* memset(void* space, int value, size_t bytes) {
    assert(space != NULL);
    for (int i = 0; i < bytes; ++i, *(char*)(space + i) = 0)
        ;
    return space;
}

bool memcmp(const void* a, const void* b, size_t bytes) {
    assert(a != NULL);
    assert(b != NULL);
    for (int ct = 0; ct < bytes; ++ct) {
        if (((char*)a)[ct] != ((char*)b)[ct]) {
            return false;
        }
    }
    return true;
}

bool strcmp(const char* a, const char* b) {
    assert(a != NULL);
    assert(b != NULL);
    int alen = strlen(a);
    int blen = strlen(b);
    return alen == blen ? memcmp(a, b, alen) : false;
}

bool strncmp(const char* a, const char* b, int n) {
    assert(a != NULL);
    assert(b != NULL);
    return memcmp(a, b, n);
}

void* memcpy(void* dest, const void* src, size_t bytes) {
    assert(src != NULL);
    assert(dest != NULL);
    for (int ct = 0; ct < bytes; ++ct) {
        ((char*)dest)[ct] = ((char*)src)[ct];
    }
    return dest;
}

void* strcpy(char* dest, const char* src) {
    assert(src != NULL);
    assert(dest != NULL);
    size_t src_len = strlen(src);
    return memcpy(dest, src, src_len);
}

size_t strlen(const char* str) {
    assert(str != NULL);
    char* ptr = (char*)str;
    while (*ptr) {
        ptr++;
    }
    return ptr - str;
}

char* strcat(char* dest, const char* src) {
    assert(src != NULL);
    assert(dest != NULL);
    char* endofdest = (char*)&dest[strlen(dest)];
    int i;
    for (i = 0; i < strlen(src); ++i) {
        endofdest[i] = src[i];
    }
    endofdest[i + 1] = 0;
    return dest;
}

const char* strchr(const char* s, int c) {
    return &(s[index(s, c)]);
}

int index(const char* s, int c) {
    assert(s != NULL);
    assert(c > 0 && c < 128);
    for (const char* ptr = s; *ptr; ++ptr) {
        if (*ptr == c) {
            return ptr - s;
        }
    }
    return -1;
}

char* strsep(char** stringp, const char* delim) {
    assert(stringp != NULL);
    assert(delim != NULL);

    char* string = *stringp;
    char* delims = (char*)delim;
    char* token;
    char cur_char, cur_delim;
    int found = 0;
    if (string == NULL) {
        return NULL;
    }
    token = string;
    while (!found) {
        cur_char = *string;
        string += 1; // Next char in string
        cur_delim = *delims;
        while (cur_delim != '\0') {
            if (cur_char == '\0') {
                string = NULL;
                found = 1;
                break;
            } else if (cur_char == cur_delim) {
                string[-1] = '\0';
                found = 1;
                break; // We found a chunk
            }
            delims += 1; // Next deliminator char
            cur_delim = *delims;
        }
        delims = (char*)delim;
        *stringp = string;
    }
    return token;
}

char* strdup(char* str) {
    assert(str != NULL);

    int i;
    int len = strlen(str);
    char* space = malloc(len + 1);
    for (i = 0; i < len; ++i) {
        space[i] = str[i];
    }
    space[i] = '\0';
    return space;
}

void reverse(char* str) {
    assert(str != NULL);

    int len = strlen(str);
    char* end = &str[len - 1];
    char* start = &str[len - 1];
    while (start != end) {
        char temp = *start;
        *start++ = *end;
        *end-- = temp;
    }
}

    /* Translations */

#define MSB (20 - ndigits)
#define NUM_STR (number_str + MSB)

void numtostr(uint64_t num, uint32_t base, bool sign) {
    char number_str[25];
    memset(number_str, 0, 25);
    int ndigits = 0;

    if (num == 0) {
        puts("0");
        return;
    }

    if (sign && num & (1UL << 63)) {
        putchar('-');
        num = ~num;
        num += 1;
    }

    while (num) {
        ndigits++;
        uint8_t rem = num % base;
        number_str[MSB] = (rem < 10) ? rem + 0x30 : rem + 0x57;
        num /= base;
    }

    puts(NUM_STR);
}
