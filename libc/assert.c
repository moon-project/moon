/**
 * @file assert.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <assert.h>
#include <signal.h>
#include <unistd.h>

void abort() {
    kill(getpid(), SIGABRT);
}
