/**
 * @file alloc.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <assert.h>
#include <string.h>
#include <unistd.h>

#define PAGE_SZ 4096
#define PAD(size) ((16 - ((size) % 16)) % 16)
#define BLK_SZ(size) (PAD(size) + size)

#define SETBLK(size) ((size) >> 4)
#define GETBLK(size) ((size) << 4)
#define BYBYTE(ptr, expr) ((void*)((uintptr_t)ptr)expr)

/* Need at least 8 bytes of space to write to (- 8 at the end) */
#define IS_NOT_BEYOND_HEAP(ptr) ((uintptr_t)(ptr) < (uintptr_t)((uintptr_t)breakpoint - (uintptr_t)8))

#define ALLOCATED 1
#define FREE 0

typedef struct {
    uint64_t alloc : 4;
    uint64_t block : 60;
} __attribute__((packed)) meta_t;

typedef meta_t header_t;
typedef meta_t footer_t;

typedef struct blk {
    meta_t meta;
    struct blk* prev;
    struct blk* next;
} freeblock_t;

static freeblock_t* freelist = NULL;
static void* breakpoint = NULL;
static void* startpoint = NULL;

void* find_block(uint64_t block_sz) {
    freeblock_t* cursor;
    for (cursor = freelist; cursor != NULL; cursor = cursor->next) {
        if (GETBLK(cursor->meta.block) >= block_sz) {
            // Remove from list
            if (cursor->prev)
                cursor->prev->next = cursor->next;
            if (cursor->next)
                cursor->next->prev = cursor->prev;
            freelist = freelist->next;
            return cursor;
        }
    }
    return NULL;
}

void* malloc(size_t size) {
    header_t* header = find_block(BLK_SZ(size));

    if (header == NULL) {
        /* Free list empty */
        breakpoint = sbrk(PAGE_SZ); // Make space!
        if (breakpoint == (void*)-1) // No memory.
            return NULL;
        breakpoint += PAGE_SZ;
        if (startpoint == NULL) {
            startpoint = breakpoint - PAGE_SZ;
        }
        // Starts at bottom, breakpoint is now equal to the beginning of inaccessible memory
        ((freeblock_t*)startpoint)->meta.alloc = FREE;
        ((freeblock_t*)startpoint)->meta.block = SETBLK(PAGE_SZ - 8);
        ((freeblock_t*)startpoint)->prev = NULL;
        ((freeblock_t*)startpoint)->next = NULL;

        footer_t* footer = BYBYTE(startpoint, +GETBLK(((freeblock_t*)startpoint)->meta.block));
        footer->block = SETBLK(PAGE_SZ - 8);
        footer->alloc = FREE;

        freelist = startpoint;
        return malloc(size); // Try again.
    }

    /* Block Found */

    uint64_t old_size = GETBLK(header->block);

    // size + 16 for header & footer BLK_SZ brings us to alignment
    uint64_t new_size = BLK_SZ(size + 16);

    header->block = SETBLK(new_size);
    header->alloc = ALLOCATED;

    // Set footer
    footer_t* footer = BYBYTE(header, +GETBLK(header->block) - 8);
    assert(IS_NOT_BEYOND_HEAP(footer));
    footer->block = SETBLK(new_size);
    footer->alloc = ALLOCATED;

    // Set header for new next block
    freeblock_t* newheader = BYBYTE(header, +GETBLK(header->block));
    assert(IS_NOT_BEYOND_HEAP(newheader));
    newheader->meta.block = SETBLK(old_size - new_size);
    newheader->meta.alloc = FREE;

    // Set footer for next block
    freeblock_t* newfooter = BYBYTE(newheader, +GETBLK(newheader->meta.block) - 8);
    assert(IS_NOT_BEYOND_HEAP(newfooter));
    newfooter->meta.block = SETBLK(old_size - new_size);
    newfooter->meta.alloc = FREE;

    // Update freelist head
    if (freelist != NULL)
        freelist->prev = newheader;
    newheader->next = freelist;
    newheader->prev = NULL;
    freelist = newheader; // Insert at head of list

    return BYBYTE(header, +8);
}

void* calloc(size_t members, size_t size) {
    uint64_t length = members * size;
    void* space = malloc(length);
    memset(space, 0, length);
    return space;
}

void free(void* ptr) {
    if (ptr < startpoint || ptr >= breakpoint)
        return;
    header_t* header = ptr - 8;
    footer_t* footer = (void*)header + GETBLK(header->block) - 8;
    // freeblock_t* header = ptr - 8;
    // freeblock_t* footer = header + GETBLK(header->meta.block) - 8;
    // some checks
    if (header->block != footer->block)
        return;
    // Coalescing
    uint64_t fblk_size = GETBLK(header->block);
    freeblock_t* fblk_header = (void*)header;
    footer_t* fblk_footer = footer;
    footer_t* prev_footer = (void*)header - 8;
    header_t* next_header = (void*)footer + 8;
    // Check if previous block is free
    if (prev_footer->alloc == FREE) {
        freeblock_t* prev_header = (void*)prev_footer - GETBLK(prev_footer->block) + 8;
        // set the prev block as the start of the new free block
        fblk_size += GETBLK(prev_footer->block);
        fblk_header = prev_header;
        // delete it from the free list
        // freelist_del(prev_header);
    }
    // Check if next block is free
    if (next_header->alloc == FREE) {
        // set the next block footer as the end of the new free block
        fblk_size += GETBLK(next_header->block);
        fblk_footer = (void*)next_header + GETBLK(next_header->block) - 8;
        // delete it from the free list
        // freelist_del(prev_header);
    }

    fblk_header->meta.alloc = FREE;
    fblk_footer->alloc = FREE;

    // insert the new block in free list
    // freelist_ins_at_head(fblk_header);
}
