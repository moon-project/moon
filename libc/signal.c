/**
 * @file signal.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <signal.h>
#include <sys/syscall.h>

int kill(pid_t pid, int sig) {
    return sys_kill(pid, sig);
}

int sigemptyset(sigset_t* set) {
    return (*set = 0);
}

int sigfillset(sigset_t* set) {
    return (*set = 0xFFFFFFFF);
}

int sigaddset(sigset_t* set, int signum) {
    *set |= (0x1 << (signum - 1));
    return 0;
}

int sigdelset(sigset_t* set, int signum) {
    *set &= (0x1 << ~(signum - 1));
    return 0;
}

int sigismember(const sigset_t* set, int signum) {
    return (*set & (0x1 << (signum - 1))) != 0;
}

int sigprocmask(int how, const sigset_t* set, sigset_t* oldset) {
    return sys_sigprocmask(how, set, oldset);
}
