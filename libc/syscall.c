/**
 * @file syscall.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/defs.h>
#include <sys/syscall.h>

uint64_t syscall0(uint64_t rax) {
    asm volatile("movq %1, %%rax\n\t"
                 "int $0x80\n\t"
                 "movq %%rax, %0\n\t"
                 : "=X"(rax) // output
                 : "X"(rax) // input
                 : "%rax" // clobbered registers
        );
    return rax;
}

uint64_t syscall1(uint64_t rax, uint64_t rdi) {
    asm volatile("movq %1, %%rax\n\t"
                 "movq %2, %%rdi\n\t"
                 "int $0x80\n\t"
                 "movq %%rax, %0\n\t"
                 : "=X"(rax)
                 : "X"(rax), "X"(rdi)
                 : "%rax", "%rdi");
    return rax;
}

uint64_t syscall2(uint64_t rax, uint64_t rdi, uint64_t rsi) {
    asm volatile("movq %1, %%rax\n\t"
                 "movq %2, %%rdi\n\t"
                 "movq %3, %%rsi\n\t"
                 "int $0x80\n\t"
                 "movq %%rax, %0\n\t"
                 : "=X"(rax)
                 : "X"(rax), "X"(rdi), "X"(rsi)
                 : "%rax", "%rdi", "%rsi");
    return rax;
}

uint64_t syscall3(uint64_t rax, uint64_t rdi, uint64_t rsi, uint64_t rdx) {
    asm volatile("movq %1, %%rax\n\t"
                 "movq %2, %%rdi\n\t"
                 "movq %3, %%rsi\n\t"
                 "movq %4, %%rdx\n\t"
                 "int $0x80\n\t"
                 "movq %%rax, %0\n\t"
                 : "=X"(rax)
                 : "X"(rax), "X"(rdi), "X"(rsi), "X"(rdx)
                 : "%rax", "%rdi", "%rsi", "%rdx");
    return rax;
}

uint64_t syscall4(uint64_t rax, uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t r10) {
    asm volatile("movq %1, %%rax\n\t"
                 "movq %2, %%rdi\n\t"
                 "movq %3, %%rsi\n\t"
                 "movq %4, %%rdx\n\t"
                 "movq %5, %%r10\n\t"
                 "int $0x80\n\t"
                 "movq %%rax, %0\n\t"
                 : "=X"(rax)
                 : "X"(rax), "X"(rdi), "X"(rsi), "X"(rdx), "X"(r10)
                 : "%rax", "%rdi", "%rsi", "%rdx", "%r10");
    return rax;
}

uint64_t syscall5(uint64_t rax, uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t r10, uint64_t r8) {
    asm volatile("movq %1, %%rax\n\t"
                 "movq %2, %%rdi\n\t"
                 "movq %3, %%rsi\n\t"
                 "movq %4, %%rdx\n\t"
                 "movq %5, %%r10\n\t"
                 "movq %6, %%r8\n\t"
                 "int $0x80\n\t"
                 "movq %%rax, %0\n\t"
                 : "=X"(rax)
                 : "X"(rax), "X"(rdi), "X"(rsi), "X"(rdx), "X"(r10), "X"(r8)
                 : "%rax", "%rdi", "%rsi", "%rdx", "%r10", "%r8");
    return rax;
}

uint64_t syscall6(uint64_t rax, uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t r10, uint64_t r8, uint64_t r9) {
    asm volatile("movq %1, %%rax\n\t"
                 "movq %2, %%rdi\n\t"
                 "movq %3, %%rsi\n\t"
                 "movq %4, %%rdx\n\t"
                 "movq %5, %%r10\n\t"
                 "movq %6, %%r8\n\t"
                 "movq %7, %%r9\n\t"
                 "int $0x80\n\t"
                 "movq %%rax, %0\n\t"
                 : "=X"(rax)
                 : "X"(rax), "X"(rdi), "X"(rsi), "X"(rdx), "X"(r10), "X"(r8), "X"(r9)
                 : "%rax", "%rdi", "%rsi", "%rdx", "%r10", "%r8", "%r9");
    return rax;
}

/*
        No. Name.       EntryPoint.
    [x] 0   read        sys_read
    [x] 1   write       sys_write
    [x] 2   open        sys_open
    [x] 3   close       sys_close
    [ ] 4   stat        sys_newstat
    [ ] 5   fstat       sys_newfstat
    [x] 12  brk         sys_brk
    [ ] 16  ioctl       sys_ioctl
    [x] 293 pipe2       sys_pipe
    [x] 24  yield       sys_yield
    [x] 33  dup2        sys_dup2
    [x] 57  fork        sys_fork
    [x] 59  execve      sys_execve
    [x] 60  exit        sys_exit
    [x] 61  wait4       sys_wait4
    [x] 78  getdents    sys_getdents
    [x] 80  chdir       sys_chdir
    [ ] 82  rename      sys_rename
    [ ] 83  mkdir       sys_mkdir
    [ ] 84  rmdir       sys_rmdir
    [ ] 85  creat       sys_creat
    [ ] 87  unlink      sys_unlink
    [ ] 169 reboot      sys_reboot
*/

/* Processes */

void sys_exit(int status) {
    syscall1(SYS_EXIT, status);
}

pid_t sys_getpid(void) {
    return syscall0(SYS_GETPID);
}

pid_t sys_getppid(void) {
    return syscall0(SYS_GETPPID);
}

void* sys_brk(void* addr) {
    return (void*)syscall1(SYS_BRK, (uint64_t)addr);
}

int sys_pipe(int fds[2]) {
    return syscall1(SYS_PIPE2, (uint64_t)fds);
}

int sys_pipe2(int fds[2], int flags) {
    return syscall2(SYS_PIPE2, (uint64_t)fds, flags);
}

int sys_execve(char* program_path, char* const argv[], char* const envp[]) {
    return syscall3(SYS_EXEC, (uint64_t)program_path, (uint64_t)argv, (uint64_t)envp);
}

uint32_t sys_sleep(uint32_t seconds) {
    return syscall1(SYS_SLEEP, seconds);
}

pid_t sys_fork() {
    return syscall0(SYS_FORK);
}

int sys_wait(pid_t pid, int* status, int options) {
    // We don't support rusage
    return syscall4(SYS_WAIT, pid, (uint64_t)status, options, (uint64_t)NULL);
}

int sys_dup2(int oldfd, int newfd) {
    return syscall2(SYS_DUP2, oldfd, newfd);
}

size_t sys_getcwd(char* buf, size_t size) {
    return syscall2(SYS_GETCWD, (uint64_t)buf, (uint64_t)size);
}

int sys_chdir(const char* path) {
    return syscall1(SYS_CHDIR, (uint64_t)path);
}

int sys_yield(void) {
    return syscall0(SYS_YIELD);
}

/* Signal */

int sys_kill(pid_t pid, int sig) {
    return syscall2(SYS_KILL, pid, sig);
}

int sys_sigprocmask(int how, const sigset_t* set, sigset_t* oldset) {
    return syscall3(SYS_SIGPROGMASK, how, (uintptr_t)set, (uintptr_t)oldset);
}

/* File System */

int sys_access(const char* pathname, int mode) {
    return syscall2(SYS_ACCESS, (uint64_t)pathname, mode);
}

int sys_open(const char* filename, int flags, int mode) {
    return syscall3(SYS_OPEN, (uint64_t)filename, flags, mode);
}

int sys_close(int fd) {
    return syscall1(SYS_CLOSE, fd);
}

int sys_ioctl(int i) {
    return syscall1(SYS_IOCTL, i);
}

int sys_unlink(const char* pathname) {
    return syscall1(SYS_UNLINK, (uint64_t)pathname);
}

int sys_getdents(DIR* dirp, void* entries, size_t size) {
    return syscall3(SYS_GETDENTS, (uint64_t)dirp->fd, (uint64_t)entries, (uint64_t)size);
}

/* I/O */

ssize_t sys_read(int fd, void* buf, size_t count) {
    return syscall3(SYS_READ, (uint64_t)fd, (uint64_t)buf, (uint64_t)count);
}

ssize_t sys_write(int fd, const void* buf, size_t count) {
    return syscall3(SYS_WRITE, (uint64_t)fd, (uint64_t)buf, (uint64_t)count);
}
