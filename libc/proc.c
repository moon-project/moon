/**
 * @file proc.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <stdlib.h>
#include <string.h>
#include <sys/syscall.h>
#include <unistd.h>

void exit(int status) {
    sys_exit(status);
}

pid_t getpid(void) {
    return sys_getpid();
}

pid_t getppid(void) {
    return sys_getppid();
}

static void* breakpoint = NULL;
void* sbrk(intptr_t increment) {
    return brk(breakpoint + increment);
}

void* brk(void* addr) {
    breakpoint = sys_brk(addr);
    return breakpoint;
}

int pipe(int fds[2]) {
    return sys_pipe(fds);
}

int pipe2(int fds[2], int flags) {
    return sys_pipe2(fds, flags);
}

pid_t fork() {
    return sys_fork();
}

pid_t wait(int* status) {
    return waitpid(-1, status);
}

int waitpid(int pid, int* status) {
    return sys_wait(pid, status, 0);
}

int waitpid_with_options(int pid, int* status, int options) {
    return sys_wait(pid, status, options);
}

int yield() {
    return sys_yield();
}

int dup2(int oldfd, int newfd) {
    return sys_dup2(oldfd, newfd);
}

char* getcwd(char* buf, size_t size) {
    size_t len = sys_getcwd(buf, size);
    if (len == (strlen(buf) + 1))
        return buf;
    else
        return NULL;
}

int chdir(const char* path) {
    return sys_chdir(path);
}

uint32_t sleep(uint32_t seconds) {
    return sys_sleep(seconds);
}

int exec(const char* file){
    sys_execve((char*)file, NULL, NULL);
    return 0;
}

int execvpe(const char* file, char* const argv[], char* const envp[]) {
    char program_path[1024] = { 0 };
    char* environ_path = getenv("PATH");
    if (*file != '.' && *file != '/' && environ_path != NULL) {
        char env_path[1024] = { 0 };
        strcpy(env_path, environ_path);
        char* token;
        int found = 0;
        char* tmp_env = env_path;
        while ((token = strsep(&tmp_env, ":")) != NULL) {
            strcpy(program_path, token);
            strcat(program_path, "/");
            strcat(program_path, file);
            if (access(program_path, F_OK) == 0) {
                found = 1;
                break;
            } else {
                memset(program_path, 0, 1024);
            }
        }
        if (!found) {
            return 127;
        }
    } else {
        if (access(file, F_OK) < 0) {
            return 127;
        } else {
            strcpy(program_path, file);
        }
    }
    return sys_execve(program_path, argv, envp);
}
