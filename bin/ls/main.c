/**
 * @file main.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>

#define BUF_SIZE 1024

int main(int argc, const char* argv[], char* envp[]) {
    int nread;
    char buf[BUF_SIZE];
    dirent_t* dir;

    DIR folder = { .fd = 0 };
    argc > 1 ? opendir_takes_ptr(&folder, argv[1]) : opendir_takes_ptr(&folder, ".");

    if (folder.fd == -1) {
        puts("Couldn't open folder");
        exit(1);
    }

    while ((nread = getdents(0, buf, BUF_SIZE)) > 0) {
        for (int i = 0; i < nread; i += dir->d_reclen) {
            dir = (dirent_t*)(buf + i);
            puts(dir->d_name);
            puts("\n");
        }
    }

    if (nread == -1)
        puts("getdents failed");

    // closedir(&folder);

    return 0;
}
