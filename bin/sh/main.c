/**
 * @file main.c
 * @author Neal Beeken, Mrunal Patel
 * @brief Simple shell
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

bool done = false;

void handle_input(char* input);

int main(int argc, char const* argv[], char* envp[]) {

    puts("\n\n~ ~ ~ ~ ~ ~ ~ ~ ~ Welcome ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\n");
    puts("~ ~ ~ ~ ~ ~ A s k  f o r  'h e l p' ~ ~ ~ ~ ~ ~\n\n\n\n");

    char input[4096];
    char* input2 = malloc(1024);

    input[0] = input2[0];

    do {
        memset(input, 0, 1024);

        puts("moon_sh >> ");
        char* inputp = fgets(input, 1024, STDIN_FILENO);
        size_t len = strlen(inputp);

        inputp[len - 1] = '\0';

        handle_input(inputp);

    } while (true);

    return 0;
}

void handle_input(char* input) {
    if (strcmp(input, "help")) {
        printf("\nMoon OS Help Menu!\n");
        printf("\tOne small step for Moon OS\n\tOne even smaller step for Operating Systems\n\n");
        printf("\tYou may type '/bin/ps' or '/bin/print'\n\ttry another program for graceful segfault\n\n");
    } else {
        if (input[0] == '/') {
            exec(input);
            yield();
        }
    }
}
