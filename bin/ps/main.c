/**
 * @file main.c
 * @author Neal Beeken, Mrunal Patel
 * @brief Simple shell
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>

#define LISTPROCS 23

int main(int argc, char const* argv[], char* envp[]) {

    sys_ioctl(LISTPROCS);

    return 0;
}
