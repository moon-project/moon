#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char const* argv[], char* envp[]) {

    pid_t pid = fork();
    int status;

    if (pid == 0) {
        printf("I am child\n");
        exit(3);
    } else {
        printf("parent\n");
        wait(&status);
        printf("parent after wait\n");
    }

    return 0;
}
