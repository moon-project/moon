/**
 * @file main.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

pid_t Fork() {
    pid_t pid = fork();

    if (pid == -1) {
        printf("Failed to fork\n");
        exit(EXIT_FAILURE);
    }

    return pid;
}

int main(int argc, char const* argv[], char* envp[]) {

    printf("Running init...\n");

    int status;

    pid_t pid = Fork();

    if (pid == 0) {
        char* argv[] = { "etc/rc", NULL };
        int ret = execvpe("etc/rc", argv, NULL);
        exit(ret);
    } else {
        int rc = waitpid(pid, &status);
        if (rc == 0) {
            printf("etc/rc was waited on sucessfully\n");
        } else {
            printf("waiting on etc/rc failed\n");
            exit(EXIT_FAILURE);
        }
    }

    pid = Fork();

    if (pid == 0) {
        char* argv[] = { "bin/sbush", NULL };
        int ret = execvpe("bin/sbush", argv, NULL);
        exit(ret);
    } else {
        int rc = waitpid(pid, &status);
        if (rc == 0) {
            printf("bin/sbush returned from wait!!! correctly tho\n");
        } else {
            printf("bin/sbush returned from wait!!! incorrectly!!\n");
        }
    }

    printf("Spinning...");

    while (true) {
        yield();
    }
}
