/**
 * @file main.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <stdio.h>
#include <sys/fcntl.h>
#include <unistd.h>
/* cat file */

int main(int argc, char const* argv[], char* envp[]) {
    int rv = 1;
    int file = argc > 1 ? open_with_mode(argv[1], O_RDONLY, 0) : STDIN_FILENO;
    char buf[2] = { 0, 0 };
    while ((rv = read(file, buf, 1)) > 0) {
        write(STDOUT_FILENO, buf, rv);
    }
    if (rv < 0) {
        printf("%s\n", "Read returned -1");
    }
    return 0;
}
