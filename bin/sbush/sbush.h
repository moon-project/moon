#ifndef SBUSH_H
#define SBUSH_H

#include <assert.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/wait.h>
#include <unistd.h>

#define BUILTIN_ASSERT(cond, msg)                                                                                      \
    do {                                                                                                               \
        if (cond) {                                                                                                    \
            printf("%s\n", msg);                                                                                       \
            exit_code = 1;                                                                                             \
            return -1;                                                                                                 \
        }                                                                                                              \
    } while (0)

#define MAX_CMD_SZ 2048
#define MAX_ARG_CT 64
#define ESC 0x1B

typedef struct prg {
    char* args[MAX_ARG_CT];
    int argc;
    char* infile;
    char* outfile;
    int infd;
    int outfd;
    // Jobs
    pid_t pid;
    // Linked List
    struct prg* next;
} program_t;

typedef int (*builtin_f)(program_t*);

typedef struct {
    char* name;
    builtin_f func;
} builtin;

typedef struct {
    program_t* programs;
    bool bg;
    int num_progs;
} command_t;

typedef struct h {
    char* input;
    struct h* prev;
    struct h* next;
} history_t;

extern bool interactive;
extern char input[];
extern int exit_code;
extern struct sigaction old_action;
extern history_t* history_head;
extern history_t* history_current;
extern char* prompt;
extern builtin builtins[];

/* Helpers */
char* join_string_array(int argc, char* argv[]);
int array_size(int count, char* array[]);
void print_cmd_struct(command_t* cmd);
void trim(char*);

/* Shell */
void child_handler(int signum);
void run(command_t* cmd, char** env);
void execute(program_t* prog, char** env);
// Set basic environment variables for the shell
void set_base_env();

/* User Input */
void insert_char(char c);
void move_insert_pt(int i);
void del_char();
void handle_comment(); // reads until new line.
command_t* parse();
void clear_input();

/* History */
void free_history();
void add_history(char* command);
char* get_prev_history();
// This is not clear, it is when the history is entered and you need to start over.
void reset_history();

/* Console Helpers */
void backspace();
void clearline();
void cursorleft();
void cursorright();
void print_prompt();

/* Builtins */
builtin_f get_builtin(char* prog_name);
int cd(program_t* prog);
int echo(program_t* prog);
int pwd(program_t* prog);
int set(program_t* prog);
int exitbltin(program_t* prog);

#endif /* SBUSH_H */
