/**
 * @file main.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include "sbush.h"

#define IS_VISIBLE_CHAR(ch) (((ch) >= ' ') && ((ch) <= '~'))

bool interactive;

int main(int argc, char const* argv[], char* envp[]) {
    interactive = true;
    if (argc > 1) {
        interactive = false;
        int infile = open_with_mode(argv[1], O_RDONLY, S_IRUSR);
        dup2(infile, STDIN_FILENO);
        close(infile);
    }

    // set base env vars
    set_base_env();
    // set sig handlers
    // struct sigaction action;
    // action.sa_handler = child_handler;
    // sigemptyset(&action.sa_mask);
    // // RESTART makes some system calls restartable
    // // NOCLDSTOP ensures sigchild isn't raised when a child is stopped
    // action.sa_flags = SA_RESTART | SA_NOCLDSTOP;
    // // install handler
    // if (sigaction(SIGCHLD, &action, &old_action) < 0) {
    // }

    char buf[2] = { 0, 0 };

    // prompt.
    print_prompt();
    while ((buf[0] = getchar()) > 0) {

        if (*input == '\0' && buf[0] == 0x4) { // ^D
            putchar('\n');
            break;
        }

        if (*buf == '#') {
            handle_comment();
            *buf = '\n'; // This is to trigger parsing with
            // whatever is potentially in the buffer so far
        }

        if (*buf != '\n' && IS_VISIBLE_CHAR(*buf)) {
            // if (interactive)
            //     putchar(*buf);
            insert_char(*buf);
        }

        if (*buf == '\n') {
            // if (interactive)
            //     putchar('\n');
            if (*input != '\0') {
                add_history(input); // Insert command into history
                command_t* cmd = parse(); // Parse
                run(cmd, envp);
                clear_input();
            }
            print_prompt();
        }

        if (buf[0] == 0x7f) { // Backspace
            del_char();
        }

        if (*buf == ESC) {
            buf[0] = getchar();
            buf[1] = getchar();
            buf[0] = buf[1]; // Toss bracket
            switch (*buf) {
            case 'A': { // Up
                strcpy(input, get_prev_history());
                clearline();
                printf("%s\n", input);
                break;
            }
            case 'B': { // Down
                strcpy(input, get_prev_history());
                clearline();
                printf("%s\n", input);
                break;
            }
            default: {
                printf("default triggered 0x%x\n", *buf);
                break;
            }
            }
        }
    }

    free_history();
    return 0;
}
