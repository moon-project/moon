/**
 * @file builtins.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include "sbush.h"

int exit_code;

/* clang-format off */

builtin builtins[] = {
    { "cd", cd },
    { "echo", echo },
    { "pwd", pwd },
    { "set", set },
    { "exit", exitbltin }
};

#define NUM_BUILTINS 5

/* clang-format off */

builtin_f get_builtin(char* prog_name) {
    for (int i = 0; i < NUM_BUILTINS; ++i) {
        if (strcmp(prog_name, builtins[i].name)) {
            return builtins[i].func;
        }
    }
    return NULL;
}

int cd(program_t* prog) {
    if (prog->argc == 1) {
        return -1; // Cannot cs to nowhere
    }
    int rv;
    char buf[1024];
    char* cwd = getcwd(buf, 1024);
    assert(cwd != NULL);
    char* owd = getenv("OLDPWD");
    char* pwdenv = getenv("PWD");
    if (strcmp(prog->args[0], "-")) {
        if (owd == NULL) {
            return -1;
        }
        rv = setenv("OLDPWD", pwdenv, 1);
        assert(rv == 0);
        rv = setenv("PWD", owd, 1);
        assert(rv == 0);
        rv = chdir(owd);
        assert(rv == 0);
    } else {
        rv = setenv("OLDPWD", pwdenv, 1);
        assert(rv == 0);
        rv = setenv("PWD", prog->args[0], 1);
        assert(rv == 0);
        rv = chdir(prog->args[1]);
        assert(rv == 0);
    }
    return 0;
}

int echo(program_t* prog) {
    for (int i = 0; i < prog->argc - 1; i++) {
        printf("%s ", prog->args[i]);
    }
    printf("\n");
    return 0;
}

int pwd(program_t* prog) {
    char wd[1024];
    char* path = getcwd(wd, 1024);
    assert(path != NULL);
    puts(path);
    puts("\n");
    return 0;
}

int set(program_t* prog) {
    for (int i = 1; i < prog->argc; i++) {
        char* key = prog->args[i];
        char* value = key;
        while (*value++ != '=' && value)
            ;
        if(value != NULL) return 1;
        *(value - 1) = 0;
        int rv = setenv(key, value, 1);
        if(rv) return 1;
    }
    return 0;
}

int exitbltin(program_t* prog) {
    exit(0);
    return -1; // If we made it here... uh oh...
}
