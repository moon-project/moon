/**
 * @file input.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include "sbush.h"

char input[MAX_CMD_SZ] = { 0 };
static char* cursor = input;
static int input_count = 0;

void clear_input() {
    int i = 0;
    while (i < MAX_CMD_SZ) {
        input[i++] = '\0';
    }
    input_count = 0;
    cursor = input;
}

command_t* parse() {
    char* input_tmp = input;
    char* token;
    // Order of precedence is |, <, ' '
    command_t* cmd = calloc(1, sizeof(command_t));
    // check if bg
    char* c = input;
    while (*++c != '&' && *c)
        ;
    if (*c != 0) {
        cmd->bg = 1;
        *c = 0; // remove the &
    }
    while ((token = strsep(&input_tmp, "|")) != NULL) { // Pipes

        program_t* prog = calloc(1, sizeof(program_t));

        if (cmd->programs == NULL) { // First program
            cmd->programs = prog;
            cmd->num_progs = 1;
        } else {
            program_t* tmp;
            // Get tail
            for (tmp = cmd->programs; tmp->next != NULL; tmp = tmp->next)
                ;
            tmp->next = prog;
            cmd->num_progs++;
            // for piping
            tmp->outfile = (void*)1;
            prog->infile = (void*)1;
        }

        // Check that there is more of token to parse, meaning args.
        char* arg = NULL;
        while (token != NULL && ((arg = strsep(&token, " ")) != NULL)) {
            if (*arg != '\0') {
                prog->args[prog->argc++] = arg;
                if (prog->argc == MAX_ARG_CT)
                    break; // Try the command anyway...
            }
        }
    }
    return cmd;
}

static void shiftdown() {
    char* tmp = &input[input_count]; // start at the end
    while (tmp != cursor) { // iterate til we reach cursor position
        *(tmp + 1) = *tmp; // assign one fwd to current
        tmp--; // move back one
    }
}

static void shiftup() {
    char* tmp = cursor; // start at the current position
    while (tmp != &input[input_count]) { // iterate til we reach end
        *tmp = *(tmp + 1); // assign current to one fwd
        tmp++; // move fwd one
    }
}

void insert_char(char c) {
    shiftdown();
    *cursor++ = c;
    input_count++;
}

void move_insert_pt(int i) {
    cursor += i; // Move cursor by i amount
}

void del_char() {
    shiftup();
    cursor--;
    input_count--;
}

void handle_comment() {
    char in;
    do {
        in = getchar();
    } while (in != '\n');
}
