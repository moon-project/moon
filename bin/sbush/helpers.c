/**
 * @file helpers.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include "sbush.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void trim(char* str) {
    char* tmp = &str[strlen(str) - 1];
    while (*tmp == ' ') {
        *tmp = '\0';
        tmp--;
    }
}

int array_size(int count, char* array[]) {
    int i, sum = 1; /* NULL terminator */
    for (i = 0; i < count; ++i) {
        sum += strlen(array[i]);
        ++sum; /* For the spaces */
    }
    return sum + 1;
}

char* join_string_array(int count, char* array[]) {
    char* ret;
    int i;
    int len = 0, str_len, cur_str_len;

    str_len = array_size(count, array);
    ret = calloc(str_len, 1);

    for (i = 0; i < count; ++i) {
        cur_str_len = strlen(array[i]);
        memcpy(ret + len, array[i], cur_str_len);
        len += cur_str_len;
        memcpy(ret + len, " ", 1);
        len += 1;
    }

    return ret;
}

void print_cmd_struct(command_t* cmd) {
    program_t* tmp;
    // Get tail
    for (tmp = cmd->programs; tmp != NULL; tmp = tmp->next) {
        printf("struct prg @ %p {\n"
               "   char* name = '%s';\n"
               "   char** args = '%s';\n"
               "   int argc = %d;\n"
               "   char* infile = %s;\n"
               "   char* outfile = %s;\n"
               "   struct prg* next = %p;\n"
               "} program_t;\n",
            tmp, tmp->args[0], join_string_array(tmp->argc, tmp->args), tmp->argc, tmp->infile, tmp->outfile,
            tmp->next);
    }
}
