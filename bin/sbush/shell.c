/**
 * @file shell.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include "sbush.h"

void set_base_env() {
    char* envvar = getenv("PWD");
    if (envvar == NULL) {
        char buf[1024];
        envvar = getcwd(buf, 1024);
        int rv = setenv("PWD", buf, 1);
        if (rv < 0) {
            printf("%s\n", "set_base_env error");
            return;
        }
    }
}

void child_handler(int signum) {
    // FUTURE: save errno, mask all before deleteing from jobs list
    pid_t pid;
    while ((pid = waitpid(-1, NULL)) > 0)
        ;
}

void run(command_t* cmd, char** env) {
    // Reset the exit_code
    exit_code = 0;
    program_t* prog = cmd->programs;
    int* pipefds = calloc(cmd->num_progs - 1, sizeof(int) * 2);
    for (int i = 0; i < cmd->num_progs - 1; i++) {
        if (pipe2(pipefds + (2 * i), O_CLOEXEC) < 0) {
            printf("pipe error\n");
            exit_code = 1;
            goto run_free;
        }
    }
    // Execute all the programs in the command
    int n = 0;
    while (1) {
        prog->infd = (prog->infile == NULL) ? STDIN_FILENO : pipefds[(n - 1) * 2];
        prog->outfd = (prog->outfile == NULL) ? STDOUT_FILENO : pipefds[n * 2 + 1];
        // execute the program
        execute(prog, env);
        // If this is the last program leave the loop
        if (prog->next == NULL) {
            break;
        } else {
            // next prog
            prog = prog->next;
            n++;
        }
    }
    // close the pipe descriptors
    for (int i = 0; i < cmd->num_progs - 1; i++) {
        close(pipefds[i * 2]);
        close(pipefds[i * 2 + 1]);
    }
    // Wait for the command to finish if the program is not bg
    if (!cmd->bg) {
        // FUTURE: while waiting (WNOHANG) check for C-c and C-z
        pid_t pid;
        if ((pid = waitpid(prog->pid, &exit_code)) < 0) {
            printf("waitpid err\n");
            goto run_free;
        }
    }
// FUTURE: add to jobs list here
run_free:
    free(pipefds);
    // free cmd
    prog = cmd->programs;
    while (prog != NULL) {
        program_t* next = prog->next;
        free(prog);
        prog = next;
    }
    free(cmd);
}

void execute(program_t* prog, char** env) {
    builtin_f builtin = get_builtin(prog->args[0]);
    if (builtin != NULL) {
        builtin(prog);
        return; // Built in successful?
    }
    // set pid for parent
    if ((prog->pid = fork()) == 0) {
        // reset signal handler
        // TODO
        // if (signal(SIGCHLD, &old_action, NULL) < 0) {
        //     printf("sigaction error\n");
        // }
        // dup the descriptors
        // pipes are opened with O_CLOEXEC, will close on exec
        if (prog->infd != STDIN_FILENO) {
            if (dup2(prog->infd, STDIN_FILENO) < 0) {
                printf("dup2 error\n");
            }
        }
        if (prog->outfd != STDOUT_FILENO) {
            if (dup2(prog->outfd, STDOUT_FILENO) < 0) {
                printf("dup2 error\n");
            }
        }
        // execute
        execvpe(prog->args[0], prog->args, env);
        // exec returns == error.
        printf("execvpe error\n");
        exit(127);
    } else if (prog->pid < 0) {
        printf("fork error\n");
        return;
    }
}

// Built-in Commands
