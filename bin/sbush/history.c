/**
 * @file history.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include "sbush.h"

history_t* history_head = NULL;
history_t* history_current = NULL;

void free_history() {

    if (!interactive)
        return;

    for (history_current = history_head; history_current; history_current = history_current->next) {
        free(history_current->input);
        free(history_current);
    }
}

void add_history(char* command) {

    if (!interactive)
        return;

    history_t* newline = malloc(sizeof(history_t));
    newline->next = history_head;
    if (history_head != NULL)
        history_head->prev = newline;
    newline->prev = NULL;
    newline->input = strdup(command);
    history_head = newline;
}

char* get_prev_history() {

    if (!interactive)
        return NULL;

    if (history_current == NULL) {
        history_current = history_head;
    } else {
        history_current = history_current->next;
    }
    return history_current->input;
}

// This is not clear, it is when the history is entered and you need to start over.
void reset_history() {
    history_current = NULL;
}
