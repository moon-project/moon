/**
 * @file console.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include "sbush.h"

char* prompt;

void print_prompt() {

    if (!interactive)
        return;

    prompt = getenv("PS1");
    if (prompt == NULL) {
        prompt = "sbush $ ";
    }
    puts(prompt);
}

void backspace() {
    putchar(0x7f);
}

void clearline() {
    puts("\033[100D"); // Should really move it back to beginning
    puts("\e[K"); // Clear it
    print_prompt();
}

void cursorleft() {
    puts("\e[1C");
}

void cursorright() {
    puts("\e[1D");
}
