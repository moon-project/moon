/**
 * @file tarfs.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/kutils.h>
#include <sys/core/tarfs.h>
#include <sys/core/vfs.h>

superblock_t* tar_filesystem = NULL;

void tarfile_to_vfs_inode(tarfile_t* file, vfs_inode_t* inode) {

    k_bzero(inode, sizeof(vfs_inode_t));

    k_strcpy(inode->name, k_basename(file->fullpath));

    if (*endofstr(inode->name) == '/') {
        *endofstr(inode->name) = '\0';
    }

    inode->filemode = file->mode;
    inode->superblock = tar_filesystem;
    inode->size = file->size;
    inode->links = 1;
    inode->is_dir = (file->type == DIRTYPE);

    inode->data_ptrs[0] = inode->is_dir ? NULL : file->data;

    // if (!inode->is_dir) {
    //     int i = 0;
    //     for (void *ptr = file->data; ptr < BYBYTE(file->data, +file->size); ptr += 4096, i++) {
    //         if (i < 20) {
    //             inode->data_ptrs[i] = ptr;
    //         } else {
    //             panics("no indirect pts");
    //         }
    //     }
    // }
}

bool tarheader_to_tarfile(tar_header_t* tarheader, tarfile_t* file) {

    k_memset(file, 0, sizeof(tarfile_t));

    if (!tar_integrity_check(tarheader)) {
        return false;
    }

    // Name, linkto, user and gname
    file->fullpath[0] = '/';
    k_strcpy(&file->fullpath[1], tarheader->name);
    k_strcpy(file->linkto, tarheader->linkname);
    k_strcpy(file->uname, tarheader->uname);
    k_strcpy(file->gname, tarheader->gname);

    // Mode
    uint64_t modebits = 0;
    panicif(!k_strtonum(tarheader->mode, &modebits, 8));
    file->mode = modebits;

    // Size
    uint64_t size = 0;
    panicif(!k_strtonum(tarheader->size, &size, 8));
    file->size = size;

    // File type
    file->type = *tarheader->typeflag;

    // Time
    uint64_t last_modified = 0;
    panicif(!k_strtonum(tarheader->mtime, &last_modified, 8));
    file->modified_time = last_modified;

    uint64_t uid = 0;
    uint64_t gid = 0;
    panicif(!k_strtonum(tarheader->uid, &uid, 8));
    panicif(!k_strtonum(tarheader->gid, &gid, 8));
    file->uid = uid;
    file->gid = gid;

    if (file->type == REGTYPE) {
        file->data = BYBYTE(tarheader, +sizeof(tar_header_t));
    } else {
        file->data = NULL;
    }

    return true;
}

bool tar_integrity_check(tar_header_t* tarheader) {
    return k_memcmp(tarheader->magic, TMAGIC, TMAGICLEN) && k_memcmp(tarheader->version, TVERSION, TVERSIONLEN);
}

int tar_open(char*, int, mode_t);
int tar_close(int);
int tar_access(const char* filename, int how);
int tar_read(int, void*, size_t);
int tar_lseek(int, off64_t, int);
int tar_mkdir(char*, mode_t);
int tar_opendir(char*);
int tar_closedir(DIR*);
dirent_t* tar_getdents(int, dirent_t*, size_t);

vfs_interface_t tarfs_interface() {
    vfs_interface_t interface;
    k_bzero(&interface, sizeof(vfs_interface_t));
    // interface.open_f = tar_open;
    // interface.close_f = tar_close;
    // interface.access_f = tar_access;
    // interface.read_f = tar_read;
    // interface.lseek_f = tar_lseek;
    interface.mkdir_f = tar_mkdir;
    // interface.opendir_f = tar_opendir;
    // interface.closedir_f = tar_closedir;
    // interface.getdents_f = tar_getdents;
    return interface;
}

int tar_mkdir(char* fullpath, mode_t mode) {
    return 0;
}
