/**
 * @file vfs.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/kutils.h>
#include <sys/core/vfs.h>

int get_free_entry(vfs_inode_t* node) {
    for (int i = 0; i < 12; i++) {
        if (node->dirents[i] == NULL) {
            return i;
        }
    }
    // TODO RETURNING NULL IS NOT ACCEPTABLE
    // SHOULD ALLOCATE INDIRECT POINTER SPACE!!
    return -1;
}

void insert_inode(superblock_t* sbp, char* fullpath, vfs_inode_t* inode) {
    // Using full path, search for slot to insert inode pointer
    // break on '/' and search for each item in tokenized string
    // when you reach that last name then you can insert, use get_free_entry

    vfs_inode_t* root = sbp->root; // convience

    char cpbuf[k_strlen(fullpath)];
    char* fullpath_copy = cpbuf;
    k_strcpy(fullpath_copy, fullpath);

    if (*endofstr(fullpath_copy) == '/') {
        *endofstr(fullpath_copy) = '\0';
    }

    size_t cur_depth = 0, depth = k_strccount(fullpath_copy, '/');

    vfs_inode_t* parent = root;
    char* token;

    if (fullpath_copy[0] == '/') {
        // Toss the empty sring at the beginning
        token = k_strsep(&fullpath_copy, "/");
    }

    bool found_parent = false;

    while ((token = k_strsep(&fullpath_copy, "/")) != NULL && !emptystr(token)) {
        // kdebug("tok: %s", token);
        if (cur_depth == depth - 1) {
            // file! (well not necesarily but its the end of the road)
            int free_index = get_free_entry(parent);
            panicif(free_index == -1);
            parent->dirents[free_index] = inode;
            return;
        } else {
            // find token dir
            for (int i = 0; i < 12; i++) {
                if (parent->dirents[i] != NULL && k_strcmp(parent->dirents[i]->name, token)) {
                    parent = parent->dirents[i];
                    found_parent = true;
                    break;
                }
            }
            if (found_parent) {
                found_parent = false;
                cur_depth++;
                continue;
            }
            // Didn't find parent so we need to insert it...
            // Or not...
            panic();
            // idk I don't wanna do this rn
            int free_index = get_free_entry(parent);
            panicif(free_index == -1);

            char pathuptohere[k_strlen(fullpath)];
            k_bzero(pathuptohere, k_strlen(fullpath));
            char* dir_location = k_strstr(fullpath, token);
            k_memcpy(pathuptohere, fullpath, dir_location - fullpath);
            k_strcat(pathuptohere, token);

            sbp->interface.mkdir_f(pathuptohere, 0);
        }
    }
}

// When full path is indicated it needs to start with a /
vfs_inode_t* find_inode(superblock_t* sbp, char* fullpath) {
    // Using full path, search for slot to insert inode pointer
    // break on '/' and search for each item in tokenized string
    // when you reach that last name then you can insert, use get_free_entry

    vfs_inode_t* root = sbp->root; // convience

    char cpbuf[k_strlen(fullpath)];
    char* fullpath_copy = cpbuf;
    k_strcpy(fullpath_copy, fullpath);

    if (*endofstr(fullpath_copy) == '/') {
        *endofstr(fullpath_copy) = '\0';
    }

    size_t cur_depth = 0, depth = k_strccount(fullpath_copy, '/');

    vfs_inode_t* parent = root;
    char* token;

    if (fullpath_copy[0] == '/') {
        // Toss the empty sring at the beginning
        token = k_strsep(&fullpath_copy, "/");
    }

    bool found_parent = false;

    while ((token = k_strsep(&fullpath_copy, "/")) != NULL && !emptystr(token)) {
        // kdebug("tok: %s", token);
        if (cur_depth == depth - 1) {
            for (int i = 0; i < 12; i++) {
                if (parent->dirents[i] != NULL && k_strcmp(parent->dirents[i]->name, token)) {
                    return parent->dirents[i];
                }
            }
            return NULL;
        } else {
            // find token dir
            for (int i = 0; i < 12; i++) {
                if (parent->dirents[i] != NULL && k_strcmp(parent->dirents[i]->name, token)) {
                    parent = parent->dirents[i];
                    found_parent = true;
                    break;
                }
            }
            if (found_parent) {
                found_parent = false;
                cur_depth++;
                continue;
            }
        }
    }
    return NULL;
}
