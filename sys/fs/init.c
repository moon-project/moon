/**
 * @file init.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */
#include <sys/core/init.h>
#include <sys/core/kutils.h>
#include <sys/core/tarfs.h>

void init_fs(const init_args_t const* args) {
    init_tarfs();
}

void init_tarfs() {

    kinfo("TarFS [ %p - %p ]", &_binary_tarfs_start, &_binary_tarfs_end);

    tar_filesystem = kalloc(sizeof(superblock_t));
    tar_filesystem->root = kalloc(sizeof(vfs_inode_t));
    tar_filesystem->root->is_dir = true;
    tar_filesystem->type = TARFS;
    tar_filesystem->interface = tarfs_interface();
    tar_filesystem->fs_meta = NULL;

    tar_filesystem->root->superblock = tar_filesystem;
    tar_filesystem->root->size = 0;

    tar_header_t* tarfs_start = (tar_header_t*)&_binary_tarfs_start;

    void* tarfs_end = &_binary_tarfs_end;

    tar_header_t* fs_ptr = tarfs_start;

    while ((void*)fs_ptr < (void*)tarfs_end) {
        tarfile_t file;

        tarheader_to_tarfile(fs_ptr, &file);

        if (k_strlen(file.fullpath) != 0) {
            // If we hit a file
            // Print info
            // kinfo("%s %s %dKB", file.fullpath, file.type == DIRTYPE ? "DIR" : "FIL", file.size / ONE_KB);

            // intsert pointer to fs
            // Create new inode:
            vfs_inode_t* new_inode = kalloc(sizeof(vfs_inode_t));

            tarfile_to_vfs_inode(&file, new_inode);

            insert_inode(tar_filesystem, file.fullpath, new_inode);
        }

        // Iteration math
        size_t size = CEIL(512, file.size);
        size_t offset = MAX(size, 512);
        fs_ptr = BYBYTE(fs_ptr, +offset);
    }
}
