.globl basic_ctxswitch
.globl get_rip
.globl userspace_test_rip
.globl first_task

.macro push_callee
 push %rbx
 push %r12
 push %r13
 push %r14
 push %r15
 push %rbp
.endm

.macro pop_callee
 pop %rbp
 pop %r15
 pop %r14
 pop %r13
 pop %r12
 pop %rbx
.endm

basic_ctxswitch:
    push_callee
    push %rdi
    movq %rsp, 0(%rdi) # update stack ptr in task struct
    movq 0(%rsi), %rsp # get next's stack ptr
    pop %rdi
    pop_callee
    ret

_ctxswitch: # rdi (current), rsi (next), rdx (pointer to prev)
    push_callee # save stuff
    push %rdi # save current on current's stack
    push %rdx # save pointer to prev on current's stack
    movq %rsp, 0(%rdi) # save current's stack pointer
    movq 0(%rsi), %rsp # switch to next task
    pop %rdx # get next's pointer to prev
    test %rdx, %rdx # check for null pointer
    jz _last_is_null
    movq %rdi, 0(%rdx) # save current as next's prev
_last_is_null:
    pop %rdi # update current to next
    pop_callee # restore stuff
    # restore context
    # restore everything (the registers) from task struct
    # set cr3 from task struct
    # set userspace stuff only if mm field is not NULL.
    # read iretq stack documentation to restore from stack
    # TODO: change to iretq
    ret # return (for first time this will jump to the function to run)

get_rip:
    pop %rax
    push %rax
    ret

userspace_test_rip:
    movq 0x800000, %rax
    jmp %rax