#include <sys/core/asm.h>
#include <sys/core/elf64.h>
#include <sys/core/kutils.h>
#include <sys/core/proc.h>
#include <sys/core/screen.h>
#include <sys/core/tarfs.h>
#include <sys/core/tty.h>
#include <sys/core/vfs.h>
#include <sys/core/vm.h>

/* http://www.cirosantilli.com/elf101.png */

// create user space will be the same as create_task
// task->function will be syscall_epilogue
task_t* make_bin_task(char* filename) {
    // TODO open crt.o and memcpy data to vma
    // uintptr_t start_sym = 0; // Get _start function symbol from bin

    task_t* newtask = create_task(NULL);
    newtask->cr3 = create_page_table();
    newtask->mm = kalloc(sizeof(mm_t));

    newtask->name = kalloc(k_strlen(filename));
    k_strcpy(newtask->name, filename);

    /* Stdin, Stdout, Stderr */
    newtask->filetable.inodes[0] = stdin_inode;
    newtask->filetable.inodes[1] = stdout_inode;
    newtask->filetable.inodes[2] = stderr_inode;
    newtask->filetable.firstfreefd = 3;
    newtask->filetable.inode_count = 3;
    newtask->filetable.maxusedfd = 2;

    vfs_inode_t* inode = find_inode(tar_filesystem, filename);

    elf_t elf_details;
    readelf(inode->data_ptrs[0], &elf_details);
    // TODO Map address space
    // vma_t* cursor = task->mm->mapping;
    // while(cursor != NULL) {
    //    map_va(cursor->start, pml4, cursor->physpage);
    //    va_user(cursor->start, pml4, true);
    //    cursor = cursor->next;
    //}

    /*
     * For each section:
     * - kalloc new VMA
     * - set fields
     * - map necessary virtual addresses for the section
     * - insert_vma(newtask, vma)
     */
    for (vma_sections_t section = 0; section <= STACK_SECTION; section++) {
        switch (section) {
        case TEXT_SECTION: {
            vma_t* text_vma = load_elf_section(newtask, inode->data_ptrs[0], elf_details.text_hdr, false, TEXT_SECTION);
            SET_EXECABLE(text_vma);
            break;
        }
        case DATA_SECTION: {
            vma_t* data_vma = load_elf_section(newtask, inode->data_ptrs[0], elf_details.data_hdr, true, DATA_SECTION);
            SET_WRITABLE(data_vma);
            SET_READABLE(data_vma);
            break;
        }
        case RODATA_SECTION: {
            vma_t* rodata_vma
                = load_elf_section(newtask, inode->data_ptrs[0], elf_details.rodata_hdr, false, RODATA_SECTION);
            SET_READABLE(rodata_vma);
            break;
        }
        case BSS_SECTION: {
            // TODO Generate a .bss section in the third vma
            vma_t* bss_vma = load_elf_section(newtask, inode->data_ptrs[0], elf_details.bss_hdr, true, BSS_SECTION);
            SET_WRITABLE(bss_vma);
            SET_READABLE(bss_vma);
            break;
        }
        case HEAP_SECTION:
            // Do nothing... heap doesn't need to exist on process start
            break;
        case STACK_SECTION: {
            vma_t* stack_vma = kalloc(sizeof(vma_t));

            SET_READABLE(stack_vma);
            SET_WRITABLE(stack_vma);
            SET_GROWSDWN(stack_vma);

            stack_vma->section = STACK_SECTION;
            stack_vma->vm_mm = newtask->mm;
            newtask->mm->stack_start = (uintptr_t)STACK_INIT_PAGE;
            newtask->mm->stack_end = (uintptr_t)STACK_INIT;
            newtask->mm->stack_mark = (uintptr_t)STACK_INIT_PAGE - PGSZ;
            stack_vma->start = STACK_INIT_PAGE;
            stack_vma->end = STACK_INIT;
            void* addr = STACK_INIT_PAGE;
            map_va(addr, PA2VA(newtask->cr3), VA_USER | VA_RW);
            insert_vma(newtask, stack_vma);
            // asm volatile (
            //     "movq %%rsp, %%r10\n\t"
            //     "movq %P[stack], %%rsp\n\t"
            //     "push $0x23\n\t"
            //     "movq %P[stack], %%r11\n\t"
            //     "push %%r11\n\t"
            //     "movq %P[entry], %%r11\n\t"
            //     "push %r11\n\t" //rsp
            //     "pushf\n\t"
            //     "push $0x1B\n\t"
            //     "lea userspace_test_main, %%r10\n\t"
            //     "push %%r10\n\t" //rip
            //     "movq %%r10, %%rsp\n\t"
            //     : [stackptr] "=r" (setup_stack) //output
            //     : [entry] "r" (ENTRY)
            //       [stack] "r" (STACK_INIT) //input
            //     : //clobber
            // );
            break;
        }
        }
    }
    return newtask;
}

vma_t* load_elf_section(task_t* newtask, void* filebase, Elf64_Shdr* header, bool rw, vma_sections_t section) {
    uint64_t nbytes = 0, phys_addr = 0;

    vma_t* vma = kalloc(sizeof(vma_t));
    vma->section = section;
    vma->vm_mm = newtask->mm;

    vma->start = (void*)header->sh_addr;
    vma->end = (void*)header->sh_addr + header->sh_size;

    size_t size = header->sh_size;

    uint64_t prologue = (uint64_t)vma->start % PGSZ;
    uint64_t epilogue = (uint64_t)vma->end % PGSZ;

    void* src = filebase + header->sh_offset;

    uint8_t va_flags = rw ? (VA_RW | VA_USER) : (VA_USER);

    if (prologue) {

        phys_addr = map_va(vma->start, PA2VA(newtask->cr3), va_flags);

        void* ceil_start_vaddr = (void*)CEIL(PGSZ, vma->start);
        nbytes = ceil_start_vaddr - vma->start;
        // kdebug("pro copying from %p nbytes %d", src, nbytes);
        if (section == BSS_SECTION) {
            k_memset(PA2VA(phys_addr) + prologue, 0, nbytes);
        } else {
            k_memcpy(PA2VA(phys_addr) + prologue, src, nbytes);
        }

        // update src and size
        src += nbytes;
        size -= nbytes;
    }

    // kdebug("src is now %p size %d", src, size);

    for (void* addr = (void*)CEIL(PGSZ, vma->start); addr < vma->end; addr += PGSZ) {

        phys_addr = map_va(addr, PA2VA(newtask->cr3), va_flags);

        if (section == BSS_SECTION) {
            k_memset(PA2VA(phys_addr), 0, PGSZ);
        } else {
            k_memcpy(PA2VA(phys_addr), src, PGSZ);
        }

        // update src and size
        src += PGSZ;
        size -= PGSZ;
    }

    if (epilogue) {

        phys_addr = map_va(vma->end, PA2VA(newtask->cr3), va_flags);

        nbytes = vma->end - vma->end;
        // kdebug("epi copying from %p nbytes %d", src, nbytes);
        if (section == BSS_SECTION) {
            k_memset(PA2VA(phys_addr), 0, nbytes);
        } else {
            k_memcpy(PA2VA(phys_addr), src, nbytes);
        }

        // update src and size
        src += nbytes;
        size -= nbytes;
    }

    if (vma->start == (void*)0x4000f0 && vma->end == (void*)0x401f02) {
        kdebug("CR3: %x", newtask->cr3);
        debug_walk(vma->start, PA2VA(newtask->cr3));
        debug_walk(vma->end, PA2VA(newtask->cr3));
    }
    // kdebug("src is now %p size %d nb %d", src, size, nbytes);
    insert_vma(newtask, vma);

    return vma;
}

vma_t* get_vma_by_index(int index, vma_t* head, bool allocate_for_it) {
    vma_t* cursor = head;
    int i = 0;
    if (allocate_for_it && cursor == NULL) {
        cursor = kalloc(sizeof(vma_t));
    }
    while (cursor != NULL && i < index) {
        cursor = cursor->next;
        if (allocate_for_it && cursor == NULL) {
            cursor = kalloc(sizeof(vma_t));
        }
        i++;
    }
    return cursor;
}

void insert_vma(task_t* proc, vma_t* vma) {
    mm_t* mm = proc->mm;
    if (mm->mapping == NULL || vma->start < mm->mapping->start) {
        vma->next = mm->mapping;
        mm->mapping = vma;
        vma->refcount++;
        return;
    }
    vma_t* cursor = mm->mapping;
    while (cursor->next != NULL && vma->start >= cursor->next->start ) {
        cursor = cursor->next;
    }
    vma->next = cursor->next;
    cursor->next = vma;
    vma->refcount++;
}

void free_vma(vma_t* vma, void* cr3) {
    vma->refcount--;
    if (vma->refcount == 0) {
        for (void* addr = vma->start; addr < vma->end; addr += PGSZ) {
            uintptr_t physpage = unmap_va(addr, cr3);
            phys_free(physpage);
        }
        kfree(vma);
    } else {
        for (void* addr = vma->start; addr < vma->end; addr += PGSZ) {
            unmap_va(addr, cr3);
        }
    }
}

void remove_vma(task_t* proc, vma_t* vma) {
    mm_t* mm = proc->mm;
    if (mm->mapping == NULL) {
        return;
    }
    if (mm->mapping == vma->start) {
        mm->mapping = vma->next;
        free_vma(vma, PA2VA(proc->cr3));
        return;
    }
    vma_t* cursor = mm->mapping;
    vma_t* target = cursor->next;
    while (target != NULL) {
        if (target->start == vma->start) {
            cursor->next = target->next;
            free_vma(target, PA2VA(proc->cr3));
        }
        cursor = cursor->next;
        target = target->next;
    }
}

void* create_page_table() {
    va_entry_t(*k_pml4)[TBLENTRIES] = kpml4;
    // Allocate space for new page table
    va_entry_t(*pml4)[TBLENTRIES] = PA2VA(phys_alloc(1));
    for (int i = 0; i < TBLENTRIES; i++) {
        (*pml4)[i] = (*k_pml4)[i];
    }
    // Map kernel
    //(*pml4)[TBLENTRIES - 1] = (*k_pml4)[TBLENTRIES - 1];
    //(*pml4)[TBLENTRIES - 2] = (*k_pml4)[TBLENTRIES - 2];

    return VA2PA(pml4);
}

// When function == NULL creates a userspace task
task_t* create_task(void* function) {
    // Allocate a new task
    task_t* newtask = kalloc(sizeof(task_t));
    // Set the stack pointer reference up
    newtask->phys_sp = valloc(PGSZ);
    newtask->rsp = newtask->phys_sp + PGSZ;
    newtask->tss_rsp = newtask->rsp;
    newtask->state = RUNNABLE;

    if (function != NULL) {
        uint64_t* setup_stack = newtask->rsp - 8;
        newtask->function = function;
        // 8 (task ptr) + 48 (6 registers, 8 bytes each) + 8 (rip)
        // Set jump point
        *setup_stack = (uint64_t)task_wrapper;
        // 6 registers are popped + rdi + rdx
        setup_stack -= 8;
        // task ptr
        newtask->rsp = (void*)setup_stack;
    } else {
        // setup userspace
        uint64_t* setup_stack = newtask->rsp;
        setup_stack -= 1;
        *setup_stack = (uint64_t)goto_userspace;
        setup_stack -= 8;
        newtask->rsp = (void*)setup_stack;
    }
    // Insert into list
    insert_task(newtask);
    return newtask;
}

void idle() {
    while (true) {
        schedule();
    }
    // for the day timer works: asm volatile("hlt");
}
