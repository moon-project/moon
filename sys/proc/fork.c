#include <sys/core/kutils.h>
#include <sys/core/proc.h>

task_t* copyproc(task_t* parent) {
    task_t* child = kalloc(sizeof(task_t));

    k_memcpy(child, parent, sizeof(task_t));

    child->cr3 = create_page_table();
    child->ppid = parent->pid;

    child->phys_sp = valloc(PGSZ);
    k_memcpy(child->phys_sp, parent->phys_sp, PGSZ);

    uint64_t stack_offset = parent->rsp - parent->phys_sp;
    child->rsp = child->phys_sp + stack_offset;
    child->tss_rsp = child->phys_sp + PGSZ;

    child->mm = kalloc(sizeof(mm_t));
    k_memcpy(child->mm, parent->mm, sizeof(mm_t));

    vma_t* parent_head = parent->mm->mapping;
    child->mm->mapping = NULL;

    vma_t* parent_cursor = parent_head;
    while (parent_cursor != NULL) {
        vma_t* child_vma = kalloc(sizeof(vma_t));
        k_memcpy(child_vma, parent_cursor, sizeof(vma_t));

        child_vma->vm_mm = child->mm;
        child_vma->next = NULL;
        uint8_t flgs = IS_WRITABLE(child_vma) ? (VA_RW | VA_USER) : (VA_USER);

        // size_t section_sz = parent_cursor->end - parent_cursor->start;

        for (void* ptr = (void*)FLOOR(PGSZ, child_vma->start); ptr < child_vma->end; ptr += PGSZ) {
            uintptr_t phys_addr = map_va(ptr, PA2VA(child->cr3), flgs);
            k_memcpy(PA2VA(phys_addr), ptr, PGSZ);
        }

        parent_cursor = parent_cursor->next;
        insert_vma(child, child_vma);
    }

    return child;
}
