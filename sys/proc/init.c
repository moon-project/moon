/**
 * @file init.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/init.h>
/**
 * @file init.c
 * @author Neal Beeken, Mrunal Patel
 * @brief Setup processes
 */

#include <sys/core/kutils.h>
#include <sys/core/proc.h>
#include <sys/core/tty.h>
#include <sys/core/vm.h>

void init_proc(const init_args_t const* args) {

    // Allocate a new task
    task_t* newtask = kalloc(sizeof(task_t));
    // Set the stack pointer reference up
    newtask->phys_sp = kalloc(PGSZ); // TODO: need to make page aligned
    // Top of stack is bottom of page
    newtask->rsp = newtask->phys_sp + PGSZ;
    newtask->function = idle;
    newtask->state = RUNNABLE;
    newtask->name = "idle";
    newtask->cr3 = VA2PA(kpml4);
    // 8 (task ptr) + 48 (6 registers, 8 bytes each) + 8 (rip)
    uint64_t* setup_stack = newtask->rsp - 8;
    // Set jump point
    *setup_stack = (uint64_t)task_wrapper;
    // task ptr
    newtask->rsp = (void*)setup_stack;
    // Insert into list6
    insert_task(newtask);

    task_t* init = make_bin_task("/bin/sh");

    fg_pid = init->pid;

    // Jump to the wrapper for the first task.
    asm volatile("movq %0, %%rsp\n\t"
                 "retq\n\t"
                 :
                 : "r"(setup_stack));

    panics("NO!!!");
}
