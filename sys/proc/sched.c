/**
 * @file sched.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/asm.h>
#include <sys/core/gdt.h>
#include <sys/core/kutils.h>
#include <sys/core/proc.h>
#include <sys/core/vm.h>

task_t* tasklisthead = NULL;
task_t* tasklistcursor = NULL;
pid_t highestpid = 0;

static void roundrobin(task_t** current, task_t** next) {
    // if (*current == NULL) {
    //    *current = (void*)&placeholder;
    //} else {
    //    *current = tasklistcursor;
    //}
    *current = tasklistcursor;
    task_t* candidate = tasklistcursor->next;
    while (candidate->state != RUNNABLE) {
        candidate = candidate->next;
    }
    tasklistcursor = candidate;
    *next = tasklistcursor;
}

void insert_task(task_t* task) {
    // Insert at the head
    if (tasklisthead == NULL) {
        tasklistcursor = task;
        tasklisthead = task;
        task->prev = task;
    } else {
        tasklisthead->prev->next = task;
        task->prev = tasklisthead->prev;
        tasklisthead->prev = task;
    }
    task->next = tasklisthead;
    task->pid = ++highestpid;
    tasklisthead = task;
}

void remove_task(task_t* task) {
    // Insert at the head
    if (task == tasklisthead) {
        tasklisthead = tasklisthead->next;
    }
    if (task == tasklistcursor) {
        tasklistcursor = tasklistcursor->next;
    }
    task->next->prev = task->prev;
    task->prev->next = task->next;
}

task_t* getcurrenttask() {
    return tasklistcursor;
}

task_t* gettask(pid_t pid) {
    panicif(pid == 0);
    for (task_t* cursor = tasklisthead; cursor->next != tasklisthead; cursor = cursor->next) {
        if (cursor->pid == pid) {
            return cursor;
        }
    }
    return NULL;
}

void ctxswitch(task_t* current, task_t* next, task_t** prev) {
    // kinfo("Next's cr3: %p", PA2VA(next->cr3));
    // debug_walk((void*)0x400020, PA2VA(next->cr3));
    asm volatile("test %[current], %[current]\n\t"
                 "jz first_switch\n\t" PUSH_CALLEE "push %%rdi\n\t" // save current on current's stack
                 "push %%rdx\n\t" // save pointer to prev on current's stack
                 "movq %%rsp, %P[taskrsp](%[current])\n\t" // "movq %%rsp, 0(%%rdi)\n\t" // save current's stack pointer
                 "first_switch:\n\t"
                 "movq %P[taskrsp](%[next]), %%rsp\n\t" // "movq 0(%%rsi), %%rsp\n\t" // switch to next task
                 "movq %[taskcr3], %%cr3\n\t"
                 "pop %%rdx\n\t" // get next's pointer to prev
                 "test %%rdx, %%rdx\n\t" // check for null pointer
                 "jz last_is_null\n\t"
                 "movq %%rdi, 0(%%rdx)\n\t" // save current as next's prev
                 "last_is_null:\n\t"
                 "pop %%rdi\n\t" // update current to next
                 POP_CALLEE "\n\tret\n\t"
                 // if task->userspace ????? restore only if task is going to userspace?
                 // restore context
                 // RESTORE_CONTEXT
                 // restore everything (the registers) from task struct
                 // set cr3 from task struct
                 // "movq %P[taskcr3](%[current]), cr3\n\t"
                 // else ?????
                 // TODO: is this supposed to be current or next?
                 // set userspace stuff only if mm field is not NULL.
                 // read iretq stack documentation to restore from stack
                 : // OUTPUT
                 : [current] "D"(current), // INPUT
                 [next] "S"(next), [taskrsp] "i"(offsetof(task_t, rsp)),
                 [taskcr3] "r"(next->cr3)
                 : // CLOBBER
    );
}

void schedule() {
    task_t* current_task = NULL;
    task_t* next_task = NULL;
    task_t* prev_task = NULL;
    // Set current_task and next_task
    roundrobin(&current_task, &next_task);
    // kinfo("PRE : C: %p\tN: %p\tP: %p", current_task, next_task, prev_task);
    // task_t *me, *next, *last;
    // context switch
    ctxswitch(current_task, next_task, &prev_task);
    // clean up maybe
    // pid_t prev_pid = (prev_task == NULL) ? -1 : prev_task->pid;
    if (prev_task->state == EXITED) {
        kwarn("TODO CLEAN FUNCTION");
    }
}

void goto_userspace() {
    task_t* current_task = getcurrenttask();
    setcr3((uint64_t)current_task->cr3);
    set_tss_rsp(current_task->tss_rsp);
    debug_walk((void*)0x4000f0, PA2VA(current_task->cr3));
    debug_walk((void*)0x401f02, PA2VA(current_task->cr3));
    asm volatile("mov $0x23, %%r10\n\t"
                 "mov %%r10, %%ds\n\t"
                 "mov %%r10, %%es\n\t"
                 "mov %%r10, %%fs\n\t"
                 "mov %%r10, %%gs\n\t"
                 // set rsp on iretq stack to be STACK_INIT
                 "movq %P[stack], %%r10\n\t"
                 // userspace setup
                 "push $0x23\n\t"
                 "push %%r10\n\t" // userspace rsp
                 "pushf\n\t"
                 "push $0x2B\n\t"
                 "movq %P[rip], %%r10\n\t"
                 "push %%r10\n\t" // userspace rip
                 "iretq\n\t"
                 : //
                 : [stack] "r"(STACK_INIT - 24), [rip] "r"(ENTRY)
                 :);
}

void task_wrapper() {
    void (*fun)(void) = tasklistcursor->function;
    fun();
    tasklistcursor->state = EXITED;
    while (true) {
        schedule();
    }
}

void listprocs() {
    task_t* cursor = tasklistcursor;
    kprintf("NAME\tPID\tPPID\n");
    do {
        if (cursor->state != ZOMBIE && cursor->state != EXITED)
            kprintf("%s\t%d\t%d\n", cursor->name, cursor->pid, cursor->ppid);
        cursor = cursor->next;
    } while (cursor->next != tasklistcursor);
}
