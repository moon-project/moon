/**
 * @file elf64.c
 * @author Neal Beeken, Mrunal Patel
 * @brief ELF managing and loading utilities
 */

#include <sys/core/elf64.h>
#include <sys/core/kutils.h>

void readelf(void* data, elf_t* elf) {

    k_bzero(elf, sizeof(elf_t));

    elf->elf_hdr = data;

    if (!k_memcmp("\177ELF", elf->elf_hdr->e_ident, 4)) {
        panics("AHHHHHHHH NOT ELF");
        return; // do nothing
    }

    elf->prog_hdrs_ct = elf->elf_hdr->e_phnum;
    elf->prog_hdrs = BYBYTE(elf->elf_hdr, +elf->elf_hdr->e_phoff);

    elf->section_table_ct = elf->elf_hdr->e_shnum;
    elf->section_table = BYBYTE(elf->elf_hdr, +elf->elf_hdr->e_shoff);

    elf->shstrtab_base = BYBYTE(elf->elf_hdr, +elf->section_table[elf->elf_hdr->e_shstrndx].sh_offset);

    for (int i = 0; i < elf->section_table_ct; i++) {
        char* section_name = elf->shstrtab_base + elf->section_table[i].sh_name;
        if (k_strcmp(section_name, ".data")) {
            elf->data_hdr = &elf->section_table[i];
            elf->data = BYBYTE(elf->elf_hdr, +elf->section_table[i].sh_offset);
        } else if (k_strcmp(section_name, ".text")) {
            elf->text_hdr = &elf->section_table[i];
            elf->text = BYBYTE(elf->elf_hdr, +elf->section_table[i].sh_offset);
        } else if (k_strcmp(section_name, ".rodata")) {
            elf->rodata_hdr = &elf->section_table[i];
            elf->rodata = BYBYTE(elf->elf_hdr, +elf->section_table[i].sh_offset);
        } else if (k_strcmp(section_name, ".bss")) {
            elf->bss_hdr = &elf->section_table[i];
            elf->bss = BYBYTE(elf->elf_hdr, +elf->section_table[i].sh_offset);
        }
    }
}
