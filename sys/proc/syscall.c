/**
 * @file syscall.c
 * @author Neal Beeken, Mrunal Patel
 * @brief Syscall table and entry point
 */

#include <sys/core/asm.h>
#include <sys/core/kutils.h>
#include <sys/core/proc.h>
#include <sys/core/syscall.h>
#include <sys/core/tarfs.h>
#include <sys/defs.h>
#include <sys/syscall.h>

int ksys_read(interrupt_stack_t* args);
int ksys_write(interrupt_stack_t* args);
int ksys_open(interrupt_stack_t* args);
int ksys_close(interrupt_stack_t* args);
int ksys_stat(interrupt_stack_t* args);
int ksys_fstat(interrupt_stack_t* args);
int ksys_brk(interrupt_stack_t* args);
int ksys_rtsigaction(interrupt_stack_t* args);
int ksys_sigprogmask(interrupt_stack_t* args);
int ksys_sigreturn(interrupt_stack_t* args);
int ksys_ioctl(interrupt_stack_t* args);
int ksys_access(interrupt_stack_t* args);
int ksys_yield(interrupt_stack_t* args);
int ksys_dup2(interrupt_stack_t* args);
int ksys_sleep(interrupt_stack_t* args);
int ksys_getpid(interrupt_stack_t* args);
int ksys_fork(interrupt_stack_t* args);
int ksys_exec(interrupt_stack_t* args);
int ksys_exit(interrupt_stack_t* args);
int ksys_wait(interrupt_stack_t* args);
int ksys_kill(interrupt_stack_t* args);
int ksys_getdents(interrupt_stack_t* args);
int ksys_getcwd(interrupt_stack_t* args);
int ksys_chdir(interrupt_stack_t* args);
int ksys_rename(interrupt_stack_t* args);
int ksys_mkdir(interrupt_stack_t* args);
int ksys_rmdir(interrupt_stack_t* args);
int ksys_create(interrupt_stack_t* args);
int ksys_unlink(interrupt_stack_t* args);
int ksys_getrusage(interrupt_stack_t* args);
int ksys_getppid(interrupt_stack_t* args);
int ksys_reboot(interrupt_stack_t* args);
int ksys_pipe2(interrupt_stack_t* args);

#define GENERATE_FUNC_AT_POS(NUM, FUN, _) [NUM] FUN,

syscall_f syscall_funcs[] = {
    [SYS_READ] = ksys_read,
    [SYS_WRITE] = ksys_write,
    [SYS_OPEN] = ksys_open,
    [SYS_CLOSE] = ksys_close,
    [SYS_STAT] = ksys_stat,
    [SYS_FSTAT] = ksys_fstat,
    [SYS_BRK] = ksys_brk,
    [SYS_RTSIGACTION] = ksys_rtsigaction,
    [SYS_SIGPROGMASK] = ksys_sigprogmask,
    [SYS_SIGRETURN] = ksys_sigreturn,
    [SYS_IOCTL] = ksys_ioctl,
    [SYS_ACCESS] = ksys_access,
    [SYS_YIELD] = ksys_yield,
    [SYS_DUP2] = ksys_dup2,
    [SYS_SLEEP] = ksys_sleep,
    [SYS_GETPID] = ksys_getpid,
    [SYS_FORK] = ksys_fork,
    [SYS_EXEC] = ksys_exec,
    [SYS_EXIT] = ksys_exit,
    [SYS_WAIT] = ksys_wait,
    [SYS_KILL] = ksys_kill,
    [SYS_GETDENTS] = ksys_getdents,
    [SYS_GETCWD] = ksys_getcwd,
    [SYS_CHDIR] = ksys_chdir,
    [SYS_RENAME] = ksys_rename,
    [SYS_MKDIR] = ksys_mkdir,
    [SYS_RMDIR] = ksys_rmdir,
    [SYS_CREATE] = ksys_create,
    [SYS_UNLINK] = ksys_unlink,
    [SYS_GETRUSAGE] = ksys_getrusage,
    [SYS_GETPPID] = ksys_getppid,
    [SYS_REBOOT] = ksys_reboot,
    [SYS_PIPE2] = ksys_pipe2,
};

syscall_f getsyscall(uint64_t syscall_num) {
    // kprintf("SYSCALL %d\n", syscall_num);
    syscall_f syscallfunction = syscall_funcs[syscall_num];
    panicif(syscallfunction == NULL);
    return syscallfunction;
}

int ksys_read(interrupt_stack_t* args) {

    int file = args->rdi;
    char* buf = (void*)args->rsi;
    size_t size = args->rdx;

    task_t* task = getcurrenttask();

    task->state = WAITING;

    enable_interrupts();
    schedule();

    return (args->rax = task->filetable.inodes[file]->superblock->interface.read_f(file, buf, size));
}

int ksys_write(interrupt_stack_t* args) {

    int file = args->rdi;
    char* buf = (void*)args->rsi;
    size_t size = args->rdx;

    task_t* task = getcurrenttask();

    return (args->rax = task->filetable.inodes[file]->superblock->interface.write_f(file, buf, size));
}

int ksys_open(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_open");
    return 0;
}

int ksys_close(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_close");
    return 0;
}

int ksys_stat(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_stat");
    return 0;
}

int ksys_fstat(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_fstat");
    return 0;
}

// This works based on the assumption that brk is only raised by page sizes
// The addr argument from brk syscall is ignored.
int ksys_brk(interrupt_stack_t* args) {
    task_t* current_task = getcurrenttask();
    // Check if this is the first call
    bool first_call = true;
    vma_t* vma = current_task->mm->mapping;
    while (vma != NULL) {
        if (vma->section == HEAP_SECTION) {
            first_call = false;
            break;
        }
        vma = vma->next;
    }
    void* addr = NULL;
    if (first_call) {
        vma = kalloc(sizeof(vma_t));
        vma->section = HEAP_SECTION;
        vma->vm_mm = current_task->mm;
        vma->start = HEAP_INIT;
        vma->end = (void*)((uintptr_t)HEAP_INIT + PGSZ);
        addr = HEAP_INIT;
        map_va(addr, PA2VA(current_task->cr3), VA_USER | VA_RW);
        insert_vma(current_task, vma);
    } else {
        addr = vma->end;
        map_va(addr, PA2VA(current_task->cr3), VA_USER | VA_RW);
        vma->end = (void*)((uintptr_t)vma->end + PGSZ);
    }
    // set return value: start of new brk
    args->rax = (uint64_t)addr;
    return 0;
}

int ksys_rtsigaction(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_rtsigaction");
    return 0;
}

int ksys_sigprogmask(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_sigprogmask");
    return 0;
}

int ksys_sigreturn(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_sigreturn");
    return 0;
}

int ksys_ioctl(interrupt_stack_t* args) {
    listprocs();
    return 0;
}

int ksys_access(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_access");
    return 0;
}

int ksys_yield(interrupt_stack_t* args) {
    schedule();
    return 0;
}

int ksys_dup2(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_dup2");
    return 0;
}

int ksys_sleep(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_sleep");
    return 0;
}

int ksys_getpid(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_getpid");
    args->rax = getcurrenttask()->pid;
    return 0;
}

int ksys_fork(interrupt_stack_t* args) {
    kprintf("%s - %p\n", "ksys_fork", args);
    task_t* parent = getcurrenttask();
    task_t* child = copyproc(parent);

    insert_task(child);

    asm volatile("movq %P[physsp](%[parent]), %%r10\n\t"
                 "movq %%rsp, %%r11\n\t"
                 "sub %%r10, %%r11\n\t"
                 "movq %P[physsp](%[child]), %%r10\n\t"
                 "add %%r10, %%r11\n\t"
                 "movq %%r11, %P[rsp](%[child])\n\t"
                 :
                 : [physsp] "i"(offsetof(task_t, phys_sp)), [rsp] "i"(offsetof(task_t, rsp)), [parent] "X"(parent),
                 [child] "X"(child)
                 :);

    uint64_t* setup_stack = child->rsp;
    setup_stack -= 1;
    *setup_stack = (uint64_t)forkret;
    setup_stack -= 8;
    child->rsp = (void*)setup_stack;

    args->rax = child->pid;

    return 0;
}

void forkret() {
    asm volatile("add $24, %%rsp\n\t" :::);
    return;
}

int ksys_exec(interrupt_stack_t* args) {

    char* filename = (char*)args->rdi;

    make_bin_task(filename);

    return 0;
}

int ksys_exit(interrupt_stack_t* args) {
    task_t* current_task = getcurrenttask();
    current_task->state = ZOMBIE;
    current_task->exit_code = args->rdi;
    enable_interrupts();
    schedule();
    return 0;
}

task_t* get_zombies(task_t* parent) {
    task_t* cursor = tasklisthead;
    bool found_child = false;
    while (cursor->next != tasklisthead) {
        if (cursor->ppid == parent->pid) {
            found_child = true;
            if (cursor->state == ZOMBIE) {
                return cursor;
            }
        }
        cursor = cursor->next;
    }
    return (found_child) ? NULL : (void*)-1;
}

int ksys_wait(interrupt_stack_t* args) {
    task_t* parent = getcurrenttask();
    task_t* child = NULL;
    if (args->rdi == 0xffffffff) {
        while ((child = get_zombies(parent)) == NULL) {
            enable_interrupts();
            schedule();
        }
        if (child == (void*)-1) {
            kinfo("No children found for parent %d\n", parent->pid);
            args->rax = -1;
            return 0;
        }
    } else {
        child = gettask(args->rdi);
        if (child->ppid != parent->pid) {
            kinfo("Child %d not found for parent %d\n", child->pid, parent->pid);
            args->rax = -1;
            return 0;
        }
        while (child->state != ZOMBIE) {
            enable_interrupts();
            schedule();
        }
    }
    *(uint64_t*)(args->rsi) = child->exit_code;
    args->rax = child->pid;
    kwarn("TODO: clean child %d up!", args->rax);
    return 0;
}

int ksys_kill(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_kill");
    return 0;
}

int ksys_getdents(interrupt_stack_t* args) {
    // char* buf = (char*)args->rdi;
    // size_t sz = (char*)args->rdx;

    // tar_filesystem->interface.getdents_f(0, buf, sz);

    return 0;
}

int ksys_getcwd(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_getcwd");
    return 0;
}

int ksys_chdir(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_chdir");
    return 0;
}

int ksys_rename(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_rename");
    return 0;
}

int ksys_mkdir(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_mkdir");
    return 0;
}

int ksys_rmdir(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_rmdir");
    return 0;
}

int ksys_create(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_create");
    return 0;
}

int ksys_unlink(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_unlink");
    return 0;
}

int ksys_getrusage(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_getrusage");
    return 0;
}

int ksys_getppid(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_getppid");
    return 0;
}

int ksys_reboot(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_reboot");
    return 0;
}

int ksys_pipe2(interrupt_stack_t* args) {
    kprintf("%s\n", "ksys_pipe2");
    return 0;
}
