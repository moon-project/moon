
.globl isr_entrypoint
.globl isr_epilogue
.align 4

# Push all registers that matter on to stack
.macro pushaq
    movq %rsp, %r10
    pushq %rax
    pushq %rcx
    pushq %rdx
    pushq %rbx
    pushq %r10
    pushq %rbp
    pushq %rsi
    pushq %rdi
.endm

# Pop all registers that matter off the stack
.macro popaq
    popq %rdi
    popq %rsi
    popq %rbp
    popq %r10
    popq %rbx
    popq %rdx
    popq %rcx
    popq %rax
    movq %r10, %rsp
.endm

isr_entrypoint:
    # ABI convention for SysV
    cld
    call isr_selector
    # goto: epilogue
isr_epilogue:
    popaq
    iretq

isr0:
    pushaq
    movq $0, %rsi
    movq $0, %rdi
    jmp isr_entrypoint

isr1:
    pushaq
    movq $0, %rsi
    movq $1, %rdi
    jmp isr_entrypoint

isr2:
    pushaq
    movq $0, %rsi
    movq $2, %rdi
    jmp isr_entrypoint

isr3:
    pushaq
    movq $0, %rsi
    movq $3, %rdi
    jmp isr_entrypoint

isr4:
    pushaq
    movq $0, %rsi
    movq $4, %rdi
    jmp isr_entrypoint

isr5:
    pushaq
    movq $0, %rsi
    movq $5, %rdi
    jmp isr_entrypoint

isr6:
    pushaq
    movq $0, %rsi
    movq $6, %rdi
    jmp isr_entrypoint

isr7:
    pushaq
    movq $0, %rsi
    movq $7, %rdi
    jmp isr_entrypoint

isr8:
    pushaq
    movq 64(%rsp), %rsi
    movq $8, %rdi
    jmp isr_entrypoint

isr9:
    pushaq
    movq $0, %rsi
    movq $9, %rdi
    jmp isr_entrypoint

isr10:
    pushaq
    movq 64(%rsp), %rsi
    movq $10, %rdi
    jmp isr_entrypoint

isr11:
    pushaq
    movq 64(%rsp), %rsi
    movq $11, %rdi
    jmp isr_entrypoint

isr12:
    pushaq
    movq 64(%rsp), %rsi
    movq $12, %rdi
    jmp isr_entrypoint

isr13:
    pop %r11
    pushaq
    movq %r11, %rsi
    movq $13, %rdi
    jmp isr_entrypoint

isr14:
    pop %r11
    pushaq
    movq %r11, %rsi
    movq $14, %rdi
    jmp isr_entrypoint

isr15:
    pushaq
    movq $0, %rsi
    movq $15, %rdi
    jmp isr_entrypoint

isr16:
    pushaq
    movq $0, %rsi
    movq $16, %rdi
    jmp isr_entrypoint

isr17:
    pushaq
    movq 64(%rsp), %rsi
    movq $17, %rdi
    jmp isr_entrypoint

isr18:
    pushaq
    movq $0, %rsi
    movq $18, %rdi
    jmp isr_entrypoint

isr19:
    pushaq
    movq $0, %rsi
    movq $19, %rdi
    jmp isr_entrypoint

isr20:
    pushaq
    movq $0, %rsi
    movq $20, %rdi
    jmp isr_entrypoint

isr21:
    pushaq
    movq $0, %rsi
    movq $21, %rdi
    jmp isr_entrypoint

isr22:
    pushaq
    movq $0, %rsi
    movq $22, %rdi
    jmp isr_entrypoint

isr23:
    pushaq
    movq $0, %rsi
    movq $23, %rdi
    jmp isr_entrypoint

isr24:
    pushaq
    movq $0, %rsi
    movq $24, %rdi
    jmp isr_entrypoint

isr25:
    pushaq
    movq $0, %rsi
    movq $25, %rdi
    jmp isr_entrypoint

isr26:
    pushaq
    movq $0, %rsi
    movq $26, %rdi
    jmp isr_entrypoint

isr27:
    pushaq
    movq $0, %rsi
    movq $27, %rdi
    jmp isr_entrypoint

isr28:
    pushaq
    movq $0, %rsi
    movq $28, %rdi
    jmp isr_entrypoint

isr29:
    pushaq
    movq $0, %rsi
    movq $29, %rdi
    jmp isr_entrypoint

isr30:
    pushaq
    movq 64(%rsp), %rsi
    movq $30, %rdi
    jmp isr_entrypoint

isr31:
    pushaq
    movq $0, %rsi
    movq $31, %rdi
    jmp isr_entrypoint

isr32:
    pushaq
    movq $0, %rsi
    movq $32, %rdi
    jmp isr_entrypoint

isr33:
    pushaq
    movq $0, %rsi
    movq $33, %rdi
    jmp isr_entrypoint

isr34:
    pushaq
    movq $0, %rsi
    movq $34, %rdi
    jmp isr_entrypoint

isr35:
    pushaq
    movq $0, %rsi
    movq $35, %rdi
    jmp isr_entrypoint

isr36:
    pushaq
    movq $0, %rsi
    movq $36, %rdi
    jmp isr_entrypoint

isr37:
    pushaq
    movq $0, %rsi
    movq $37, %rdi
    jmp isr_entrypoint

isr38:
    pushaq
    movq $0, %rsi
    movq $38, %rdi
    jmp isr_entrypoint

isr39:
    pushaq
    movq $0, %rsi
    movq $39, %rdi
    jmp isr_entrypoint

isr40:
    pushaq
    movq $0, %rsi
    movq $40, %rdi
    jmp isr_entrypoint

isr41:
    pushaq
    movq $0, %rsi
    movq $41, %rdi
    jmp isr_entrypoint

isr42:
    pushaq
    movq $0, %rsi
    movq $42, %rdi
    jmp isr_entrypoint

isr43:
    pushaq
    movq $0, %rsi
    movq $43, %rdi
    jmp isr_entrypoint

isr44:
    pushaq
    movq $0, %rsi
    movq $44, %rdi
    jmp isr_entrypoint

isr45:
    pushaq
    movq $0, %rsi
    movq $45, %rdi
    jmp isr_entrypoint

isr46:
    pushaq
    movq $0, %rsi
    movq $46, %rdi
    jmp isr_entrypoint

isr47:
    pushaq
    movq $0, %rsi
    movq $47, %rdi
    jmp isr_entrypoint

isr48:
    pushaq
    movq $0, %rsi
    movq $48, %rdi
    jmp isr_entrypoint

isr49:
    pushaq
    movq $0, %rsi
    movq $49, %rdi
    jmp isr_entrypoint

isr50:
    pushaq
    movq $0, %rsi
    movq $50, %rdi
    jmp isr_entrypoint

isr51:
    pushaq
    movq $0, %rsi
    movq $51, %rdi
    jmp isr_entrypoint

isr52:
    pushaq
    movq $0, %rsi
    movq $52, %rdi
    jmp isr_entrypoint

isr53:
    pushaq
    movq $0, %rsi
    movq $53, %rdi
    jmp isr_entrypoint

isr54:
    pushaq
    movq $0, %rsi
    movq $54, %rdi
    jmp isr_entrypoint

isr55:
    pushaq
    movq $0, %rsi
    movq $55, %rdi
    jmp isr_entrypoint

isr56:
    pushaq
    movq $0, %rsi
    movq $56, %rdi
    jmp isr_entrypoint

isr57:
    pushaq
    movq $0, %rsi
    movq $57, %rdi
    jmp isr_entrypoint

isr58:
    pushaq
    movq $0, %rsi
    movq $58, %rdi
    jmp isr_entrypoint

isr59:
    pushaq
    movq $0, %rsi
    movq $59, %rdi
    jmp isr_entrypoint

isr60:
    pushaq
    movq $0, %rsi
    movq $60, %rdi
    jmp isr_entrypoint

isr61:
    pushaq
    movq $0, %rsi
    movq $61, %rdi
    jmp isr_entrypoint

isr62:
    pushaq
    movq $0, %rsi
    movq $62, %rdi
    jmp isr_entrypoint

isr63:
    pushaq
    movq $0, %rsi
    movq $63, %rdi
    jmp isr_entrypoint

isr64:
    pushaq
    movq $0, %rsi
    movq $64, %rdi
    jmp isr_entrypoint

isr65:
    pushaq
    movq $0, %rsi
    movq $65, %rdi
    jmp isr_entrypoint

isr66:
    pushaq
    movq $0, %rsi
    movq $66, %rdi
    jmp isr_entrypoint

isr67:
    pushaq
    movq $0, %rsi
    movq $67, %rdi
    jmp isr_entrypoint

isr68:
    pushaq
    movq $0, %rsi
    movq $68, %rdi
    jmp isr_entrypoint

isr69:
    pushaq
    movq $0, %rsi
    movq $69, %rdi
    jmp isr_entrypoint

isr70:
    pushaq
    movq $0, %rsi
    movq $70, %rdi
    jmp isr_entrypoint

isr71:
    pushaq
    movq $0, %rsi
    movq $71, %rdi
    jmp isr_entrypoint

isr72:
    pushaq
    movq $0, %rsi
    movq $72, %rdi
    jmp isr_entrypoint

isr73:
    pushaq
    movq $0, %rsi
    movq $73, %rdi
    jmp isr_entrypoint

isr74:
    pushaq
    movq $0, %rsi
    movq $74, %rdi
    jmp isr_entrypoint

isr75:
    pushaq
    movq $0, %rsi
    movq $75, %rdi
    jmp isr_entrypoint

isr76:
    pushaq
    movq $0, %rsi
    movq $76, %rdi
    jmp isr_entrypoint

isr77:
    pushaq
    movq $0, %rsi
    movq $77, %rdi
    jmp isr_entrypoint

isr78:
    pushaq
    movq $0, %rsi
    movq $78, %rdi
    jmp isr_entrypoint

isr79:
    pushaq
    movq $0, %rsi
    movq $79, %rdi
    jmp isr_entrypoint

isr80:
    pushaq
    movq $0, %rsi
    movq $80, %rdi
    jmp isr_entrypoint

isr81:
    pushaq
    movq $0, %rsi
    movq $81, %rdi
    jmp isr_entrypoint

isr82:
    pushaq
    movq $0, %rsi
    movq $82, %rdi
    jmp isr_entrypoint

isr83:
    pushaq
    movq $0, %rsi
    movq $83, %rdi
    jmp isr_entrypoint

isr84:
    pushaq
    movq $0, %rsi
    movq $84, %rdi
    jmp isr_entrypoint

isr85:
    pushaq
    movq $0, %rsi
    movq $85, %rdi
    jmp isr_entrypoint

isr86:
    pushaq
    movq $0, %rsi
    movq $86, %rdi
    jmp isr_entrypoint

isr87:
    pushaq
    movq $0, %rsi
    movq $87, %rdi
    jmp isr_entrypoint

isr88:
    pushaq
    movq $0, %rsi
    movq $88, %rdi
    jmp isr_entrypoint

isr89:
    pushaq
    movq $0, %rsi
    movq $89, %rdi
    jmp isr_entrypoint

isr90:
    pushaq
    movq $0, %rsi
    movq $90, %rdi
    jmp isr_entrypoint

isr91:
    pushaq
    movq $0, %rsi
    movq $91, %rdi
    jmp isr_entrypoint

isr92:
    pushaq
    movq $0, %rsi
    movq $92, %rdi
    jmp isr_entrypoint

isr93:
    pushaq
    movq $0, %rsi
    movq $93, %rdi
    jmp isr_entrypoint

isr94:
    pushaq
    movq $0, %rsi
    movq $94, %rdi
    jmp isr_entrypoint

isr95:
    pushaq
    movq $0, %rsi
    movq $95, %rdi
    jmp isr_entrypoint

isr96:
    pushaq
    movq $0, %rsi
    movq $96, %rdi
    jmp isr_entrypoint

isr97:
    pushaq
    movq $0, %rsi
    movq $97, %rdi
    jmp isr_entrypoint

isr98:
    pushaq
    movq $0, %rsi
    movq $98, %rdi
    jmp isr_entrypoint

isr99:
    pushaq
    movq $0, %rsi
    movq $99, %rdi
    jmp isr_entrypoint

isr100:
    pushaq
    movq $0, %rsi
    movq $100, %rdi
    jmp isr_entrypoint

isr101:
    pushaq
    movq $0, %rsi
    movq $101, %rdi
    jmp isr_entrypoint

isr102:
    pushaq
    movq $0, %rsi
    movq $102, %rdi
    jmp isr_entrypoint

isr103:
    pushaq
    movq $0, %rsi
    movq $103, %rdi
    jmp isr_entrypoint

isr104:
    pushaq
    movq $0, %rsi
    movq $104, %rdi
    jmp isr_entrypoint

isr105:
    pushaq
    movq $0, %rsi
    movq $105, %rdi
    jmp isr_entrypoint

isr106:
    pushaq
    movq $0, %rsi
    movq $106, %rdi
    jmp isr_entrypoint

isr107:
    pushaq
    movq $0, %rsi
    movq $107, %rdi
    jmp isr_entrypoint

isr108:
    pushaq
    movq $0, %rsi
    movq $108, %rdi
    jmp isr_entrypoint

isr109:
    pushaq
    movq $0, %rsi
    movq $109, %rdi
    jmp isr_entrypoint

isr110:
    pushaq
    movq $0, %rsi
    movq $110, %rdi
    jmp isr_entrypoint

isr111:
    pushaq
    movq $0, %rsi
    movq $111, %rdi
    jmp isr_entrypoint

isr112:
    pushaq
    movq $0, %rsi
    movq $112, %rdi
    jmp isr_entrypoint

isr113:
    pushaq
    movq $0, %rsi
    movq $113, %rdi
    jmp isr_entrypoint

isr114:
    pushaq
    movq $0, %rsi
    movq $114, %rdi
    jmp isr_entrypoint

isr115:
    pushaq
    movq $0, %rsi
    movq $115, %rdi
    jmp isr_entrypoint

isr116:
    pushaq
    movq $0, %rsi
    movq $116, %rdi
    jmp isr_entrypoint

isr117:
    pushaq
    movq $0, %rsi
    movq $117, %rdi
    jmp isr_entrypoint

isr118:
    pushaq
    movq $0, %rsi
    movq $118, %rdi
    jmp isr_entrypoint

isr119:
    pushaq
    movq $0, %rsi
    movq $119, %rdi
    jmp isr_entrypoint

isr120:
    pushaq
    movq $0, %rsi
    movq $120, %rdi
    jmp isr_entrypoint

isr121:
    pushaq
    movq $0, %rsi
    movq $121, %rdi
    jmp isr_entrypoint

isr122:
    pushaq
    movq $0, %rsi
    movq $122, %rdi
    jmp isr_entrypoint

isr123:
    pushaq
    movq $0, %rsi
    movq $123, %rdi
    jmp isr_entrypoint

isr124:
    pushaq
    movq $0, %rsi
    movq $124, %rdi
    jmp isr_entrypoint

isr125:
    pushaq
    movq $0, %rsi
    movq $125, %rdi
    jmp isr_entrypoint

isr126:
    pushaq
    movq $0, %rsi
    movq $126, %rdi
    jmp isr_entrypoint

isr127:
    pushaq
    movq $0, %rsi
    movq $127, %rdi
    jmp isr_entrypoint

isr128:
    pushaq
    movq $0, %rsi
    movq $128, %rdi
    jmp isr_entrypoint

isr129:
    pushaq
    movq $0, %rsi
    movq $129, %rdi
    jmp isr_entrypoint

isr130:
    pushaq
    movq $0, %rsi
    movq $130, %rdi
    jmp isr_entrypoint

isr131:
    pushaq
    movq $0, %rsi
    movq $131, %rdi
    jmp isr_entrypoint

isr132:
    pushaq
    movq $0, %rsi
    movq $132, %rdi
    jmp isr_entrypoint

isr133:
    pushaq
    movq $0, %rsi
    movq $133, %rdi
    jmp isr_entrypoint

isr134:
    pushaq
    movq $0, %rsi
    movq $134, %rdi
    jmp isr_entrypoint

isr135:
    pushaq
    movq $0, %rsi
    movq $135, %rdi
    jmp isr_entrypoint

isr136:
    pushaq
    movq $0, %rsi
    movq $136, %rdi
    jmp isr_entrypoint

isr137:
    pushaq
    movq $0, %rsi
    movq $137, %rdi
    jmp isr_entrypoint

isr138:
    pushaq
    movq $0, %rsi
    movq $138, %rdi
    jmp isr_entrypoint

isr139:
    pushaq
    movq $0, %rsi
    movq $139, %rdi
    jmp isr_entrypoint

isr140:
    pushaq
    movq $0, %rsi
    movq $140, %rdi
    jmp isr_entrypoint

isr141:
    pushaq
    movq $0, %rsi
    movq $141, %rdi
    jmp isr_entrypoint

isr142:
    pushaq
    movq $0, %rsi
    movq $142, %rdi
    jmp isr_entrypoint

isr143:
    pushaq
    movq $0, %rsi
    movq $143, %rdi
    jmp isr_entrypoint

isr144:
    pushaq
    movq $0, %rsi
    movq $144, %rdi
    jmp isr_entrypoint

isr145:
    pushaq
    movq $0, %rsi
    movq $145, %rdi
    jmp isr_entrypoint

isr146:
    pushaq
    movq $0, %rsi
    movq $146, %rdi
    jmp isr_entrypoint

isr147:
    pushaq
    movq $0, %rsi
    movq $147, %rdi
    jmp isr_entrypoint

isr148:
    pushaq
    movq $0, %rsi
    movq $148, %rdi
    jmp isr_entrypoint

isr149:
    pushaq
    movq $0, %rsi
    movq $149, %rdi
    jmp isr_entrypoint

isr150:
    pushaq
    movq $0, %rsi
    movq $150, %rdi
    jmp isr_entrypoint

isr151:
    pushaq
    movq $0, %rsi
    movq $151, %rdi
    jmp isr_entrypoint

isr152:
    pushaq
    movq $0, %rsi
    movq $152, %rdi
    jmp isr_entrypoint

isr153:
    pushaq
    movq $0, %rsi
    movq $153, %rdi
    jmp isr_entrypoint

isr154:
    pushaq
    movq $0, %rsi
    movq $154, %rdi
    jmp isr_entrypoint

isr155:
    pushaq
    movq $0, %rsi
    movq $155, %rdi
    jmp isr_entrypoint

isr156:
    pushaq
    movq $0, %rsi
    movq $156, %rdi
    jmp isr_entrypoint

isr157:
    pushaq
    movq $0, %rsi
    movq $157, %rdi
    jmp isr_entrypoint

isr158:
    pushaq
    movq $0, %rsi
    movq $158, %rdi
    jmp isr_entrypoint

isr159:
    pushaq
    movq $0, %rsi
    movq $159, %rdi
    jmp isr_entrypoint

isr160:
    pushaq
    movq $0, %rsi
    movq $160, %rdi
    jmp isr_entrypoint

isr161:
    pushaq
    movq $0, %rsi
    movq $161, %rdi
    jmp isr_entrypoint

isr162:
    pushaq
    movq $0, %rsi
    movq $162, %rdi
    jmp isr_entrypoint

isr163:
    pushaq
    movq $0, %rsi
    movq $163, %rdi
    jmp isr_entrypoint

isr164:
    pushaq
    movq $0, %rsi
    movq $164, %rdi
    jmp isr_entrypoint

isr165:
    pushaq
    movq $0, %rsi
    movq $165, %rdi
    jmp isr_entrypoint

isr166:
    pushaq
    movq $0, %rsi
    movq $166, %rdi
    jmp isr_entrypoint

isr167:
    pushaq
    movq $0, %rsi
    movq $167, %rdi
    jmp isr_entrypoint

isr168:
    pushaq
    movq $0, %rsi
    movq $168, %rdi
    jmp isr_entrypoint

isr169:
    pushaq
    movq $0, %rsi
    movq $169, %rdi
    jmp isr_entrypoint

isr170:
    pushaq
    movq $0, %rsi
    movq $170, %rdi
    jmp isr_entrypoint

isr171:
    pushaq
    movq $0, %rsi
    movq $171, %rdi
    jmp isr_entrypoint

isr172:
    pushaq
    movq $0, %rsi
    movq $172, %rdi
    jmp isr_entrypoint

isr173:
    pushaq
    movq $0, %rsi
    movq $173, %rdi
    jmp isr_entrypoint

isr174:
    pushaq
    movq $0, %rsi
    movq $174, %rdi
    jmp isr_entrypoint

isr175:
    pushaq
    movq $0, %rsi
    movq $175, %rdi
    jmp isr_entrypoint

isr176:
    pushaq
    movq $0, %rsi
    movq $176, %rdi
    jmp isr_entrypoint

isr177:
    pushaq
    movq $0, %rsi
    movq $177, %rdi
    jmp isr_entrypoint

isr178:
    pushaq
    movq $0, %rsi
    movq $178, %rdi
    jmp isr_entrypoint

isr179:
    pushaq
    movq $0, %rsi
    movq $179, %rdi
    jmp isr_entrypoint

isr180:
    pushaq
    movq $0, %rsi
    movq $180, %rdi
    jmp isr_entrypoint

isr181:
    pushaq
    movq $0, %rsi
    movq $181, %rdi
    jmp isr_entrypoint

isr182:
    pushaq
    movq $0, %rsi
    movq $182, %rdi
    jmp isr_entrypoint

isr183:
    pushaq
    movq $0, %rsi
    movq $183, %rdi
    jmp isr_entrypoint

isr184:
    pushaq
    movq $0, %rsi
    movq $184, %rdi
    jmp isr_entrypoint

isr185:
    pushaq
    movq $0, %rsi
    movq $185, %rdi
    jmp isr_entrypoint

isr186:
    pushaq
    movq $0, %rsi
    movq $186, %rdi
    jmp isr_entrypoint

isr187:
    pushaq
    movq $0, %rsi
    movq $187, %rdi
    jmp isr_entrypoint

isr188:
    pushaq
    movq $0, %rsi
    movq $188, %rdi
    jmp isr_entrypoint

isr189:
    pushaq
    movq $0, %rsi
    movq $189, %rdi
    jmp isr_entrypoint

isr190:
    pushaq
    movq $0, %rsi
    movq $190, %rdi
    jmp isr_entrypoint

isr191:
    pushaq
    movq $0, %rsi
    movq $191, %rdi
    jmp isr_entrypoint

isr192:
    pushaq
    movq $0, %rsi
    movq $192, %rdi
    jmp isr_entrypoint

isr193:
    pushaq
    movq $0, %rsi
    movq $193, %rdi
    jmp isr_entrypoint

isr194:
    pushaq
    movq $0, %rsi
    movq $194, %rdi
    jmp isr_entrypoint

isr195:
    pushaq
    movq $0, %rsi
    movq $195, %rdi
    jmp isr_entrypoint

isr196:
    pushaq
    movq $0, %rsi
    movq $196, %rdi
    jmp isr_entrypoint

isr197:
    pushaq
    movq $0, %rsi
    movq $197, %rdi
    jmp isr_entrypoint

isr198:
    pushaq
    movq $0, %rsi
    movq $198, %rdi
    jmp isr_entrypoint

isr199:
    pushaq
    movq $0, %rsi
    movq $199, %rdi
    jmp isr_entrypoint

isr200:
    pushaq
    movq $0, %rsi
    movq $200, %rdi
    jmp isr_entrypoint

isr201:
    pushaq
    movq $0, %rsi
    movq $201, %rdi
    jmp isr_entrypoint

isr202:
    pushaq
    movq $0, %rsi
    movq $202, %rdi
    jmp isr_entrypoint

isr203:
    pushaq
    movq $0, %rsi
    movq $203, %rdi
    jmp isr_entrypoint

isr204:
    pushaq
    movq $0, %rsi
    movq $204, %rdi
    jmp isr_entrypoint

isr205:
    pushaq
    movq $0, %rsi
    movq $205, %rdi
    jmp isr_entrypoint

isr206:
    pushaq
    movq $0, %rsi
    movq $206, %rdi
    jmp isr_entrypoint

isr207:
    pushaq
    movq $0, %rsi
    movq $207, %rdi
    jmp isr_entrypoint

isr208:
    pushaq
    movq $0, %rsi
    movq $208, %rdi
    jmp isr_entrypoint

isr209:
    pushaq
    movq $0, %rsi
    movq $209, %rdi
    jmp isr_entrypoint

isr210:
    pushaq
    movq $0, %rsi
    movq $210, %rdi
    jmp isr_entrypoint

isr211:
    pushaq
    movq $0, %rsi
    movq $211, %rdi
    jmp isr_entrypoint

isr212:
    pushaq
    movq $0, %rsi
    movq $212, %rdi
    jmp isr_entrypoint

isr213:
    pushaq
    movq $0, %rsi
    movq $213, %rdi
    jmp isr_entrypoint

isr214:
    pushaq
    movq $0, %rsi
    movq $214, %rdi
    jmp isr_entrypoint

isr215:
    pushaq
    movq $0, %rsi
    movq $215, %rdi
    jmp isr_entrypoint

isr216:
    pushaq
    movq $0, %rsi
    movq $216, %rdi
    jmp isr_entrypoint

isr217:
    pushaq
    movq $0, %rsi
    movq $217, %rdi
    jmp isr_entrypoint

isr218:
    pushaq
    movq $0, %rsi
    movq $218, %rdi
    jmp isr_entrypoint

isr219:
    pushaq
    movq $0, %rsi
    movq $219, %rdi
    jmp isr_entrypoint

isr220:
    pushaq
    movq $0, %rsi
    movq $220, %rdi
    jmp isr_entrypoint

isr221:
    pushaq
    movq $0, %rsi
    movq $221, %rdi
    jmp isr_entrypoint

isr222:
    pushaq
    movq $0, %rsi
    movq $222, %rdi
    jmp isr_entrypoint

isr223:
    pushaq
    movq $0, %rsi
    movq $223, %rdi
    jmp isr_entrypoint

isr224:
    pushaq
    movq $0, %rsi
    movq $224, %rdi
    jmp isr_entrypoint

isr225:
    pushaq
    movq $0, %rsi
    movq $225, %rdi
    jmp isr_entrypoint

isr226:
    pushaq
    movq $0, %rsi
    movq $226, %rdi
    jmp isr_entrypoint

isr227:
    pushaq
    movq $0, %rsi
    movq $227, %rdi
    jmp isr_entrypoint

isr228:
    pushaq
    movq $0, %rsi
    movq $228, %rdi
    jmp isr_entrypoint

isr229:
    pushaq
    movq $0, %rsi
    movq $229, %rdi
    jmp isr_entrypoint

isr230:
    pushaq
    movq $0, %rsi
    movq $230, %rdi
    jmp isr_entrypoint

isr231:
    pushaq
    movq $0, %rsi
    movq $231, %rdi
    jmp isr_entrypoint

isr232:
    pushaq
    movq $0, %rsi
    movq $232, %rdi
    jmp isr_entrypoint

isr233:
    pushaq
    movq $0, %rsi
    movq $233, %rdi
    jmp isr_entrypoint

isr234:
    pushaq
    movq $0, %rsi
    movq $234, %rdi
    jmp isr_entrypoint

isr235:
    pushaq
    movq $0, %rsi
    movq $235, %rdi
    jmp isr_entrypoint

isr236:
    pushaq
    movq $0, %rsi
    movq $236, %rdi
    jmp isr_entrypoint

isr237:
    pushaq
    movq $0, %rsi
    movq $237, %rdi
    jmp isr_entrypoint

isr238:
    pushaq
    movq $0, %rsi
    movq $238, %rdi
    jmp isr_entrypoint

isr239:
    pushaq
    movq $0, %rsi
    movq $239, %rdi
    jmp isr_entrypoint

isr240:
    pushaq
    movq $0, %rsi
    movq $240, %rdi
    jmp isr_entrypoint

isr241:
    pushaq
    movq $0, %rsi
    movq $241, %rdi
    jmp isr_entrypoint

isr242:
    pushaq
    movq $0, %rsi
    movq $242, %rdi
    jmp isr_entrypoint

isr243:
    pushaq
    movq $0, %rsi
    movq $243, %rdi
    jmp isr_entrypoint

isr244:
    pushaq
    movq $0, %rsi
    movq $244, %rdi
    jmp isr_entrypoint

isr245:
    pushaq
    movq $0, %rsi
    movq $245, %rdi
    jmp isr_entrypoint

isr246:
    pushaq
    movq $0, %rsi
    movq $246, %rdi
    jmp isr_entrypoint

isr247:
    pushaq
    movq $0, %rsi
    movq $247, %rdi
    jmp isr_entrypoint

isr248:
    pushaq
    movq $0, %rsi
    movq $248, %rdi
    jmp isr_entrypoint

isr249:
    pushaq
    movq $0, %rsi
    movq $249, %rdi
    jmp isr_entrypoint

isr250:
    pushaq
    movq $0, %rsi
    movq $250, %rdi
    jmp isr_entrypoint

isr251:
    pushaq
    movq $0, %rsi
    movq $251, %rdi
    jmp isr_entrypoint

isr252:
    pushaq
    movq $0, %rsi
    movq $252, %rdi
    jmp isr_entrypoint

isr253:
    pushaq
    movq $0, %rsi
    movq $253, %rdi
    jmp isr_entrypoint

isr254:
    pushaq
    movq $0, %rsi
    movq $254, %rdi
    jmp isr_entrypoint

isr255:
    pushaq
    movq $0, %rsi
    movq $255, %rdi
    jmp isr_entrypoint

.data
.globl int_routines
int_routines:
    .quad isr0
    .quad isr1
    .quad isr2
    .quad isr3
    .quad isr4
    .quad isr5
    .quad isr6
    .quad isr7
    .quad isr8
    .quad isr9
    .quad isr10
    .quad isr11
    .quad isr12
    .quad isr13
    .quad isr14
    .quad isr15
    .quad isr16
    .quad isr17
    .quad isr18
    .quad isr19
    .quad isr20
    .quad isr21
    .quad isr22
    .quad isr23
    .quad isr24
    .quad isr25
    .quad isr26
    .quad isr27
    .quad isr28
    .quad isr29
    .quad isr30
    .quad isr31
    .quad isr32
    .quad isr33
    .quad isr34
    .quad isr35
    .quad isr36
    .quad isr37
    .quad isr38
    .quad isr39
    .quad isr40
    .quad isr41
    .quad isr42
    .quad isr43
    .quad isr44
    .quad isr45
    .quad isr46
    .quad isr47
    .quad isr48
    .quad isr49
    .quad isr50
    .quad isr51
    .quad isr52
    .quad isr53
    .quad isr54
    .quad isr55
    .quad isr56
    .quad isr57
    .quad isr58
    .quad isr59
    .quad isr60
    .quad isr61
    .quad isr62
    .quad isr63
    .quad isr64
    .quad isr65
    .quad isr66
    .quad isr67
    .quad isr68
    .quad isr69
    .quad isr70
    .quad isr71
    .quad isr72
    .quad isr73
    .quad isr74
    .quad isr75
    .quad isr76
    .quad isr77
    .quad isr78
    .quad isr79
    .quad isr80
    .quad isr81
    .quad isr82
    .quad isr83
    .quad isr84
    .quad isr85
    .quad isr86
    .quad isr87
    .quad isr88
    .quad isr89
    .quad isr90
    .quad isr91
    .quad isr92
    .quad isr93
    .quad isr94
    .quad isr95
    .quad isr96
    .quad isr97
    .quad isr98
    .quad isr99
    .quad isr100
    .quad isr101
    .quad isr102
    .quad isr103
    .quad isr104
    .quad isr105
    .quad isr106
    .quad isr107
    .quad isr108
    .quad isr109
    .quad isr110
    .quad isr111
    .quad isr112
    .quad isr113
    .quad isr114
    .quad isr115
    .quad isr116
    .quad isr117
    .quad isr118
    .quad isr119
    .quad isr120
    .quad isr121
    .quad isr122
    .quad isr123
    .quad isr124
    .quad isr125
    .quad isr126
    .quad isr127
    .quad isr128
    .quad isr129
    .quad isr130
    .quad isr131
    .quad isr132
    .quad isr133
    .quad isr134
    .quad isr135
    .quad isr136
    .quad isr137
    .quad isr138
    .quad isr139
    .quad isr140
    .quad isr141
    .quad isr142
    .quad isr143
    .quad isr144
    .quad isr145
    .quad isr146
    .quad isr147
    .quad isr148
    .quad isr149
    .quad isr150
    .quad isr151
    .quad isr152
    .quad isr153
    .quad isr154
    .quad isr155
    .quad isr156
    .quad isr157
    .quad isr158
    .quad isr159
    .quad isr160
    .quad isr161
    .quad isr162
    .quad isr163
    .quad isr164
    .quad isr165
    .quad isr166
    .quad isr167
    .quad isr168
    .quad isr169
    .quad isr170
    .quad isr171
    .quad isr172
    .quad isr173
    .quad isr174
    .quad isr175
    .quad isr176
    .quad isr177
    .quad isr178
    .quad isr179
    .quad isr180
    .quad isr181
    .quad isr182
    .quad isr183
    .quad isr184
    .quad isr185
    .quad isr186
    .quad isr187
    .quad isr188
    .quad isr189
    .quad isr190
    .quad isr191
    .quad isr192
    .quad isr193
    .quad isr194
    .quad isr195
    .quad isr196
    .quad isr197
    .quad isr198
    .quad isr199
    .quad isr200
    .quad isr201
    .quad isr202
    .quad isr203
    .quad isr204
    .quad isr205
    .quad isr206
    .quad isr207
    .quad isr208
    .quad isr209
    .quad isr210
    .quad isr211
    .quad isr212
    .quad isr213
    .quad isr214
    .quad isr215
    .quad isr216
    .quad isr217
    .quad isr218
    .quad isr219
    .quad isr220
    .quad isr221
    .quad isr222
    .quad isr223
    .quad isr224
    .quad isr225
    .quad isr226
    .quad isr227
    .quad isr228
    .quad isr229
    .quad isr230
    .quad isr231
    .quad isr232
    .quad isr233
    .quad isr234
    .quad isr235
    .quad isr236
    .quad isr237
    .quad isr238
    .quad isr239
    .quad isr240
    .quad isr241
    .quad isr242
    .quad isr243
    .quad isr244
    .quad isr245
    .quad isr246
    .quad isr247
    .quad isr248
    .quad isr249
    .quad isr250
    .quad isr251
    .quad isr252
    .quad isr253
    .quad isr254
    .quad isr255
