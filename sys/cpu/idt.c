/**
 * @file idt.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/asm.h>
#include <sys/core/gdt.h>
#include <sys/core/irq.h>
#include <sys/core/keyboard.h>
#include <sys/core/kutils.h>
#include <sys/core/proc.h>
#include <sys/core/screen.h>
#include <sys/core/syscall.h>
#include <sys/core/tty.h>
#include <sys/debug.h>
#include <sys/defs.h>

typedef struct {
    uint16_t fun_p1; // offset bits 0..15
    uint16_t selector; // a code segment selector in GDT or LDT. Just make 8
    uint8_t ist; // bits 0..2 holds Interrupt Stack Table offset, rest of bits zero.
    struct {
        uint8_t gate_type : 4;
        uint8_t zero : 1;
        uint8_t dpl : 2;
        uint8_t present : 1;
    } __attribute__((packed)) type_attr; // type and attributes
    uint16_t fun_p2; // offset bits 16..31
    uint32_t fun_p3; // offset bits 32..63
    uint32_t zero; // reserved
} __attribute__((packed)) idt_entry_t;

idt_entry_t idt[256];

void idt_load() {
    struct {
        uint16_t size : 16;
        uintptr_t base : 64;
    } __attribute__((packed)) idt_description;

    idt_description.size = 255 * sizeof(idt_entry_t);
    idt_description.base = (uintptr_t)idt;

    asm volatile("lidt %0" : : "m"(idt_description));
}

#define _32_64 (0xFFFFFFFFF << 32)
#define _16_31 (0xFFFF << 16)
#define _00_15 (0xFFFF)
#define DOT_FUNC(fptr)                                                                                                 \
    .fun_p1 = ((uintptr_t)(fptr)&_00_15), .fun_p2 = ((uintptr_t)(fptr)&_16_31) >> 16,                                  \
    .fun_p3 = ((uintptr_t)(fptr)&_32_64) >> 32,

void idt_install() {
    uint8_t* tmpidt = (uint8_t*)idt;
    for (int i = 0; i < sizeof(idt_entry_t) * 256; i++) {
        tmpidt[i] = 0;
    }
    for (int i = 0; i < 256; ++i) {
        void* isrfp = int_routines[i];

        int dpl_val = (i == 0x80) ? 3 : 0;

        idt_entry_t default_int = {
            DOT_FUNC(isrfp)
            .selector = 8,
            .ist = 0,
            .type_attr = {
                .present = 1,
                .dpl = dpl_val,
                .gate_type = 0xE,
                .zero = 0,
            },
            .zero = 0,
        };
        idt[i] = default_int;
    }
}

// FOR Master PIC => outb(0x20,0x20)
// FOR Slave PIC  => outb(0x20,0x20); outb(0xa0,0x20);
#define GPF_TBL_GDT 0b00
#define GPF_TBL_IDT 0b01
#define GPF_TBL_LDT 0b10
#define GPF_TBL_IDT2 0b11

void isr_selector(uint8_t intrno, uint32_t errcode) {
    /* DONT TOUCH ME */
    interrupt_stack_t* regs;
    asm volatile("movq %%rsp, %0\n\t" : "=X"(regs));
    regs = BYBYTE(regs, +32);

    /* DONT TOUCH ME */

    task_t* task = getcurrenttask();
    (void)task;

    switch (intrno) {
    case 13: {

        bool external = errcode & (0b1);
        uint16_t index = errcode & (0xFFF8);
        uint8_t table = errcode & (0b110);

        /* clang-format off */
        char* tablename = table == GPF_TBL_GDT ? "GDT"
            : table == GPF_TBL_IDT ? "IDT"
            : table == GPF_TBL_LDT ? "LDT"
            : table == GPF_TBL_IDT2 ? "IDT"
            : "Unknown";
        /* clang-format on */

        kerror("General Protection Fault: external=%b in %s[%d]", external, tablename, index);

        panic();
        break;
    }
    case 14: {
        void* fault_addr = (void*)getcr2();
        kwarn("PGFLT: %p", fault_addr);
        page_fault_handler(fault_addr, errcode);
        // todo find the vma that has the faulting addr
        // get permissions for page from the vma
        // map_va
        // set va users
        break;
    }
    case 32: { // timer
        update_clock();
        break;
    }
    case 33: { // keyboard
        char keypress = convert_scancode(read_scan_code());
        if (keypress != 0) {
            input(keypress);
        }
        break;
    }
    case 0x80: { // syscall
        // kprintf("-------syscall--------\n");

        // /* First 3 args */

        // kprintf("1st = %d\n", regs->rdi);
        // kprintf("2nd = %d\n", regs->rsi);
        // kprintf("3rd = %d\n", regs->rdx);

        getsyscall(regs->rax)(regs);
        // set_tss_rsp(task->tss_rsp);
        break;
    }
    default: {
        kerror("Unsupported Interrupt: %d", intrno);
        // TODO
        panic();
    }
    }
    if (intrno >= 32 && intrno <= 48)
        pic_intr_ack(intrno - 32);
}
