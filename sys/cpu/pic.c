/**
 * @file pic.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/asm.h>
#include <sys/core/irq.h>
#include <sys/core/kutils.h>

#define PIC1_COMD 0x20
#define PIC2_COMD 0xA0
#define PIC1_DATA ((PIC1_COMD) + 1)
#define PIC2_DATA ((PIC2_COMD) + 1)

#define PIT_CTR 0x43
#define PIT_CH0 0x40
#define PIT_CH2 0x42

#define TIMER_FREQ 1193182

void pic_init() {
    unsigned char pic_mask1, pic_mask2;
    pic_mask1 = rport8(PIC1_DATA);
    pic_mask2 = rport8(PIC2_DATA);
    // restart pic
    wport8(PIC1_COMD, 0x11);
    wport8(PIC2_COMD, 0x11);
    // set offset
    wport8(PIC1_DATA, 0x20);
    wport8(PIC2_DATA, 0x28);
    // cascade mode
    wport8(PIC1_DATA, 0x04);
    wport8(PIC2_DATA, 0x02);
    // 8086/88 mode
    wport8(PIC1_DATA, 0x01);
    wport8(PIC2_DATA, 0x01);
    // restore PIC mask
    wport8(PIC1_DATA, pic_mask1);
    wport8(PIC2_DATA, pic_mask2);

    // CLEAR INTs
    for (int i = 0; i < 16; ++i) {
        pic_ignore_irq(i, true); // Ignore all irqs, initialize..
    }
}

void pic_ignore_irq(uint8_t irq_line, bool set) {
    uint16_t port = 0;
    uint8_t value = 0;
    if (irq_line < 8) {
        port = PIC1_DATA;
    } else {
        port = PIC2_DATA;
        irq_line -= 8;
    }
    value = rport8(port);
    value = set ? value | (1 << irq_line) : value & ~(1 << irq_line);
    wport8(port, value);
}

void pic_intr_ack(uint8_t irq_line) {
    if (irq_line > 7)
        wport8(PIC2_COMD, 0x20);
    wport8(PIC1_COMD, 0x20);
}

void pit_init() {
    int32_t desired_freq = 100, /* 10 times a second */
        interrupt_freq = ((TIMER_FREQ + desired_freq) / 2) / desired_freq;
    // 00: counter, 11: r&w, 011: square wave, 0: 16-bit counter
    wport8(PIT_CTR, 0b00110110);
    wport8(PIT_CH0, interrupt_freq % 256);
    wport8(PIT_CH0, interrupt_freq / 256);
}
