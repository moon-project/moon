/**
 * @file init.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/asm.h>
#include <sys/core/init.h>
#include <sys/core/irq.h>

void init_cpu(const init_args_t const* args) {
    idt_load();
    idt_install();
    enable_interrupts();
    pit_init();
    // disable timer
    pic_ignore_irq(0, true);
    // enable keyboard
    pic_ignore_irq(1, false);
}
