/**
 * @file kfs.c
 * @author Neal Beeken, Mrunal Patel
 * @brief File system helpers
 */

#include <sys/core/kutils.h>

char* k_basename(char* fullpath) {
    char* end = endofstr(fullpath);
    // If its a dir that ends in slash we still want the dir name
    if (*end == '/') {
        end--;
    }
    // find the start of the path delimiter
    while (*end != '/' && end != fullpath) {
        end--;
    }
    // give it back
    end += 1;
    return end;
}
