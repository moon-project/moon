/**
 * @file kutils.c
 * @author Neal Beeken, Mrunal Patel
 * @brief Generic helpers
 * The goal of utils is a cache of trivial operations
 * that can be used anywhere in the operating system
 */

#include <sys/core/kutils.h>
#include <sys/core/screen.h>

int64_t k_pow(int64_t base, int64_t exponent) {
    if (base == 0) {
        return 1;
    }
    int64_t sum = 1;
    while (exponent != 0) {
        sum *= base;
        exponent -= 1;
    }
    return sum;
}

void k_bzero(void* space, size_t n) {
    panicif(space == NULL);
    for (int i = 0; i < n; i++) {
        ((char*)space)[i] = 0;
    }
}

void k_memset(void* space, uint8_t value, size_t n) {
    panicif(space == NULL);
    for (int i = 0; i < n; i++) {
        ((char*)space)[i] = value;
    }
}

void k_memcpy(void* dst, void* src, size_t n) {
    panicif(dst == NULL);
    panicif(src == NULL);
    for (int i = 0; i < n; i++) {
        ((char*)dst)[i] = ((char*)src)[i];
    }
}

bool k_memcmp(void* dst, void* src, size_t n) {
    panicif(dst == NULL);
    panicif(src == NULL);
    for (int i = 0; i < n; i++) {
        if (((char*)dst)[i] != ((char*)src)[i]) {
            return false;
        }
    }
    return true;
}
