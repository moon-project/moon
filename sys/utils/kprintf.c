/**
 * @file kprintf.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/kutils.h>
#include <sys/core/screen.h>
#include <sys/stdarg.h>

void kprintf(const char* fmt, ...) {
    va_list args;
    va_start(args, fmt);
    int num_specifiers = 0;
    int len = k_strlen(fmt);
    char buf[2];
    char* string;
    void* ptr;

    for (int i = 0; i < len; ++i) {
        if (fmt[i] == '\e') {
            esc((char*)(fmt + i), current_terminal);
            i += LUNABYTSEQLEN;
            continue;
        }
        if (fmt[i] == '%') {
            ++i; // Move i fwd to the specifier
            // Handle fmt
            switch (fmt[i]) {
            case 'c': { // %c - character
                buf[0] = va_arg(args, int);
                buf[1] = '\0';
                puts(buf);
                break;
            }
            case 'b': {
                string = va_arg(args, bool) ? "true" : "false";
                puts(string);
                break;
            }
            case 's': { // %s - string
                string = va_arg(args, char*);
                puts(string);
                break;
            }
            case 'd': { // %d - pass in to knumstr up to 64-bit int
                k_numtostr(va_arg(args, int32_t), 10, true);
                break;
            }
            case 'x': { // %x - pass in to knumstr up to 64-bit int in hex
                k_numtostr(va_arg(args, uint64_t), 16, false);
                break;
            }
            case 'o': { // %x - pass in to knumstr up to 64-bit int in hex
                k_numtostr(va_arg(args, uint64_t), 8, false);
                break;
            }
            case 'p': { // %p - 64-bit pointer proceeded by 0x or (nil) if 0x0
                ptr = va_arg(args, void*);
                if (ptr == 0) {
                    puts("(nil)");
                } else {
                    putc('0');
                    putc('x');
                    k_numtostr((uintptr_t)ptr, 16, false);
                }
                break;
            }
            default: { // Unsupported modifier
                putc(fmt[i]);
                break;
            }
            }
            ++num_specifiers;
        } else {
            putc(fmt[i]);
        }
    }
    va_end(args);
    return;
}

void ktitle(char* title) {
    size_t len = k_strlen(title) + 2;
    size_t eq_len = (79 - len) / 2;
    kprintf(KBWN);
    for (int i = 0; i < eq_len; i++) {
        kprintf("=");
    }
    kprintf(KNRM);
    kprintf(" %s ", title);
    kprintf(KBWN);
    for (int i = 0; i < eq_len; i++) {
        kprintf("=");
    }
    kprintf(KNRM);
    kprintf("\n");
}
