/**
 *
 */

#include <sys/core/kutils.h>

bool k_isalpha(char c) {
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

bool k_isdigit(char c) {
    return (c >= '0' && c <= '9');
}

bool k_isxdigit(char c) {
    return (c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
}

bool k_ispunct(char c) {
    return (c >= '!' && c <= '/') || (c >= ':' && c <= '@') || (c >= '[' && c <= '`') || (c >= '{' && c <= '~');
}

char k_toupper(char c) {
    if (c >= 'a' && c <= 'z') {
        return c - 32;
    }
    return c;
}

char k_tolower(char c) {
    if (c >= 'a' && c <= 'z') {
        return c - 32;
    }
    return c;
}

size_t k_strlen(const char* str) {
    panicif(str == NULL);
    char* ptr = (char*)str;
    while (*ptr) {
        ptr++;
    }
    return ptr - str;
}

void k_strcpy(char* dst, char* src) {
    panicif(src == NULL);
    panicif(dst == NULL);
    size_t len = k_strlen(src);
    k_memcpy(dst, src, len);
    dst[len] = '\0';
}

bool k_strcmp(char* a, char* b) {
    size_t len_a = k_strlen(a);
    size_t len_b = k_strlen(b);
    if (len_a != len_b) {
        return false;
    }
    return k_memcmp(a, b, len_a);
}

size_t k_strccount(char* str, char c) {
    size_t len = k_strlen(str);
    size_t count = 0;
    for (int i = 0; i < len; i++) {
        if (str[i] == c) {
            count++;
        }
    }
    return count;
}

char* k_strstr(char* searchspace, char* key) {
    size_t key_len = k_strlen(key);
    for (char* ptr = searchspace; *ptr != '\0'; ptr++) {
        if (k_memcmp(ptr, key, key_len)) {
            return ptr;
        }
    }
    return NULL;
}

void k_strcat(char* buffer, char* str) {
    char* end = endofstr(buffer);
    end++;
    k_memcpy(end, str, k_strlen(str));
}

uint8_t digit_to_val(char digit) {
    if (k_isxdigit(digit) && k_isalpha(digit)) {
        return (k_toupper(digit) - 'A') + 10;
    } else if (k_isdigit(digit)) {
        return digit - '0';
    } else {
        // Hopefully this will crash your bad code
        return -1;
    }
}

bool k_strtonum(char* start, uint64_t* num, int base) {
    *num = 0;
    if (base > 16) {
        return false;
    }
    size_t len = k_strlen(start);
    char* end = start + (len - 1);
    for (char* ptr = end; ptr != start; ptr--) {
        if (!k_isxdigit(*ptr)) {
            return false;
        }
        int8_t val = digit_to_val(*ptr);
        int64_t power = end - ptr;
        int64_t placeval = val * k_pow(base, power);
        // kdebug("val %d, placeval %d, power %d", val, placeval, power);
        *num += placeval;
    }
    return true;
}

#define MSB (20 - ndigits)
#define NUM_STR (number_str + MSB)

void k_numtostr(uint64_t num, uint32_t base, bool sign) {
    char number_str[25];
    k_bzero(number_str, 25);
    int ndigits = 0;

    if (num == 0) {
        puts("0");
        return;
    }

    if (sign && num & (1UL << 63)) {
        putc('-');
        num = ~num;
        num += 1;
    }

    while (num) {
        ndigits++;
        uint8_t rem = num % base;
        number_str[MSB] = (rem < 10) ? rem + 0x30 : rem + 0x57;
        num /= base;
    }

    puts(NUM_STR);
}

char* k_strsep(char** stringp, const char* delim) {
    panicif(stringp == NULL);
    panicif(delim == NULL);

    char* string = *stringp;
    char* delims = (char*)delim;
    char* token;
    char cur_char, cur_delim;
    int found = 0;
    if (string == NULL) {
        return NULL;
    }
    token = string;
    while (!found) {
        cur_char = *string;
        string += 1; // Next char in string
        cur_delim = *delims;
        while (cur_delim != '\0') {
            if (cur_char == '\0') {
                string = NULL;
                found = 1;
                break;
            } else if (cur_char == cur_delim) {
                string[-1] = '\0';
                found = 1;
                break; // We found a chunk
            }
            delims += 1; // Next deliminator char
            cur_delim = *delims;
        }
        delims = (char*)delim;
        *stringp = string;
    }
    return token;
}
