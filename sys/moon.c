/**
 * @file moon.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/gdt.h>
#include <sys/core/init.h>
#include <sys/core/kutils.h>
#include <sys/core/screen.h>
#include <sys/debug.h>
#include <sys/defs.h>

#define INITIAL_STACK_SIZE 4096
uint8_t initial_stack[INITIAL_STACK_SIZE] __attribute__((aligned(16)));
uint32_t* loader_stack;
extern char kernmem, physbase;

size_t init_func_len = FOREACH_INIT(GENERATE_COUNT);
init_f init_funcs[] = { FOREACH_INIT(GENERATE_FUNCS) };
char* init_strs[] = { FOREACH_INIT(GENERATE_STRS) };

void start(uint32_t* modulep, void* physbase, void* physfree) {

    moon_os();

    init_args_t init_args = {.initial_stack = initial_stack,
        .loader_stack = loader_stack,
        .physbase = physbase,
        .physfree = physfree,
        .modulep = modulep,
        .virtbase = &kernmem };

    for (int i = 0; i < init_func_len; ++i) {
        ktitle(init_strs[i]);
        init_funcs[i](&init_args);
    }

    while (1)
        ;
}

void boot(void) {
    // note: function changes rsp, local stack variables can't be practically used
    register char* screen;

    for (screen = (char*)0xb8001; screen < (char*)0xb8000 + 160 * 25; screen += 2) {
        *screen = 7 /* white */;
    }

    __asm__("cli;"
            "movq %%rsp, %0;"
            "movq %1, %%rsp;"
            : "=g"(loader_stack)
            : "r"(&initial_stack[INITIAL_STACK_SIZE]));

    init_gdt();

    start((uint32_t*)((char*)(uint64_t)loader_stack[3] + (uint64_t)&kernmem - (uint64_t)&physbase),
        (uint64_t*)&physbase, (uint64_t*)(uint64_t)loader_stack[4]);

    panics("Boot Ending...");
}
