/**
 * @file init.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/asm.h>
#include <sys/core/init.h>
#include <sys/core/kutils.h>
#include <sys/core/tty.h>

void init_io(const init_args_t const* args) {
    init_tty();
}
