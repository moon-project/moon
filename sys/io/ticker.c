/**
 * @file ticker.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/screen.h>

static clock_t clock = { 0 };
static uint64_t clk_calls = 0;

void update_clock() {
    if (clk_calls % 200 == 0) {
        clock.sec++;
        if (clock.sec == 60) {
            clock.min++;
            clock.sec = 0;
            if (clock.min == 60) {
                clock.hrs++;
                clock.min = 0;
            }
        }
        clk_calls = 0;
    }
    clk_calls++;
    print_clock();
}

void moon_os() {
#define LGC COLOR(VGA_KBRIGHTCYAN, VGA_KBLACK)
    (*real_screen)[24][0] = CHAR2ANSI('m', LGC);
    (*real_screen)[24][1] = CHAR2ANSI('o', LGC);
    (*real_screen)[24][2] = CHAR2ANSI('o', LGC);
    (*real_screen)[24][3] = CHAR2ANSI('n', LGC);
#undef LGC
}

void print_clock() {
#define CLKCLR COLOR(VGA_KYELLOW, VGA_KBLACK)
    (*real_screen)[24][72] = CHAR2ANSI((clock.hrs / 10) + '0', CLKCLR); // 10^1 hrs digit
    (*real_screen)[24][73] = CHAR2ANSI((clock.hrs % 10) + '0', CLKCLR); // 10^0 hrs digit
    (*real_screen)[24][74] = CHAR2ANSI(':', CLKCLR);

    (*real_screen)[24][75] = CHAR2ANSI((clock.min / 10) + '0', CLKCLR); // 10^1 min digit
    (*real_screen)[24][76] = CHAR2ANSI((clock.min % 10) + '0', CLKCLR); // 10^0 min digit
    (*real_screen)[24][77] = CHAR2ANSI(':', CLKCLR);

    (*real_screen)[24][78] = CHAR2ANSI((clock.sec / 10) + '0', CLKCLR); // 10^1 sec digit
    (*real_screen)[24][79] = CHAR2ANSI((clock.sec % 10) + '0', CLKCLR); // 10^0 sec digit
}

void panic_notification() {
#define PANIKCLR COLOR(VGA_KBRIGHTRED, VGA_KBLACK)
    for (int i = 4; i < 71; i++) {
        (*real_screen)[24][i] = CHAR2ANSI('!', PANIKCLR);
    }
    (*real_screen)[24][0] = CHAR2ANSI('S', PANIKCLR);
    (*real_screen)[24][1] = CHAR2ANSI('U', PANIKCLR);
    (*real_screen)[24][2] = CHAR2ANSI('N', PANIKCLR);
    (*real_screen)[24][3] = CHAR2ANSI(' ', PANIKCLR);
#undef PANIKCLR
}
