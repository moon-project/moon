/**
 * @file tty.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/kutils.h>
#include <sys/core/proc.h>
#include <sys/core/screen.h>
#include <sys/core/tty.h>

#define STDIN_FILENO 0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2

#define BUFFER_SIZE (0x100 * 2)

superblock_t* stdbuff_sb = NULL;

void stdout_errflush();
void stdinflush();
vfs_interface_t stdbuff_interface();

stdbuff_t stdin;
stdbuff_t stdout;
stdbuff_t stderr;

vfs_inode_t* stdin_inode = NULL;
vfs_inode_t* stdout_inode = NULL;
vfs_inode_t* stderr_inode = NULL;

pid_t fg_pid = 0;

void init_tty() {
    stdbuff_sb = kalloc(sizeof(superblock_t));

    stdin_inode = kalloc(sizeof(vfs_inode_t));
    stdout_inode = kalloc(sizeof(vfs_inode_t));
    stderr_inode = kalloc(sizeof(vfs_inode_t));

    stdin_inode->superblock = stdbuff_sb;
    stdout_inode->superblock = stdbuff_sb;
    stderr_inode->superblock = stdbuff_sb;

    k_bzero(&stdin, sizeof(stdbuff_t));
    k_bzero(&stdout, sizeof(stdbuff_t));
    k_bzero(&stderr, sizeof(stdbuff_t));

    stdin.buffer = kalloc(BUFFER_SIZE);
    stdout.buffer = kalloc(BUFFER_SIZE);
    stderr.buffer = kalloc(BUFFER_SIZE);

    stdin.flush = stdinflush;
    stdout.flush = stdout_errflush;
    stderr.flush = stdout_errflush;

    stdbuff_sb->type = STDBUFFFS;
    stdbuff_sb->interface = stdbuff_interface();
}

int std_close(int fd) {
    // Why do you want to close stdbufs...
    // there's no valgrind here
    return 0;
}

int std_read(int fd, void* buf, size_t bufsz) {

    int amount_read = MIN(k_strlen(stdin.buffer), bufsz);

    switch (fd) {
    case STDIN_FILENO: {
        k_memcpy(buf, stdin.buffer, amount_read); // Copy to userspace
        k_memcpy(stdin.buffer, &stdin.buffer[amount_read], amount_read); // shift down
        break;
    }
    case STDOUT_FILENO: {
        kerror("You can't read from stdout!");
        amount_read = -1;
        break;
    }
    case STDERR_FILENO: {
        kerror("You can't read from stderr!");
        amount_read = -1;
        break;
    }
    default:
        kerror("Don't ask me that fd=%d", fd);
        amount_read = -1;
        break;
    }
    return amount_read;
}

int std_write(int fd, void* buf, size_t bufsz) {
    switch (fd) {
    case STDIN_FILENO: {
        kerror("No writing to stdin!");
        break;
    }
    case STDOUT_FILENO: {
        kprintf("%s", buf);
        break;
    }
    case STDERR_FILENO: {
        kprintf(KRED "%s" KNRM, buf);
        break;
    }
    default:
        kerror("Don't ask me that fd=%d", fd);
        break;
    }
    return bufsz;
}

vfs_interface_t stdbuff_interface() {
    vfs_interface_t interface;
    k_bzero(&interface, sizeof(vfs_interface_t));
    interface.write_f = std_write;
    interface.close_f = std_close;
    interface.read_f = std_read;
    return interface;
}

void stdinflush() {
    task_t* fgp = gettask(fg_pid);
    fgp->state = RUNNABLE; // Should have been waiting
}

void stdout_errflush() {}

void insert_into_buffer(stdbuff_t std, char c) {

    size_t sz = k_strlen(std.buffer);

    std.buffer[sz] = c;
    std.buffer[sz + 1] = 0;

    std.tail = sz + 1;

    if (std.tail == BUFFER_SIZE / 2 || c == '\n') {
        std.flush();
    }
}

void input(char c) {
    char buf[2];
    buf[0] = c;
    buf[1] = '\0';
    puts(buf); // echo
    insert_into_buffer(stdin, c); // where a program will read from
}

void output(char c, bool is_error) {
    if (is_error) {
        insert_into_buffer(stderr, c);
    } else {
        insert_into_buffer(stdout, c);
    }
}
