/**
 * @file keyboard.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/asm.h>
#include <sys/core/keyboard.h>
#include <sys/core/kutils.h>
#include <sys/core/tty.h>

bool shift = false;
bool ctrl = false;

/* clang-format off */
char scancodes_make[] = { FOREACH_SCANCODE(YIELD_SC_TO_ASCII_MAKE) };

char scancodes_break[] = { FOREACH_SCANCODE(YIELD_SC_TO_ASCII_BREAK) };

char nums_to_symbols[] = {
    ['1'] '!', ['2'] '@', ['3'] '#', ['4'] '$',
    ['5'] '%', ['6'] '^', ['7'] '&', ['8'] '*',
    ['9'] '(', ['0'] ')',
};
char lowersymbols_to_uppersymbols[] = {
    [','] '<', ['.'] '>', ['/'] '?', [';'] ':',
    ['\''] '"', ['['] '{', [']'] '}', ['\\'] '|',
    ['-'] '_', ['='] '+', ['`'] '~',
};
/* clang-format on */

uint8_t read_scan_code() {
    uint8_t sc = 0;
    do {
        if (rport8(PS2DATA) != sc) {
            sc = rport8(PS2DATA);
            if (sc > 0) {
                return sc;
            }
        }
    } while (true);
}

/**
 * @brief      Returns character pressed or 0 if control key
 *
 * @return     Ascii char of the key pressed or zero for control key
 */
char convert_scancode(uint8_t sc) {
    char ascii_char;
    switch (sc) {
    case BKSPC_MK:
        backspace();
        stdin.buffer[k_strlen(stdin.buffer) - 1] = 0;
        return 0;
    case LSHIFT_MK:
        shift = true;
        return 0;
    case LSHIFT_BK:
        shift = false;
        return 0;
    case LCTRL_MK:
        ctrl = true;
        return 0;
    case LCTRL_BK:
        ctrl = false;
        return 0;
    default:
        ascii_char = scancodes_make[sc];
        if (shift) {
            if (k_ispunct(ascii_char)) {
                ascii_char = lowersymbols_to_uppersymbols[(int)ascii_char];
            }
            if (k_isdigit(ascii_char)) {
                ascii_char = nums_to_symbols[(int)ascii_char];
            }
            if (k_isalpha(ascii_char)) {
                ascii_char = k_toupper(ascii_char);
            }
        }
    }
    return ascii_char;
}
