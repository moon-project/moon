/**
 * @file screen.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/asm.h>
#include <sys/core/keyboard.h>
#include <sys/core/kutils.h>
#include <sys/core/screen.h>

static terminal_t terminals[TERMS_COUNT] = { [USER_TERM] { .name = "tty/user", .color = WONB, .cursor = CURSOR(0, 0) },
    [KERN_TERM] { .name = "tty/kern", .color = WONB, .cursor = CURSOR(0, 0) } };

terminal_t* current_terminal = &terminals[USER_TERM];
lunabytes_t (*real_screen)[SCREEN_ROW][SCREEN_COL] = SCREEN_BASE;

void clearline(lunabytes_t* row) {
    for (int i = 0; i < SCREEN_COL; ++i) {
        row[i] = CHAR2ANSI(' ', WONB);
    }
}

// BASE FUNCTION RANDOM ACCESS PLACEMENT - calls display to copy to screen
void put_at_on(lunabytes_t bytes, cursor_t* location, terminal_t* terminal) {
    if (' ' <= bytes.glyph && '~' >= bytes.glyph) { // Visible chars only!
        terminal->screen[location->row][location->col] = bytes;
    }
    update_cursor(terminal, location, bytes.glyph);
    display(terminal);
}

void put_on(lunabytes_t bytes, terminal_t* terminal) {
    put_at_on(bytes, &(terminal->cursor), terminal);
}

void put(lunabytes_t bytes) {
    put_on(bytes, current_terminal);
}

void putc(char glyph) {
    put_on(CHAR2ANSI(glyph, current_terminal->color), current_terminal);
}

void puts(char* glyph_str) {
    for (char* ptr = glyph_str; *ptr; ++ptr) {
        if (*ptr == '\e') {
            esc(ptr, current_terminal);
            ptr += LUNABYTSEQLEN + 1;
        }
        putc(*ptr);
    }
}

void esc(char* string, terminal_t* terminal) {
    if (string[TYPE_OFF] == 's') {
        int fg = string[FG_OFF] - '0';
        int bg = string[BG_OFF] - '0';

        if (string[S_OFF] == '1') { // 0b01
            fg += BOLDIFY;
        }
        if (string[S_OFF] == '2') { // 0b10
            bg += BOLDIFY;
        }
        if (string[S_OFF] == '3') { // 0b11
            bg += BOLDIFY;
            fg += BOLDIFY;
        }

        terminal->color = COLOR(fg, bg);
    } else if (string[TYPE_OFF] == 'x') {
        int x = (string[XTENS_OFF] - '0') * 10 + (string[XONES_OFF] - '0');
        int y = (string[YTENS_OFF] - '0') * 10 + (string[YONES_OFF] - '0');
        terminal->cursor = CURSOR(x, y);
    }
}

void display(terminal_t* terminal) {
    for (int row = 0; row < SCREEN_ROW; ++row)
        for (int col = 0; col < SCREEN_COL; ++col) {
            (*real_screen)[row][col] = terminal->screen[row][col];
        }
}

void scroll(terminal_t* terminal) {
    for (int row = 1; row < SCREEN_ROW; ++row) {
        for (int col = 0; col < SCREEN_COL; ++col) {
            terminal->screen[row - 1][col] = terminal->screen[row][col];
        }
    }
    clearline(terminal->screen[23]);
}

void vga_cursor(cursor_t* location) {
    uint16_t position = (location->row * 80) + location->col;
    // cursor LOW port to vga INDEX register
    wport8(0x3D4, 0x0F);
    wport8(0x3D5, (uint8_t)(position & 0xFF));
    // cursor HIGH port to vga INDEX register
    wport8(0x3D4, 0x0E);
    wport8(0x3D5, (uint8_t)((position >> 8) & 0xFF));
}

void update_cursor(terminal_t* terminal, cursor_t* location, char c) {
    location->col++;
    if (c == '\n' || location->col == SCREEN_COL) { // Newline or Word Wrap
        location->row++;
        location->col = 0;
    }
    if (c == '\r') {
        location->col = 0;
    }
    if (c == '\t') {
        location->col += 4;
    }
    if (location->row == SCREEN_ROW) { // Max Row time to scroll
        scroll(terminal);
        location->row--;
    }
    vga_cursor(location);
}

void echokey(char c) {
    (*real_screen)[24][69] = CHAR2ANSI(c, WONB);
    (*real_screen)[24][68] = ctrl ? CHAR2ANSI('^', WONB) : CHAR2ANSI(' ', WONB);
}

void backspace() {
    current_terminal->cursor.col--;
    put_on(CHAR2ANSI(' ', WONB), current_terminal);
    current_terminal->cursor.col--;
    vga_cursor(&(current_terminal->cursor));
}

void __attribute__((noreturn)) panic() {
    current_terminal->cursor.row = 12;
    current_terminal->cursor.col = 29;
    panic_notification();
    asm("hlt");
    __builtin_unreachable();
}

void __attribute__((noreturn)) panics(char* msg) {
    puts(msg);
    panic();
}
