/**
 * @file init.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/init.h>
#include <sys/core/pcie.h>

void init_dev(const init_args_t const* args) {
    pcie_scanbus();
}
