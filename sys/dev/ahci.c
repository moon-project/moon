/**
 * @file ahci.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/ahci.h>
#include <sys/core/kutils.h>

static hba_mem_t* abar = NULL;

uint32_t probe_port(hba_mem_t* abar) {
    uint32_t pi = abar->pi, valid = 0;
    kinfo("pi = %x", pi); // 3f 001111111
    for (int i = 0; i < 32; i++) {
        if (pi & 1) {
            uint32_t ssts = abar->ports[i].ssts;
            uint8_t ipm = (ssts >> 8) & 0x0F;
            uint8_t det = ssts & 0x0F; // DET: Device detection
            if (det != HBA_PORT_DET_PRESENT) // Check drive status
                continue;
            if (ipm != HBA_PORT_IPM_ACTIVE)
                continue;
            switch (abar->ports[i].sig) {
            case AHCI_DEV_SATAPI:
                kinfo("ATAPI DEV");
                break;
            case AHCI_DEV_SEMB:
                kinfo("SEMB DEV");
                break;
            case AHCI_DEV_PM:
                kinfo("PM DEV");
                break;
            case AHCI_DEV_SATA:
                kinfo("SSTS: 0x%x\tSATA DEV %d", ssts, i);
                valid |= 1 << i;
                break;
            default:
                kerror("INVALID DEV");
                break;
            }
        }
        pi >>= 1;
    }
    return valid;
}

void ahci_start_cmd(hba_port_t* port) {
    kinfo("Starting AHCI Commands");
    // Wait for cr bit to be cleared (15)
    while (port->cmd & HBA_PxCMD_CR)
        ;
    port->cmd |= HBA_PxCMD_FRE; // (4)
    port->cmd |= HBA_PxCMD_ST; // (0)
    kinfo("Started AHCI Commands");
}

void ahci_stop_cmd(hba_port_t* port) {
    kwarn("Stopping AHCI Commands");
    // Clear st (0)
    port->cmd &= ~HBA_PxCMD_ST;
    // wait for fr (14) and cr (15) are cleared
    while (true) {
        if (port->cmd & HBA_PxCMD_FR)
            continue;
        if (port->cmd & HBA_PxCMD_CR)
            continue;
        break;
    }
    // clear fre (4)
    port->cmd &= ~HBA_PxCMD_FRE;
    ksuccess("Stopped AHCI Commands");
}

void ahci_port_rebase(hba_port_t* port, int portnum) {

    // ahci_stop_cmd(port);

    // clb: command list base
    kinfo("Old CLB: %x", port->clb);
    kinfo("Old FB: %x", port->fb);

    return;

    port->clb = AHCI_BASE + (portnum << 10);

    k_bzero((void*)(port->clb), 1024);

    kinfo("New CLB: %x", port->clb);

    // fb: fis base
    // FIS offset: 32K + 256 * portnum
    // FIS entry size = 256 bytes per port

    port->fb = AHCI_BASE + (32 << 10) + (portnum << 8);

    k_bzero((void*)(port->fb), 256);

    kinfo("New FB: %x", port->fb);

    // Command table offset: 40K + 8K*portnum
    // Command table size = 256*32 = 8K per port
    hba_cmd_header_t* cmdheader = (hba_cmd_header_t*)(port->clb);

    for (int i = 0; i < 32; i++) {
        cmdheader[i].prdtl = 8; // 8 prdt entries per command table
        // 256 bytes per command table, 64+16+48+16*8
        // Command table offset: 40K + 8K*portnum + cmdheader_index*256
        cmdheader[i].ctba = AHCI_BASE + (40 << 10) + (portnum << 13) + (i << 8);
        k_bzero((void*)cmdheader[i].ctba, 256);
    }

    // ahci_start_cmd(port);
}

// Find a free command list slot
int ahci_find_cmdslot(hba_port_t* port) {
    // If not set in SACT and CI, the slot is free
    uint32_t slots = (port->sact | port->ci);
    for (int i = 0; i < 32; i++) {
        if ((slots & 1) == 0)
            return i;
        slots >>= 1;
    }
    kerror("Cannot find free command list entry\n");
    return -1;
}

bool busy_wait_on_port(hba_port_t* port, int slot) {
    int spin = 0; // Spin lock timeout counter
    // The below loop waits until the port is no longer busy before issuing a new command
    while ((port->tfd & (ATA_STATUS_BSY | ATA_STATUS_DRQ)) && spin < 1000000) {
        spin++;
    }
    if (spin == 1000000) {
        kerror("Port is hung");
        return false;
    }

    port->ci = 1 << slot; // Issue command

    // Wait for completion
    while (true) {
        // In some longer duration reads, it may be helpful to spin on the DPS bit
        // in the PxIS port field as well (1 << 5)
        if ((port->ci & (1 << slot)) == 0)
            break;
        if (port->is_rwc & HBA_PxIS_TFES) { // Task file error
            kerror("Read disk error 1");
            return false;
        }
    }

    // Check again
    if (port->is_rwc & HBA_PxIS_TFES) {
        kerror("Read disk error 2");
        return false;
    }

    return true;
}

bool ahci_write(hba_port_t* port, uint64_t sector, uint32_t count, uint8_t* buf) {

    port->is_rwc = 0xFFFFFFFF; // Clear pending interrupt bits

    int slot = ahci_find_cmdslot(port);

    if (slot == -1) {
        return false;
    }

    hba_cmd_header_t* cmdheader = (hba_cmd_header_t*)port->clb;
    cmdheader += slot;
    cmdheader->cfl = sizeof(fis_reg_h2d_t) / sizeof(uint32_t); // Command FIS size
    cmdheader->w = 1; // write to device
    cmdheader->prdtl = (uint16_t)((count - 1) >> 4) + 1; // PRDT entries count

    hba_cmd_tbl_t* cmdtbl = (hba_cmd_tbl_t*)(cmdheader->ctba);
    k_bzero(cmdtbl, sizeof(hba_cmd_tbl_t) + (cmdheader->prdtl - 1) * sizeof(hba_prdt_entry_t));

    // Only entry
    cmdtbl->prdt_entry[0].dba = (uint64_t)buf;
    cmdtbl->prdt_entry[0].dbc = count << 9; // 512 bytes per sector
    cmdtbl->prdt_entry[0].i = 1;

    // Setup command
    fis_reg_h2d_t* cmdfis = (fis_reg_h2d_t*)(&cmdtbl->cfis);

    cmdfis->fis_type = FIS_TYPE_REG_H2D;
    cmdfis->c = 1; // Command
    cmdfis->command = ATA_CMD_WRITE_DMA_EX;

    SECTOR_TO_LBA(cmdfis, sector);

    cmdfis->device = CMD_FIS_DEV_LBA; // LBA mode

    cmdfis->count = count;

    return busy_wait_on_port(port, slot);
}

bool ahci_read(hba_port_t* port, uint64_t sector, uint32_t count, uint8_t* buf) {

    port->is_rwc = 0xFFFFFFFF; // Clear pending interrupt bits

    int slot = ahci_find_cmdslot(port);

    if (slot == -1) {
        return false;
    }

    hba_cmd_header_t* cmdheader = (hba_cmd_header_t*)port->clb;
    cmdheader += slot;
    cmdheader->cfl = sizeof(fis_reg_h2d_t) / sizeof(uint32_t); // Command FIS size
    cmdheader->w = 0; // Read from device
    cmdheader->prdtl = (uint16_t)((count - 1) >> 4) + 1; // PRDT entries count

    hba_cmd_tbl_t* cmdtbl = (hba_cmd_tbl_t*)(cmdheader->ctba);
    k_bzero(cmdtbl, sizeof(hba_cmd_tbl_t) + (cmdheader->prdtl - 1) * sizeof(hba_prdt_entry_t));

    // Only entry
    cmdtbl->prdt_entry[0].dba = (uint64_t)buf;
    cmdtbl->prdt_entry[0].dbc = count << 9; // 512 bytes per sector
    cmdtbl->prdt_entry[0].i = 1;

    // Setup command
    fis_reg_h2d_t* cmdfis = (fis_reg_h2d_t*)(&cmdtbl->cfis);

    cmdfis->fis_type = FIS_TYPE_REG_H2D;
    cmdfis->c = 1; // Command
    cmdfis->command = ATA_CMD_READ_DMA_EX;

    SECTOR_TO_LBA(cmdfis, sector);

    cmdfis->device = CMD_FIS_DEV_LBA; // LBA mode

    cmdfis->count = count;

    return busy_wait_on_port(port, slot);
}

bool verify_block(uint64_t value, uint8_t* bytes) {
    for (int i = 0; i < 4096; ++i) {
        if (bytes[i] != value) {
            kerror("Non-matching byte at %d", i);
            return false;
        }
    }
    return true;
}

int ahci_config(pcie_header0_t* pcidev) {
    // Generic Host Control

    abar = (hba_mem_t*)(uint64_t)pcidev->bar[5];

    /* uint32_t sata_ports = */ probe_port(abar);

    uint8_t* outbuffer = (uint8_t*)0x100000;
    uint8_t* inbuffer = (uint8_t*)0x100000 + 4096;
    k_memset(inbuffer, 0xff, 4096);

    for (int i = 0; i < 100; ++i) {
        k_memset(outbuffer, i, 4096);
        ahci_write(&abar->ports[1], i, 8, outbuffer);
        ahci_read(&abar->ports[1], i, 8, inbuffer);
        if (!verify_block(i, inbuffer)) {
            kerror("sector %d: Did not write correctly!", i);
            break;
        }
    }

    ksuccess("Done writing/reading verification");

    return 0;
}
