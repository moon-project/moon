/**
 * @file pcie.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/ahci.h>
#include <sys/core/asm.h>
#include <sys/core/kutils.h>
#include <sys/core/pcie.h>
#include <sys/defs.h>

void pcie_register_fun(uint8_t bus, uint8_t device, uint8_t fun, pcie_header0_t* pcie) {

    // kinfo("bs: 0x%x  "
    //         "de: 0x%x  "
    //         "fn: 0x%x  "
    //         "HT: 0x%x  "
    //         "CC: 0x%x  "
    //         "SC: 0x%x  "
    //         "PI: 0x%x",
    //         bus,
    //         device,
    //         fun,
    //         pcie->pcie_header.header_type,
    //         pcie->pcie_header.class_code,
    //         pcie->pcie_header.subclass,
    //         pcie->pcie_header.prog_if);

    if (pcie->pcie_header.class_code == AHCI_CLASSCODE && pcie->pcie_header.subclass == AHCI_SUBCLASS
        && pcie->pcie_header.prog_if == AHCI_PROGIF) {
        // Set BAR 5 to 0xA0000
        uint32_t bar5 = pcie_readconfig(bus, device, fun, 0x24);
        kwarn("OLD BAR5 = %p\t", bar5);
        bar5 = 640 << 10;
        pcie_writeconfig(bus, device, fun, 0x24, bar5);
        ksuccess("NEW BAR5 = %p\n", bar5);
        pcie->bar[5] = bar5;
        ahci_config(pcie);
    }
}

void pcie_fun(uint8_t bus, uint8_t device, uint8_t fun) {
    uint32_t buf[18];
    k_bzero(buf, 18 * sizeof(uint32_t));
    for (int reg = 0; reg < 0x40; reg += 4) {

        buf[reg >> 2] = pcie_readconfig(bus, device, fun, reg);

        if (reg == 0xC && ((buf[reg] & 0x00FF0000) != 0)) {
            // Early stop: Not header of type 0x0
            return;
        }
    }
    pcie_register_fun(bus, device, fun, (pcie_header0_t*)buf);
}

void pcie_device(uint8_t bus, uint8_t device) {
    // Get header type
    // if top bit set then is multifun, so check 8 possible funs
    // otherwise, 1 fun only

    uint16_t vendor_id = pcie_readconfig(bus, device, 0, 0x0) & 0xFFFF;
    if (vendor_id == 0xFFFF) {
        // Early stop: vendor_id is all one's
        return;
    }

    uint32_t header_type = (pcie_readconfig(bus, device, 0, 0xC) & 0x00FF0000) >> 16;

    uint32_t num_fun = header_type & 0x80 ? 8 : 1;

    for (int fun = 0; fun < num_fun; ++fun) {
        pcie_fun(bus, device, fun);
    }
}

void pcie_scanbus() {
    uint8_t bus = 0, device = 0;

    int i = 0;
    while (i < (32 * 256)) { // 32 devices per 256 buses
        bus = i / 32;
        device = i % 32;
        pcie_device(bus, device);
        i++;
    }
    ksuccess("Finished walking config\n");
}

uint32_t pcie_readconfig(uint8_t bus, uint8_t dev, uint8_t fun, uint8_t reg) {
    uint32_t address = 0;

    address |= (uint32_t)(0x80000000);
    address |= (uint32_t)(bus << 16);
    address |= (uint32_t)(dev << 11);
    address |= (uint32_t)(fun << 8);
    address |= (uint32_t)(reg & 0xFC);

    /* write out the pcie address */
    wport32(CONFIG_ADDRESS, address);
    /* read in the pcie data */
    return rport32(CONFIG_DATA);
}

void pcie_writeconfig(uint8_t bus, uint8_t dev, uint8_t fun, uint8_t reg, uint32_t data) {
    uint32_t address = 0;

    address |= (uint32_t)(0x80000000);
    address |= (uint32_t)(bus << 16);
    address |= (uint32_t)(dev << 11);
    address |= (uint32_t)(fun << 8);
    address |= (uint32_t)(reg & 0xFC);

    /* write out the pcie address */
    wport32(CONFIG_ADDRESS, address);
    wport32(CONFIG_DATA, data);
}
