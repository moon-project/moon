/**
 * @file init.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/init.h>
#include <sys/core/kutils.h>
#include <sys/core/vm.h>

void cpuGetMSR(uint32_t msr, uint32_t* lo, uint32_t* hi) {
    asm volatile("rdmsr" : : "a"(*lo), "d"(*hi), "c"(msr));
}

void cpuSetMSR(uint32_t msr, uint32_t lo, uint32_t hi) {
    asm volatile("wrmsr" : : "a"(lo), "d"(hi), "c"(msr));
}

void init_ram(const init_args_t const* args) {

    // Enable syscalls
    // EFER MSR num = 0xC0000080
    cpuSetMSR(0xc0000080, 1, 0);

    uint32_t* modulep = args->modulep;

    struct smap_t {
        uint64_t base, length;
        uint32_t type;
    } __attribute__((packed)) * smap;

    while (modulep[0] != 0x9001) {
        modulep += modulep[1] + 2;
    }

    phys_blocks = (physblk_t*)args->physfree;
    physblk_count = 0;
    uintptr_t first_free_blk = (uintptr_t)args->physfree;
    for (smap = (struct smap_t*)(modulep + 2); smap < (struct smap_t*)((char*)modulep + modulep[1] + 2 * 4); ++smap) {
        if (smap->type != 1 /* memory */ || smap->length == 0) {
            continue;
        }

        // ksuccess(
        //     "Discovered Region [ %p - %p ]  %d pages", smap->base, smap->base + smap->length, (smap->length / 4096));

        add_blocks(smap->base, smap->length, &first_free_blk);
    }
    // kinfo("Num Free Blocks: %d", physblk_count);
    // initialize ze allocator
    init_allocator((uintptr_t)args->physbase, first_free_blk);
    init_vm((uintptr_t)args->physbase, args->virtbase);
}

uint64_t add_blocks(uint64_t start, uint64_t length, uint64_t* physfree) {

    // Minus one so that we don't mark the base address of a gap as a free page
    size_t npages = (length / PGSZ);

    for (int block_count = 0; block_count < npages; ++block_count, ++physblk_count) {
        phys_blocks[physblk_count].base = start + (block_count * PGSZ);
        // Move the first available free block after all the physical memory bookkeeping.
        if (phys_blocks + physblk_count >= (physblk_t*)*physfree) {
            // This will guarantee that it gets rounded up.
            *physfree += PGSZ;
        }
    }

    return npages;
}
