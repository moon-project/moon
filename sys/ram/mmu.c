/**
 * @file mmu.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/kutils.h>
#include <sys/core/vm.h>

physblk_t* phys_blocks;
size_t physblk_count = 0;
static uint64_t current_head;
static uint64_t num_free = 0;
size_t one2one_offset = 0;

void init_allocator(uintptr_t physbase, uintptr_t physfree) {
    for (uint64_t i = 0; i < physblk_count; i++) {
        if (phys_blocks[i].base < physfree) {
            phys_blocks[i].alloc = true;
        } else {
            num_free++;
            phys_blocks[i].alloc = false;
            phys_blocks[i].next = i + 1;
            phys_blocks[i].prev = i - 1;
            if (phys_blocks[i].base == physfree) {
                phys_blocks[i].prev = -1;
                current_head = i;
            }
            if (i == physblk_count - 1) {
                phys_blocks[i].next = -1;
            }
        }
    }
}

uintptr_t phys_alloc(size_t numpgs) {
    for (uint64_t i = current_head; i < physblk_count; i++) {
        bool found = true;
        for (uint64_t j = i; j < i + numpgs; j++) {
            if (phys_blocks[j].alloc) { // allocated page
                found = false;
                break;
            }
        }
        if (!found) {
            continue;
        }
        uintptr_t physpage = phys_blocks[i].base;
        for (uint64_t j = i; j < i + numpgs; j++) {
            phys_blocks[j].alloc = 1;
        }
        // remove from list
        uint64_t prev_free = phys_blocks[i].prev;
        uint64_t next_free = phys_blocks[i + numpgs - 1].next;
        phys_blocks[next_free].prev = prev_free;
        phys_blocks[prev_free].next = next_free;
        if (prev_free == -1) {
            current_head = next_free;
        }
        num_free--;
        if (num_free == 0) {
            current_head = -1;
        }
        k_memset(PA2VA(physpage), 0, PGSZ);

        return physpage;
    }
    // ggwp
    panics("phys ENOMEM");
    return -1;
}

void phys_free(uintptr_t addr) {
    uint64_t pagenum = addr / PGSZ;
    // free page
    phys_blocks[pagenum].alloc = false;
    // insert in list
    num_free++;
    if (current_head == -1) { // First block
        current_head = pagenum;
        phys_blocks[pagenum].next = -1;
        phys_blocks[pagenum].prev = -1;
    } else if (pagenum < current_head) { // Head
        phys_blocks[pagenum].next = current_head;
        phys_blocks[pagenum].prev = -1;
        phys_blocks[current_head].prev = pagenum;
    } else {
        uint64_t i = current_head;
        uint64_t pi = i;
        while (i < pagenum) {
            pi = i;
            i = phys_blocks[i].next;
        }
        phys_blocks[pagenum].next = i;
        phys_blocks[pagenum].prev = pi;
        phys_blocks[pi].next = pagenum;
        if (i != -1) { // Not Tail
            phys_blocks[i].prev = pagenum;
        }
    }
}
