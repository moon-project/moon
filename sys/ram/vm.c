/**
 * @file vm.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/asm.h>
#include <sys/core/kutils.h>
#include <sys/core/screen.h>
#include <sys/core/vm.h>

void* heapbase = NULL;
void* heapbrk = NULL;
void* kpml4 = NULL;
static void *kvirtbase, *kvirtend;
uintptr_t kphysbase;

bool donewithkernel = false;

typedef struct {
    uint64_t ppo : 12;
    uint64_t pto : 9;
    uint64_t pdo : 9;
    uint64_t pdpo : 9;
    uint64_t pml4o : 9;
    uint64_t _ : 16;
} virtaddress_fmt;

void init_vm(uintptr_t physbase, void* virtbase) {
    // Allocate a page for PML4
    va_entry_t(*pml4)[TBLENTRIES] = kpml4 = (void*)phys_alloc(1);
    for (int i = 0; i < TBLENTRIES; i++) {
        (*pml4)[i].present = false;
    }
    // Iterate over virtaddresses from virtbase to virtbase + (physfree - physbase) in units of pages
    uintptr_t offset = ((uintptr_t)pml4) - physbase + PGSZ;
    kvirtend = (void*)((uintptr_t)virtbase + offset);

    kphysbase = physbase;
    kvirtbase = virtbase;
    heapbase = kvirtend;
    heapbrk = heapbase - PGSZ;

    ksuccess("Kernel [ %p - %p ]", virtbase, kvirtend);

    // Map kernel
    uintptr_t physpage = physbase;
    for (void *addr = virtbase; addr < kvirtend; addr += PGSZ, physpage += PGSZ) {
        map_va2pa(addr, pml4, physpage);
    }

    // Map physical memory
    for (int i = 0; i < physblk_count; i++) {
        map_va2pa((void*)phys_blocks[i].base + PHYS_OFFSET, pml4, phys_blocks[i].base);
    }

    void* screen_addr = SCREEN_BASE;
    map_va2pa((void*)screen_addr, pml4, (uintptr_t)0xb8000);

    setcr3((uint64_t)pml4);

    phys_blocks = (void*)phys_blocks + PHYS_OFFSET;
    kpml4 += PHYS_OFFSET;
    one2one_offset = PHYS_OFFSET;

    donewithkernel = true;
}

/*
    Allocate a page for PML4, set all entries to invalid
    Iterate over virt addresses from virtbase to virtbase + (physfree - physbase) by pages
    Page walk for each iteration, allocating levels if an invalid entry is reached.
    On allocation, set valid.
    This maps kernel in our own page tables, we can now set cr3. and then throw a party!
*/
uintptr_t va_is_mapped(void* addr, va_entry_t (*pml4_table)[TBLENTRIES]) {
    virtaddress_fmt* addrp = (virtaddress_fmt*)&addr;

    va_entry_t(*table)[TBLENTRIES];

    // PML4
    if ((*pml4_table)[addrp->pml4o].present == false) {
        return false;
    }

    // PDP
    table = PA2VA(GETBASE(((*pml4_table)[addrp->pml4o].base)));
    if ((*table)[addrp->pdpo].present == false) {
        return false;
    }

    // PD
    table = PA2VA(GETBASE((*table)[addrp->pdpo].base));
    if ((*table)[addrp->pdo].present == false) {
        return false;
    }

    // PT
    table = PA2VA(GETBASE((*table)[addrp->pdo].base));
    if ((*table)[addrp->pto].present == false) {
        return GETBASE((*table)[addrp->pto].base);
    }

    return false;
}

uintptr_t map_va2pa(void* addr, va_entry_t (*pml4)[TBLENTRIES], uintptr_t physaddr) {
    virtaddress_fmt* addrp = (virtaddress_fmt*)&addr;

    if ((*pml4)[addrp->pml4o].present == false) {
        (*pml4)[addrp->pml4o].base = SETBASE(phys_alloc(1));
        (*pml4)[addrp->pml4o].present = true;
        (*pml4)[addrp->pml4o].rw = true;
        (*pml4)[addrp->pml4o].user = false;
    }

    va_entry_t(*table)[TBLENTRIES];
    // PDP
    table = PA2VA(GETBASE(((*pml4)[addrp->pml4o].base)));
    if ((*table)[addrp->pdpo].present == false) {
        (*table)[addrp->pdpo].base = SETBASE(phys_alloc(1));
        (*table)[addrp->pdpo].present = true;
        (*table)[addrp->pdpo].rw = true;
        (*table)[addrp->pdpo].user = false;
    }

    // PD
    table = PA2VA(GETBASE((*table)[addrp->pdpo].base));
    if ((*table)[addrp->pdo].present == false) {
        (*table)[addrp->pdo].base = SETBASE(phys_alloc(1));
        (*table)[addrp->pdo].present = true;
        (*table)[addrp->pdo].rw = true;
        (*table)[addrp->pdo].user = false;
    }

    // PT
    table = PA2VA(GETBASE((*table)[addrp->pdo].base));
    if ((*table)[addrp->pto].present == false) {
        (*table)[addrp->pto].base = SETBASE(physaddr);
        (*table)[addrp->pto].present = true;
        (*table)[addrp->pto].rw = true;
        (*table)[addrp->pto].user = false;
    }

    return GETBASE((*table)[addrp->pto].base);
}

uintptr_t map_va(void* addr, va_entry_t (*pml4)[TBLENTRIES], uint8_t flags) {
    addr = (void*)((uintptr_t)addr & ~0xfff);
    uintptr_t pa; // If addr is already mapped then pa will be unused

    if ((pa = va_is_mapped(addr, pml4)) == 0) {
        pa = map_va2pa(addr, pml4, phys_alloc(1));
    }

    panicif(va_user(addr, pml4, (flags & VA_USER) != 0) < 0); // the page should always be mapped

    panicif(va_rw(addr, pml4, (flags & VA_RW) != 0) < 0); // the page should always be mapped

    return pa;
}

uintptr_t unmap_va(void* addr, va_entry_t (*pml4)[TBLENTRIES]) {

    virtaddress_fmt* addrp = (virtaddress_fmt*)&addr;

    if ((*pml4)[addrp->pml4o].present == false) {
        return 0;
    }

    va_entry_t(*table)[TBLENTRIES];
    // PDP
    table = PA2VA(GETBASE(((*pml4)[addrp->pml4o].base)));
    if ((*table)[addrp->pdpo].present == false) {
        return 0;
    }

    // PD
    table = PA2VA(GETBASE((*table)[addrp->pdpo].base));
    if ((*table)[addrp->pdo].present == false) {
        return 0;
    }

    // PT
    table = PA2VA(GETBASE((*table)[addrp->pdo].base));
    // only if present is set?
    if ((*table)[addrp->pto].present == false) {
        return 0;
    }
    uintptr_t physpage = GETBASE((*table)[addrp->pto].base);
    (*table)[addrp->pto].base = SETBASE(NULL);
    (*table)[addrp->pto].present = false;
    (*table)[addrp->pto].rw = false;
    (*table)[addrp->pto].user = false;
    return physpage;
}

bool DebugWalk = false;

void debug_walk(void* addr, va_entry_t (*pml4)[TBLENTRIES]) {
    if (!DebugWalk)
        return;
    virtaddress_fmt* addrp = (virtaddress_fmt*)&addr;

    kdebug("Walking %x for %x", addr, pml4);

    if ((*pml4)[addrp->pml4o].present == false) {
        return;
    }
    kdebug("PML4 %p    RW=%d USER=%d", GETBASE(((*pml4)[addrp->pml4o].base)), (*pml4)[addrp->pml4o].rw,
        (*pml4)[addrp->pml4o].user);

    va_entry_t(*table)[TBLENTRIES];
    // PDP
    table = PA2VA(GETBASE(((*pml4)[addrp->pml4o].base)));
    if ((*table)[addrp->pdpo].present == false) {
        return;
    }
    kdebug("PDP  %p    RW=%d USER=%d", GETBASE(((*table)[addrp->pdpo].base)), (*table)[addrp->pdpo].rw,
        (*table)[addrp->pdpo].user);

    // PD
    table = PA2VA(GETBASE((*table)[addrp->pdpo].base));
    if ((*table)[addrp->pdo].present == false) {
        return;
    }
    kdebug("PD   %p    RW=%d USER=%d", GETBASE(((*table)[addrp->pdo].base)), (*table)[addrp->pdo].rw,
        (*table)[addrp->pdo].user);
    // kdebug("PDraw: %x", *(uint64_t*)(&(*table)[addrp->pdo]));

    // PT
    table = PA2VA(GETBASE((*table)[addrp->pdo].base));
    if ((*table)[addrp->pto].present == false) {
        return;
    }
    kdebug("PT   %p    RW=%d USER=%d", GETBASE(((*table)[addrp->pto].base)), (*table)[addrp->pto].rw,
        (*table)[addrp->pto].user);
    // kdebug("PTraw: %x", *(uint64_t*)(&(*table)[addrp->pto]));
}

int va_user(void* addr, va_entry_t (*pml4_table)[TBLENTRIES], bool set) {
    virtaddress_fmt* addrp = (virtaddress_fmt*)&addr;

    va_entry_t(*table)[TBLENTRIES];

    // PML4
    if ((*pml4_table)[addrp->pml4o].present == false) {
        return -1;
    }
    (*pml4_table)[addrp->pml4o].user = true;

    // PDP
    table = PA2VA(GETBASE(((*pml4_table)[addrp->pml4o].base)));
    if ((*table)[addrp->pdpo].present == false) {
        return -2;
    }
    (*table)[addrp->pdpo].user = true;

    // PD
    table = PA2VA(GETBASE((*table)[addrp->pdpo].base));
    if ((*table)[addrp->pdo].present == false) {
        return -3;
    }
    (*table)[addrp->pdo].user = true;

    // PT
    table = PA2VA(GETBASE((*table)[addrp->pdo].base));
    if ((*table)[addrp->pto].present == false) {
        return -4;
    }
    (*table)[addrp->pto].user = true;

    return false;
}

int va_rw(void* addr, va_entry_t (*pml4_table)[TBLENTRIES], bool set) {
    virtaddress_fmt* addrp = (virtaddress_fmt*)&addr;

    va_entry_t(*table)[TBLENTRIES];

    // PML4
    if ((*pml4_table)[addrp->pml4o].present == false) {
        return -1;
    }
    (*pml4_table)[addrp->pml4o].rw = true;

    // PDP
    table = PA2VA(GETBASE(((*pml4_table)[addrp->pml4o].base)));
    if ((*table)[addrp->pdpo].present == false) {
        return -2;
    }
    (*table)[addrp->pdpo].rw = true;

    // PD
    table = PA2VA(GETBASE((*table)[addrp->pdpo].base));
    if ((*table)[addrp->pdo].present == false) {
        return -3;
    }
    (*table)[addrp->pdo].rw = true;

    // PT
    table = PA2VA(GETBASE((*table)[addrp->pdo].base));
    if ((*table)[addrp->pto].present == false) {
        return -4;
    }
    (*table)[addrp->pto].rw = true;

    return false;
}

void* valloc() {
    heapbrk += PGSZ;

    map_va(heapbrk, kpml4, phys_alloc(1));

    return heapbrk;
}

void vfree(void* ptr) {
    uintptr_t physpage = unmap_va(ptr, kpml4);
    phys_free(physpage);
}

void* get_kern_heapbase() {
    return heapbase;
}

// Base address of last valid page in heap (POINTS TO ACCESSIBLE MEMORY!!)
void* get_kern_heapbrk() {
    return heapbrk;
}
