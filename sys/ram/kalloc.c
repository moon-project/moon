/**
 * @file kalloc.c
 * @author Neal Beeken, Mrunal Patel
 * @brief
 */

#include <sys/core/kutils.h>
#include <sys/core/vm.h>

void kfree(void* ptr);

#define ALLOCATED 1
#define FREE 0

#define MIN_SZ 32

#define PAD(size) ((16 - ((size) % 16)) % 16)
#define ALIGN(size) MAX((PAD(size) + size), MIN_SZ)

#define SHIFT_DOWN_SIZE(size) ((size) >> 4)
#define SHIFT_UP_SIZE(size) ((size) << 4)

// Read Ops
#define GET_BLK_SZ(metaptr) (SHIFT_UP_SIZE(((meta_t*)metaptr)->block))
#define IS_FREE(metaptr) (((meta_t*)metaptr)->alloc == FREE)
#define IS_ALLOCATED(metaptr) (((meta_t*)metaptr)->alloc == FREE)

// Write Ops
#define SET_BLK_SZ(metaptr, size)                                                                                      \
    do {                                                                                                               \
        ((meta_t*)metaptr)->block = SHIFT_DOWN_SIZE(size);                                                             \
        GET_CURR_FOOTER(metaptr)->block = SHIFT_DOWN_SIZE(size);                                                       \
    } while (0)

#define SET_ALLOCATED(metaptr)                                                                                         \
    do {                                                                                                               \
        ((meta_t*)metaptr)->alloc = ALLOCATED;                                                                         \
        GET_CURR_FOOTER(metaptr)->alloc = ALLOCATED;                                                                   \
    } while (0)

#define SET_FREE(metaptr)                                                                                              \
    do {                                                                                                               \
        ((meta_t*)metaptr)->alloc = FREE;                                                                              \
        GET_CURR_FOOTER(metaptr)->alloc = FREE;                                                                        \
    } while (0)

// Get the block size add it to the current header minus 8 should point you at the footer
#define GET_CURR_FOOTER(headerptr) ((footer_t*)(void*)(BYBYTE(headerptr, +GET_BLK_SZ(headerptr) - 8)))

// Minus 8 from current header should previous block's footer
#define GET_PREV_FOOTER(headerptr) ((footer_t*)(BYBYTE(headerptr, -8)))

// Get block size from footer, then subtract that amount from header address should point you at the prev header addr
#define GET_PREV_HEADER(headerptr) ((freeblock_t*)(BYBYTE(headerptr, -GET_BLK_SZ(GET_PREV_FOOTER(headerptr)))))

// Get current block size, add it to the current header and you should be pointing at the next header addr
#define GET_NEXT_HEADER(headerptr)                                                                                     \
    ((freeblock_t*)(BYBYTE(headerptr, +SHIFT_UP_SIZE(((freeblock_t*)headerptr)->meta.block))))

// Need at least 8 bytes of space to write to (- 8 at the end)
#define IS_IN_HEAP(ptr) (BYBYTE(ptr, < BYBYTE(get_kern_heapbrk(), +PGSZ))) && BYBYTE(ptr, >= get_kern_heapbase())

typedef struct {
    uint64_t alloc : 4;
    uint64_t block : 60;
} __attribute__((packed)) meta_t;

typedef meta_t header_t;
typedef meta_t footer_t;

typedef struct blk {
    meta_t meta;
    struct blk* prev;
    struct blk* next;
} freeblock_t;

static freeblock_t* freelist = NULL;

void remove_block(freeblock_t* block) {
    if (block->next != NULL) {
        block->next->prev = block->prev;
    }
    if (block->prev != NULL) {
        block->prev->next = block->next;
    }
    if (block == freelist) {
        freelist = block->next;
    }
}

void insert_block(freeblock_t* block) {
    panicif(block->meta.alloc == ALLOCATED);
    // Update freelist head
    if (freelist != NULL) {
        freelist->prev = block;
    }
    block->next = freelist;
    block->prev = NULL;
    freelist = block; // Insert at head of list
}

void* find_block(uint64_t block_sz) {
    freeblock_t* cursor;
    for (cursor = freelist; cursor != NULL; cursor = cursor->next) {
        if (GET_BLK_SZ(cursor) >= block_sz) {
            // Remove from list
            remove_block(cursor);
            return cursor;
        }
    }
    return NULL;
}

void* kalloc(size_t size) {

    header_t* header = find_block(ALIGN(size));

    if (header == NULL) {
        /* Either freelist is empty or doesn't have a block good enough for us */
        /* In either case we want to allocate for more! */

        freeblock_t* newblock = valloc();

        SET_BLK_SZ(newblock, PGSZ);
        SET_ALLOCATED(newblock);

        newblock->prev = NULL;
        newblock->next = NULL;

        kfree(BYBYTE(newblock, +8)); // "free" this new block

        return kalloc(size); // Try again.
    }

    /* Block Found */

    uint64_t old_size = header->block << 4; // GET_BLK_SZ(header);

    // size + 16 for header & footer ALIGN brings us to alignment
    uint64_t new_size = ALIGN(size + 16);

    SET_BLK_SZ(header, new_size);
    SET_ALLOCATED(header);
    remove_block((freeblock_t*)header);

    // Set header for new next block
    freeblock_t* newheader = GET_NEXT_HEADER(header);
    panicif(!IS_IN_HEAP(newheader));
    panicif(!IS_IN_HEAP(GET_CURR_FOOTER(newheader)));

    SET_BLK_SZ(newheader, old_size - new_size);

    insert_block(newheader);

    void* payload = BYBYTE(header, +8);
    k_bzero(header, new_size - 16); // Zero the whole block cus we're nice
    return payload;
}

void kfree(void* ptr) {
    // Check if ptr not in heap
    panicif(!IS_IN_HEAP(ptr));

    header_t* newheader = ptr - 8;
    footer_t* newfooter = GET_CURR_FOOTER(newheader);

    panicif(GET_BLK_SZ(newheader) != GET_BLK_SZ(newfooter)); // Panic if footer and header don't match
    panicif(IS_FREE(newheader) && IS_FREE(newfooter)); // Panic if freeing already free blocks

    // Coalescing
    size_t newblock_size = GET_BLK_SZ(newheader);

    freeblock_t* freeblock_header = (freeblock_t*)newheader;

    freeblock_t* prevblock_header = GET_PREV_HEADER(freeblock_header);
    freeblock_t* nextblock_header = GET_NEXT_HEADER(freeblock_header);

    // Check if previous block is free
    if (IS_IN_HEAP(prevblock_header) && IS_FREE(prevblock_header)) {
        // Remove from list
        remove_block(prevblock_header);
        // Increase total size of new block
        newblock_size += GET_BLK_SZ(prevblock_header);
        // Move pointer up to this prev block
        freeblock_header = prevblock_header;
    }

    // Check if next block is free
    if (IS_IN_HEAP(nextblock_header) && IS_FREE(nextblock_header)) {
        // Remove from list
        remove_block(nextblock_header);
        // Increase total size of new block
        newblock_size += GET_BLK_SZ(nextblock_header);
    }

    SET_BLK_SZ(freeblock_header, newblock_size);
    SET_FREE(freeblock_header);

    insert_block(freeblock_header);
}

void* krealloc(void* ptr, size_t req_size) {
    header_t* header = BYBYTE(ptr, -8);
    size_t userdata_sz = GET_BLK_SZ(header) - 16;
    void* new_mem = kalloc(req_size);
    k_memcpy(new_mem, ptr, userdata_sz);
    kfree(ptr);
    return new_mem;
}
