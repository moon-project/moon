#include <sys/core/asm.h>
#include <sys/core/kutils.h>
#include <sys/core/proc.h>
#include <sys/core/screen.h>
#include <sys/core/vm.h>

/*
info =>
31                4                             0
+-----+-...-+-----+-----+-----+-----+-----+-----+
|     Reserved    | I/D | RSVD| U/S | W/R |  P  |
+-----+-...-+-----+-----+-----+-----+-----+-----+
P:
    When set, the fault was caused by a protection violation.
    When not set, it was caused by a non-present page.
W/R:
    When set, write access caused the fault; otherwise read access.
U/S:
    When set, the fault occurred in user mode; otherwise in supervisor mode.
RSVD:
    When set, one or more page directory entries contain reserved bits which are set to 1.
    This only applies when the PSE or PAE flags in CR4 are set to 1.
I/D:
    When set, the fault was caused by an instruction fetch.
    This only applies when the No-Execute bit is supported and enabled.
*/

#define PF_PROTECTION 1
#define PF_WRITE 2
#define PF_USER 4
#define PF_RSVD 8
#define PF_INST_FETCH 16

void segfault() {
    // Kill the current thread here
    task_t* current_task = getcurrenttask();
    current_task->state = EXITED;
    kerror("SEGFAULT: Accessing protected page. Killing.");
    enable_interrupts();
    schedule();
}

void page_fault_handler(void* fault_addr, int info) {
    if (info & PF_PROTECTION) {
        // SEGFAULT
        segfault();
    }
    // current task
    task_t* current_task = getcurrenttask();
    mm_t* mm = current_task->mm;

    // MAJOR ASSUMPTION: only stack faults

    if ((uintptr_t)fault_addr > mm->stack_start || (uintptr_t)fault_addr < mm->stack_mark) {
        // SEGFAULT
        segfault();
    }

    map_va(fault_addr, PA2VA(current_task->cr3), VA_USER | VA_RW);

    mm->stack_start -= PGSZ;
    mm->stack_mark -= PGSZ;
}
